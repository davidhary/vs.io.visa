Imports isr.Core.Pith
Imports isr.Core.Pith.EventHandlerExtensions
Imports isr.Core.Pith.ExceptionExtensions
''' <summary> Defines a Test Script Processor (TSP) System consists of one or more TSP nodes. </summary>
''' <license> (c) 2007 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="11/9/2013" by="David" revision="">             Uses new VISA library. </history>
''' <history date="03/21/2007" by="David" revision="1.15.2636.x"> Created. </history>
Public Class TspSystem
    Implements IDisposable, ITraceMessagePublisher

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructor. </summary>
    ''' <param name="device"> The master device. </param>
    Public Sub New(ByVal device As MasterDeviceBase)
        MyBase.New()
        Me._Device = device
    End Sub

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make this method Overridable (virtual) because a derived 
    '''   class should not be able to override this method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' GC.SuppressFinalize is issued to take this object off the 
        ' finalization(Queue) and prevent finalization code for this 
        ' prevent from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived class
    ''' provided proper implementation.
    ''' </summary>
    Protected Property IsDisposed() As Boolean

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    ''' release only unmanaged resources. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        If Not Me.IsDisposed Then

            Try

                If disposing Then

                    For Each d As [Delegate] In Me.ConnectionChangedEvent.SafeInvocationList
                        Try
                            RemoveHandler Me.ConnectionChanged, CType(d, Global.System.EventHandler(Of System.EventArgs))
                        Catch ex As Exception
                            Debug.Assert(Not Debugger.IsAttached, ex.ToString)
                        End Try
                    Next

                    For Each d As [Delegate] In Me.TraceMessageAvailableEvent.SafeInvocationList
                        Try
                            RemoveHandler Me.TraceMessageAvailable, CType(d, Global.System.EventHandler(Of TraceMessageEventArgs))
                        Catch ex As Exception
                            Debug.Assert(Not Debugger.IsAttached, ex.ToString)
                        End Try
                    Next

                    If Me._Device IsNot Nothing Then
                        Me._Device.Dispose()
                        Me._Device = Nothing
                    End If
                End If

                ' Free shared unmanaged resources

            Finally

                ' set the sentinel indicating that the class was disposed.
                Me.IsDisposed = True

            End Try

        End If

    End Sub

    ''' <summary>This destructor will run only if the Dispose method 
    '''   does not get called. It gives the base class the opportunity to 
    '''   finalize. Do not provide destructors in types derived from this class.</summary>
    Protected Overrides Sub Finalize()
        Try
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal for readability and maintainability.
            Dispose(False)
        Finally
            ' The compiler automatically adds a call to the base class finalizer 
            ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
            MyBase.Finalize()
        End Try
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Clears all status data structure registers (enable, event, negative and positive transitions)
    ''' to their power up states.  The naked TSP clear does not clear the
    ''' error queue. This command adds clear for error queue.
    '''          </summary>
    Public Sub ClearExecutionState()
        Me.Device.ClearExecutionState()
    End Sub

    ''' <summary> Resets and clears the subsystem. Starts with issuing a selective-device-clear, reset
    ''' (RST), Clear Status (CLS, and clear error queue). </summary>
    Public Sub ResetAndClear()
        Me.Device.ResetAndClear()
    End Sub

    ''' <summary> Resets the Device to its known state. </summary>
    Public Sub ResetKnownState()
        ' RST issues the reset() message, which returns all the nodes
        ' on the TSP LInk system to the original factory defaults:
        Me.Device.ResetKnownState()
    End Sub

#End Region

#Region " I TRACE MESSAGE PUBLISHER "

    ''' <summary> Executes the trace message available action. </summary>
    ''' <param name="value"> The TraceMessage to process. </param>
    Public Sub OnTraceMessageAvailable(ByVal value As TraceMessage) Implements ITraceMessagePublisher.OnTraceMessageAvailable
        Dim evt As EventHandler(Of TraceMessageEventArgs) = Me.TraceMessageAvailableEvent
        evt?.Invoke(Me, New TraceMessageEventArgs(value))
    End Sub

    ''' <summary> Executes the trace message available action. </summary>
    ''' <param name="eventType">  The <see cref="TraceEventType">event type</see>. </param>
    ''' <param name="id">         The identifier to use with the trace event. </param>
    ''' <param name="format">     Describes the format to use. </param>
    ''' <param name="args">       A variable-length parameters list containing arguments. </param>
    ''' <returns> The event arguments. </returns>
    Public Function OnTraceMessageAvailable(ByVal eventType As TraceEventType, ByVal id As Integer,
                                            format As String, ParamArray args() As Object) As TraceMessage Implements ITraceMessagePublisher.OnTraceMessageAvailable
        Dim e As New TraceMessage(eventType, id, format, args)
        Me.OnTraceMessageAvailable(e)
        Return e
    End Function

    ''' <summary> Raises the core. diagnosis. trace message event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Public Sub OnTraceMessageAvailable(ByVal e As TraceMessageEventArgs) Implements ITraceMessagePublisher.OnTraceMessageAvailable
        If e IsNot Nothing Then
            Me.OnTraceMessageAvailable(e.TraceMessage)
        End If
    End Sub

    ''' <summary> Event queue for all listeners interested in TraceMessageAvailable events. </summary>
    Public Event TraceMessageAvailable As EventHandler(Of TraceMessageEventArgs) Implements ITraceMessagePublisher.TraceMessageAvailable

#End Region

#Region " TSP ENTITIES "

    Private WithEvents _Device As MasterDeviceBase

    ''' <summary> Gets or sets reference to the
    ''' <see cref="MasterDeviceBase">master device</see> for accessing this TSP system. </summary>
    ''' <value> The master device. </value>
    Public ReadOnly Property Device() As MasterDeviceBase
        Get
            Return Me._Device
        End Get
    End Property

#End Region

#Region " DEVICE EVENT HANDLERS "

    ''' <summary> Gets the is device open. </summary>
    ''' <value> The is device open. </value>
    Public ReadOnly Property IsDeviceOpen As Boolean
        Get
            Return Me.Device IsNot Nothing AndAlso Me.Device.IsDeviceOpen
        End Get
    End Property

    ''' <summary> Gets the is session open. </summary>
    ''' <value> The is session open. </value>
    Public ReadOnly Property IsSessionOpen As Boolean
        Get
            Return Me.Device IsNot Nothing AndAlso Me.Device.IsSessionOpen
        End Get
    End Property

    ''' <summary> Device property changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Sub DevicePropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Handles _Device.PropertyChanged
        Try
            If e IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(e.PropertyName) Then
                Select Case e.PropertyName
                    Case "Enabled"
                        Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Hardware {0};. ",
                                                   If(Me.Device.Enabled, "Enabled", "Disabled"))
                    Case "ResourcesSearchPattern"
                    Case "ServiceRequestFailureMessage"
                        If Not String.IsNullOrWhiteSpace(Me.Device.ServiceRequestFailureMessage) Then
                            Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, Me.Device.ServiceRequestFailureMessage)
                        End If
#If False Then
                    Case "IsSessionOpen"
                    Case "IsDeviceOpen"
#End If
                End Select
            End If
        Catch ex As Exception
            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                       "Exception handling property changed Event;. Failed property {0}. {1}", e.PropertyName, ex.ToFullBlownString)
        End Try
    End Sub


    ''' <summary> Device service requested. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Message based session event information. </param>
    Private Sub DeviceServiceRequested(ByVal sender As Object, ByVal e As NationalInstruments.VisaNS.MessageBasedSessionEventArgs) Handles _Device.ServiceRequested
    End Sub

    ''' <summary> Event handler. Called upon device opening. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    Private Sub DeviceOpening(ByVal sender As Object, ByVal e As System.EventArgs) Handles _Device.Opening
    End Sub

    ''' <summary> Event handler. Called when device opened. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    Private Sub DeviceOpened(ByVal sender As Object, ByVal e As System.EventArgs) Handles _Device.Opened

        ' moved to the calling application
        ' Me.Device.InitializeKnownState()
        Me.OnConnectionChanged(System.EventArgs.Empty)
    End Sub

    ''' <summary> Event handler. Called when device is closing. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    Private Sub DeviceClosing(ByVal sender As Object, ByVal e As System.EventArgs) Handles _Device.Closing
        If Me.IsDeviceOpen Then
        End If
    End Sub

    ''' <summary> Event handler. Called when device is closed. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    Private Sub DeviceClosed(ByVal sender As Object, ByVal e As System.EventArgs) Handles _Device.Closed
        Me.OnConnectionChanged(System.EventArgs.Empty)
    End Sub

#End Region

#Region " EVENT HANDLERS "

    ''' <summary> Raised to update the instrument connection ExecutionState. </summary>
    ''' <param name="sender">Specifies reference to the <see cref="TspSystem">TSP system.</see>.
    ''' </param>
    ''' <param name="e">Specifies the <see cref="EventArgs">event arguments</see>.
    ''' </param>
    Public Event ConnectionChanged As EventHandler(Of EventArgs)

    ''' <summary> Raises an event to alert on change of connection. </summary>
    ''' <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Public Sub OnConnectionChanged(ByVal e As System.EventArgs)
        Dim evt As EventHandler(Of EventArgs) = Me.ConnectionChangedEvent
        evt?.Invoke(Me, e)
    End Sub

#End Region

End Class
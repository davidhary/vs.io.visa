﻿Imports NationalInstruments
Imports isr.Core.Pith.ExceptionExtensions
''' <summary> Implements a TSP based device. Defines the I/O driver for accessing the master node
''' of a TSP Linked system. </summary>
''' <license> (c) 2009 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="11/6/2013" by="David" revision=""> Based on legacy TSP library. </history>
''' <history date="02/21/2009" by="David" revision="3.0.3339.x"> Created </history>
Public MustInherit Class MasterDeviceBase
    Inherits Visa.DeviceBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="MasterDeviceBase" /> class. </summary>
    Protected Sub New()
        MyBase.New()
        Me.InitializeTimeout = TimeSpan.FromMilliseconds(30000)
        Me.ResourcesSearchPattern = Visa.InstrumentSearchPattern(VisaNS.HardwareInterfaceType.Gpib)
    End Sub

#Region " I Disposable Support"

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <param name="disposing"> <c>True</c> if this method releases both managed and unmanaged resources;
    ''' <c>False</c> if this method releases only unmanaged resources. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Overrides Sub Dispose(disposing As Boolean)
        Try
            If Not MyBase.IsDisposed Then
                If disposing Then
                    ' dispose managed state (managed objects).
                    Me.OnClosing(New System.ComponentModel.CancelEventArgs)
                End If
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, "Exception disposing device", "Exception {0}", ex.ToFullBlownString)
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Clears the active state. Issues selective device clear. </summary>
    Public Overrides Sub ClearActiveState()
        Me.StatusSubsystem.ClearActiveState()
    End Sub

    ''' <summary> Initializes the Device. Performs a reset and additional custom setting by the parent
    ''' Devices. </summary>
    Public Overrides Sub InitializeKnownState()
        MyBase.InitializeKnownState()

        ' establish the current node as the controller node. 
        Me.StatusSubsystem.InitiateControllerNode()

    End Sub

#End Region

#Region " SESSION "

    Private _IsDeviceOpen As Boolean
    ''' <summary> Gets or sets a value indicating whether the device is open. This is
    '''           required when the device is used in emulation. </summary>
    ''' <value> <c>True</c> if the device has an open session; otherwise, <c>False</c>. </value>
    Public Overrides Property IsDeviceOpen As Boolean
        Get
            Return Me._IsDeviceOpen
        End Get
        Set(ByVal value As Boolean)
            If Not Me.IsDeviceOpen.Equals(value) Then
                Me._IsDeviceOpen = value
                Me.SafePostPropertyChanged("IsDeviceOpen")
                If Me.StatusSubsystem IsNot Nothing Then
                    Me.StatusSubsystem.IsDeviceOpen = Me.IsDeviceOpen
                End If
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the session. </summary>
    ''' <value> The session. </value>
    Public Shadows Property Session As TspSession

    ''' <summary> Creates the session. </summary>
    ''' <param name="resourceName"> Name of the resource. </param>
    Public Overrides Sub CreateSession(ByVal resourceName As String)
        Me.Session = New TspSession(resourceName)
        MyBase.Session = Me.Session
    End Sub

    ''' <summary> Allows the derived device to take actions after closing. </summary>
    Protected Overrides Sub OnClosed()
        MyBase.OnClosed()
    End Sub

    ''' <summary> Allows the derived device to take actions before closing. Removes subsystems and
    ''' event handlers. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Overrides Sub OnClosing(e As System.ComponentModel.CancelEventArgs)
        MyBase.OnClosing(e)
        If e Is Nothing OrElse Not e.Cancel Then

            If Me.SourceMeasureUnit IsNot Nothing Then
                Try
                    RemoveHandler Me.SourceMeasureUnit.TraceMessageAvailable, AddressOf OnTraceMessageAvailable
                    Me._SourceMeasureUnit.Dispose()
                    Me._SourceMeasureUnit = Nothing
                Catch ex As Exception
                    Debug.Assert(Not Debugger.IsAttached, "Exception disposing", "{0}", ex.ToFullBlownString)
                End Try
            End If

            If Me.ScriptManager IsNot Nothing Then
                Try
                    RemoveHandler Me.ScriptManager.TraceMessageAvailable, AddressOf OnTraceMessageAvailable
                    Me._ScriptManager.Dispose()
                    Me._ScriptManager = Nothing
                Catch ex As Exception
                    Debug.Assert(Not Debugger.IsAttached, "Exception disposing", "{0}", ex.ToFullBlownString)
                End Try
            End If

            If Me.DisplaySubsystem IsNot Nothing Then
                Try
                    RemoveHandler Me.DisplaySubsystem.TraceMessageAvailable, AddressOf OnTraceMessageAvailable
                    Me._DisplaySubsystem.Dispose()
                    Me._DisplaySubsystem = Nothing
                Catch ex As Exception
                    Debug.Assert(Not Debugger.IsAttached, "Exception disposing", "{0}", ex.ToFullBlownString)
                End Try
            End If

            If Me._SystemSubsystem IsNot Nothing Then
                Try
                    RemoveHandler Me.SystemSubsystem.TraceMessageAvailable, AddressOf OnTraceMessageAvailable
                    Me._SystemSubsystem.Dispose()
                    Me._SystemSubsystem = Nothing
                Catch ex As Exception
                    Debug.Assert(Not Debugger.IsAttached, "Exception disposing", "{0}", ex.ToFullBlownString)
                End Try
            End If

            If Me._StatusSubsystem IsNot Nothing Then
                Try
                    ' turn off prompts
                    Me.StatusSubsystem.WriteShowPrompts(False)
                    Me.StatusSubsystem.WriteShowErrors(False)
                Catch ex As Exception
                    Debug.Assert(Not Debugger.IsAttached, "Exception turning of prompts", "{0}", ex.ToFullBlownString)
                End Try
                Try
                    ' set the state to closed
                    Me.StatusSubsystem.ExecutionState = TspExecutionState.Closed
                    Me.StatusSubsystem.IsDeviceOpen = False
                    RemoveHandler Me.StatusSubsystem.PropertyChanged, AddressOf StatusSubsystemPropertyChanged
                    RemoveHandler Me.StatusSubsystem.TraceMessageAvailable, AddressOf OnTraceMessageAvailable
                    Me._StatusSubsystem.Dispose()
                    Me._StatusSubsystem = Nothing
                Catch ex As Exception
                    Debug.Assert(Not Debugger.IsAttached, "Exception disposing", "{0}", ex.ToFullBlownString)
                End Try
            End If
        End If
    End Sub

    ''' <summary> Allows the derived device to take actions after opening. Adds subsystems and event
    ''' handlers. </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Overrides Sub OnOpened()

        ' prevent adding multiple times
        If MyBase.Subsystems Is Nothing OrElse MyBase.Subsystems.Count = 0 Then
            Debug.Assert(Not Debugger.IsAttached, "Subsystems not defined.")
        Else
            AddHandler Me.StatusSubsystem.TraceMessageAvailable, AddressOf OnTraceMessageAvailable
            AddHandler Me.SystemSubsystem.TraceMessageAvailable, AddressOf OnTraceMessageAvailable
            AddHandler Me.DisplaySubsystem.TraceMessageAvailable, AddressOf OnTraceMessageAvailable
            AddHandler Me.ScriptManager.TraceMessageAvailable, AddressOf OnTraceMessageAvailable
            AddHandler Me.SourceMeasureUnit.TraceMessageAvailable, AddressOf OnTraceMessageAvailable
        End If

        ' allow connection time to materialize
        Threading.Thread.Sleep(100)

        Me.StatusSubsystem.EnableServiceRequest(ServiceRequests.None)
        Me.StatusSubsystem.ProcessExecutionStateEnabled = True
        Try
            If Me.IsSessionOpen Then
                Me.Session.StoreTimeout(Me.InitializeTimeout)
                Me.OnTraceMessageAvailable(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Clearing error queue;. ")
                ' clear the error queue on the controller node only.
                Me.StatusSubsystem.ClearErrorQueue()
            End If
        Catch ex As Exception
            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Exception ignored clearing error queue;. {0}", ex.ToFullBlownString)
        Finally
            If Me.IsSessionOpen Then
                Me.Session.RestoreTimeout()
            End If
        End Try

        Try
            If Me.IsSessionOpen Then
                Me.Session.StoreTimeout(Me.InitializeTimeout)
                ' turn prompts off.  This may not be necessary.
                Me.StatusSubsystem.TurnPromptsErrorsOff()
            End If
        Catch ex As Exception
            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Exception ignored turning off prompts;. {0}", ex.ToFullBlownString)
        Finally
            If Me.IsSessionOpen Then
                Me.Session.RestoreTimeout()
            End If
        End Try

        Try
            ' flush the input buffer in case the instrument has some leftovers.
            If Me.IsSessionOpen Then
                Me.Session.DiscardUnreadData()
                If Not String.IsNullOrWhiteSpace(Me.Session.DiscardedData) Then
                    Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Data discarded after turning prompts and errors off;. Data: {0}.", Me.Session.DiscardedData)
                End If
            End If
        Catch ex As VisaException
            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception ignored clearing read buffer;. {0}", ex.ToFullBlownString)
        End Try

        Try
            ' flush write may cause the instrument to send off a new data.
            If Me.IsSessionOpen Then
                Me.Session.DiscardUnreadData()
                If Not String.IsNullOrWhiteSpace(Me.Session.DiscardedData) Then
                    Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                               "Unread data discarded after discarding unset data;. Data: {0}.", Me.Session.DiscardedData)
                End If
            End If
        Catch ex As VisaException
            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception ignored clearing read buffer;. {0}", ex.ToFullBlownString)
        End Try

        Try
            MyBase.OnOpened()
        Catch ex As InvalidCastException
            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Failed initiating controller node--closing this session;. {0}", ex.ToFullBlownString)
            Me.CloseSession()
        Catch ex As FormatException
            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Failed initiating controller node--closing this session;. {0}", ex.ToFullBlownString)
            Me.CloseSession()
        Catch ex As OperationFailedException
            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Failed initiating controller node--closing this session;. {0}", ex.ToFullBlownString)
            Me.CloseSession()
        Catch ex As VisaException
            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Failed initiating controller node--closing this session;. {0}", ex.ToFullBlownString)
            Me.CloseSession()
        End Try

    End Sub

    ''' <summary> Allows the derived device to take actions before opening. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overrides Sub OnOpening(e As System.ComponentModel.CancelEventArgs)
        MyBase.OnOpening(e)
    End Sub

#End Region

#Region " SUBSYSTEMS "

#Region " STATUS "

    ''' <summary> Gets or sets the Status Subsystem. </summary>
    ''' <value> The Status Subsystem. </value>
    Public Property StatusSubsystem As StatusSubsystemBase

    ''' <summary> Status subsystem property changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub StatusSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        Try
            If sender IsNot Nothing AndAlso
                e IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(e.PropertyName) Then
                Dim subsystem As StatusSubsystemBase = CType(sender, StatusSubsystemBase)
                Select Case e.PropertyName
                    Case "Identity"
                        If Not String.IsNullOrWhiteSpace(subsystem.Identity) Then
                            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "{0} identified as {1}.",
                                                       Me.ResourceName, subsystem.Identity)

                        End If
#If False Then
                    Case "DeviceErrors"
                    Case "ErrorAvailable"
                    Case "ErrorAvailableBits"
                    Case "MeasurementAvailable"
                    Case "MeasurementAvailableBits"
                    Case "MeasurementEventCondition"
                    Case "MeasurementEventEnableBitmask"
                    Case "MeasurementEventStatus"
                    Case "MessageAvailable"
                    Case "MessageAvailableBits"
                    Case "OperationEventCondition"
                    Case "OperationEventEnableBitmask"
                    Case "OperationEventStatus"
                    Case "OperationNegativeTransitionEventEnableBitmask"
                    Case "OperationPositiveTransitionEventEnableBitmask"
                    Case "QuestionableEventCondition"
                    Case "QuestionableEventEnableBitmask"
                    Case "QuestionableEventStatus"
                    Case "ServiceRequestEnableBitmask"
                    Case "ServiceRequestStatus"
                    Case "StandardDeviceErrorAvailable"
                    Case "StandardDeviceErrorAvailableBits"
                    Case "StandardEventEnableBitmask"
                    Case "StandardEventStatus"
#End If
                End Select
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, "Exception handling property", "Exception handling '{0}' property change. {1}.", e.PropertyName, ex.ToFullBlownString)
        End Try
    End Sub

#End Region

    ''' <summary>
    ''' Gets or sets the System Subsystem.
    ''' </summary>
    ''' <value>The System Subsystem.</value>
    Public Property SystemSubsystem As SystemSubsystemBase

    ''' <summary> Gets or sets the Display Subsystem. </summary>
    ''' <value> Display Subsystem. </value>
    Public Property DisplaySubsystem As DisplaySubsystemBase

    ''' <summary> Gets or sets the TSP Script Manager. </summary>
    ''' <value> TSP Script Manager. </value>
    Public Property ScriptManager As ScriptManagerBase

    ''' <summary> Gets or sets the Smu Subsystem. </summary>
    ''' <value> Smu Subsystem. </value>
    Public Property SourceMeasureUnit As IO.Visa.Tsp.SourceMeasureUnit

#End Region

#Region " CHECK CONTACTS "

    ''' <summary> Checks contact resistance. </summary>
    Public Sub CheckContacts(ByVal threshold As Integer)
        Me.SourceMeasureUnit.CheckContacts(threshold)
        If Not Me.SourceMeasureUnit.ContactCheckOkay.HasValue Then
            Throw New OperationFailedException("Failed Measuring contacts;. ")
        ElseIf Not Me.SourceMeasureUnit.ContactCheckOkay.Value Then
            Throw New OperationFailedException("High contact resistances;. Values: '{0}'", Me.SourceMeasureUnit.ContactResistances)
        End If
    End Sub

    ''' <summary> Checks contact resistance. </summary>
    ''' <param name="threshold"> The threshold. </param>
    ''' <param name="details">   [in,out] The details. </param>
    ''' <returns> <c>True</c> if contacts checked okay. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="1#")>
    Public Function TryCheckContacts(ByVal threshold As Integer, ByRef details As String) As Boolean
        Me.SourceMeasureUnit.CheckContacts(threshold)
        If Not Me.SourceMeasureUnit.ContactCheckOkay.HasValue Then
            details = "Failed Measuring contacts;. "
            Return False
        ElseIf Me.SourceMeasureUnit.ContactCheckOkay.Value Then
            Return True
        Else
            details = String.Format("High contact resistances;. Values: '{0}'",
                                    Me.SourceMeasureUnit.ContactResistances)
            Return False
        End If
    End Function

#End Region

#Region " SERVICE REQUEST "

    ''' <summary> Reads the event registers after receiving a service request. </summary>
    Protected Overrides Sub ProcessServiceRequest()
        Me.StatusSubsystem.ReadRegisters()
        If Me.StatusSubsystem.ErrorAvailable Then
            Me.SystemSubsystem.QueryLastError()
        End If
    End Sub

#End Region

End Class


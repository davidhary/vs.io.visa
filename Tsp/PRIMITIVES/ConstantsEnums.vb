Imports System
Imports System.ComponentModel

#Region " TSP TYPES "

''' <summary> Specifies the contact check speed modes. </summary>
Public Enum ContactCheckSpeedMode
    <ComponentModel.Description("None")> None
    <ComponentModel.Description("Fast (CONTACT_FAST)")> Fast
    <ComponentModel.Description("Medium (CONTACT_MEDIUM)")> Medium
    <ComponentModel.Description("Slow (CONTACT_SLOW)")> Slow
End Enum

''' <summary>Enumerates the status bits as defined in reading buffers.</summary>
<Flags()> Public Enum OperationEventBits
    <ComponentModel.Description("Empty")> None = 0

    ''' <summary>Calibrating.</summary>
    <ComponentModel.Description("Calibrating")> Calibrating = &H1

    ''' <summary>Measuring.</summary>
    <ComponentModel.Description("Measuring")> Measuring = &H10

    ''' <summary>Prompts enabled.</summary>
    <ComponentModel.Description("Prompts Enabled")> Prompts = &H800

    ''' <summary>User Register.</summary>
    <ComponentModel.Description("User Register")> UserRegister = &H1000

    ''' <summary>User Register.</summary>
    <ComponentModel.Description("Instrument summary")> InstrumentSummary = &H2000

    ''' <summary>Program running.</summary>
    <ComponentModel.Description("Program Running")> ProgramRunning = &H4000

    ''' <summary>Unknown value. Sets bit 16 (zero based and beyond the register size).</summary>
    <ComponentModel.Description("Unknown")> Unknown = &H10000

End Enum


''' <summary>Enumerates the status bits as defined in reading buffers.</summary>
<Flags()> Public Enum BufferStatusBits
    <ComponentModel.Description("Not defined")> None = 0
    ''' <summary>0x02 over temperature condition.</summary>
    <ComponentModel.Description("Over Temp")> OverTemp = 2
    ''' <summary>0x04 measure range was auto ranged.</summary>
    <ComponentModel.Description("Auto Range Measure")> AutoRangeMeasure = 4
    ''' <summary>0x08 source range was auto ranged.</summary>
    <ComponentModel.Description("Auto Range Source")> AutoRangeSource = 8
    ''' <summary>0x10 4W (remote) sense mode.</summary>
    <ComponentModel.Description("Four Wire")> FourWire = 16
    ''' <summary>0x20 relative applied to reading.</summary>
    <ComponentModel.Description("Relative")> Relative = 32
    ''' <summary>0x40 source function in compliance.</summary>
    <ComponentModel.Description("Compliance")> Compliance = 64
    ''' <summary>0x80 reading was filtered.</summary>
    <ComponentModel.Description("Filtered")> Filtered = 128
End Enum

''' <summary> Enumerates the TSP Execution State. </summary>
Public Enum TspExecutionState
    ''' <summary> Not defined. </summary>
    <ComponentModel.Description("Not defined")> None
    ''' <summary> Closed. </summary>
    <ComponentModel.Description("Closed")> Closed
    ''' <summary> Received the continuation prompt.
    ''' Send between lines when loading a script indicating that
    ''' TSP received script line successfully and is waiting for next line
    ''' or the end script command. </summary>
    <ComponentModel.Description("Continuation")> IdleContinuation
    ''' <summary> Received the error prompt. Error occurred; 
    '''           handle as desired. Use �errorqueue� commands to read and clear errors. </summary>
    <ComponentModel.Description("Error")> IdleError
    ''' <summary> Received the ready prompt. For example, TSP received script successfully and is ready for next command. </summary>
    <ComponentModel.Description("Ready")> IdleReady
    ''' <summary> A command was sent to the instrument. </summary>
    <ComponentModel.Description("Processing")> Processing
    ''' <summary> Cannot tell because prompt are off. </summary>
    <ComponentModel.Description("Unknown")> Unknown
End Enum

''' <summary> Enumerates the TSP error levels. </summary>
Public Enum TspErrorLevel
    ''' <summary> Indicates no error: �Queue is Empty�. </summary>
    <ComponentModel.Description("None")> None = 0
    ''' <summary> Indicates an event or a minor error.
    ''' Examples: �Reading Available� and �Reading Overflow�. </summary>
    <ComponentModel.Description("Informational")> Informational = 10
    ''' <summary> Indicates possible invalid user input.
    ''' Operation will continue but action should be taken to correct the error.
    ''' Examples: �Exponent Too Large� and �Numeric Data Not Allowed�. </summary>
    <ComponentModel.Description("Recoverable")> Recoverable = 20
    ''' <summary> Indicates a serious error and may require technical assistance.
    ''' Example: �Saved calibration constants corrupted�. </summary>
    <ComponentModel.Description("Serious")> Serious = 30
    ''' <summary> Indicates that the Series 2600 is non-operational and will
    ''' require service. Contact information for service is provided in Section 1.
    ''' Examples: �Bad SMU A FPGA image size�, �SMU is unresponsive� and
    ''' �Communication Timeout with D FPGA�. </summary>
    <ComponentModel.Description("Fatal")> Fatal = 40
End Enum

''' <summary>Enumerates the synchronizer Execution State.
''' </summary>
Public Enum SyncExecutionState
    <ComponentModel.Description("Not Defined")> None
    <ComponentModel.Description("Idle")> Idle
    <ComponentModel.Description("Primed")> Primed
    <ComponentModel.Description("Active")> Active
    <ComponentModel.Description("Finished")> Finished
    <ComponentModel.Description("Aborted")> Aborted
End Enum

''' <summary>Enumerates the content type of a TSP chunk line.
''' </summary>
Public Enum TspChunkLineContentType
    <ComponentModel.Description("Not Defined")> None
    <ComponentModel.Description("Start Comment Block")> StartCommentBlock
    <ComponentModel.Description("End Comment Block")> EndCommentBlock
    <ComponentModel.Description("Comment")> Comment
    <ComponentModel.Description("Syntax")> Syntax
    <ComponentModel.Description("Syntax and Start Comment Block")> SyntaxStartCommentBlock
    <ComponentModel.Description("Chunk Name Declaration")> ChunkNameDeclaration
    <ComponentModel.Description("Chunk Name Requirement")> ChunkNameRequire
    <ComponentModel.Description("Chunk Name Loaded")> ChunkNameLoaded
End Enum

#End Region

#Region " TSP MESSAGE TYPES "

''' <summary>Enumerates the TSP Message Type.  This is set as system flags to
''' allow combining types for detecting a super structure.  For example,
''' data available is the sum of binary and ASCII data available bits.</summary>
<Flags()> Public Enum TspMessageTypes
    ''' <summary>0x0000. not defined.</summary>
    <ComponentModel.Description("Not Defined")> None = 0
    ''' <summary>0x001. Sync Instrument message.</summary>
    <ComponentModel.Description("Sync Status")> SyncStatus = CInt(2 ^ 0)
End Enum

''' <summary>Enumerates the TSP Instrument status.</summary>
<Flags()> Public Enum TspMessageInfoBits
    ''' <summary>0x0000. not defined.</summary>
    <ComponentModel.Description("Not Defined")> None = 0
    ''' <summary>0x0001. New message.  Not yet handled.</summary>
    <ComponentModel.Description("New Message")> NewMessage = CInt(2 ^ 0)
    ''' <summary>0x0002. Debug message.  Debug messages always have
    ''' Contents following the information register.
    ''' .</summary>
    <ComponentModel.Description("Debug Message")> DebugMessage = CInt(2 ^ 1)
    ''' <summary>0x0004. Non zero if unit or node is still active.</summary>
    <ComponentModel.Description("Tsp Active")> TspActive = CInt(2 ^ 2)
    ''' <summary>0x0008. Non zero if unit or node had an error.</summary>
    <ComponentModel.Description("Tsp Error")> TspError = CInt(2 ^ 3)
    ''' <summary>0x0010. Source function in compliance.</summary>
    <ComponentModel.Description("Compliance")> Compliance = CInt(2 ^ 4)
    ''' <summary>0x0020. Non zero if unit has taken new �chunks� of data in its
    ''' buffer.</summary>
    <ComponentModel.Description("Data Available")> DataAvailable = CInt(2 ^ 5)
End Enum

#End Region

''' <summary>Enumerates the display screens and special status.</summary>
<Flags()> Public Enum DisplayScreens

    ''' <summary>Not defined.</summary>
    <System.ComponentModel.Description("Not defined")> None = 0

    ''' <summary>Custom lines are displayed.</summary>
    <System.ComponentModel.Description("Default screen")> [Default] = 1

    ''' <summary>Cleared user screen mode.</summary>
    <System.ComponentModel.Description("User screen")> User = 128

    ''' <summary>Last command displayed title.</summary>
    <System.ComponentModel.Description("Title is displayed")> Title = 256

    ''' <summary>Custom lines are displayed.</summary>
    <System.ComponentModel.Description("Special display")> Custom = 512

    ''' <summary>Measurement displayed.</summary>
    <System.ComponentModel.Description("Measurement is displayed")> Measurement = 1024

End Enum

''' <summary>Enumerates the instrument model families.</summary>
Public Enum InstrumentModelFamily

    ''' <summary>Not defined.</summary>
    <System.ComponentModel.Description("Not defined")> None = 0

    ''' <summary>26xx Source Meters.</summary>
    <System.ComponentModel.Description("26xx Source Meters")> K2600 = 1

    ''' <summary>26xxA Source Meters.</summary>
    <System.ComponentModel.Description("26xxA Source Meters")> K2600A = 2

    ''' <summary>37xx Switch Systems.</summary>
    <System.ComponentModel.Description("37xx Switch Systems")> K3700 = 3

End Enum

Public Module InstrumentModel

    ''' <summary> Returns <c>True</c> if the <paramref name="model">model</paramref> matches the mask. </summary>
    ''' <param name="model"> Actual mode. </param>
    ''' <param name="mask">  Mode mask using '%' to signify ignored characters and * to specify
    ''' wildcard suffix. </param>
    ''' <returns> Returns <c>True</c> if the <paramref name="model">model</paramref> matches the mask. </returns>
    Public Function IsModelMatch(ByVal model As String, ByVal mask As String) As Boolean

        Dim wildcard As Char = "*"c
        Dim ignore As Char = "%"c
        If String.IsNullOrWhiteSpace(mask) Then
            Return True
        ElseIf String.IsNullOrWhiteSpace(model) Then
            Return False
        ElseIf mask.Contains(wildcard) Then
            Dim length As Integer = mask.IndexOf(wildcard)
            Dim m As Char() = mask.Substring(0, length).ToCharArray
            Dim candidate As Char() = model.Substring(0, length).ToCharArray
            For i As Integer = 0 To m.Length - 1
                Dim c As Char = m(i)
                If c <> ignore AndAlso c <> candidate(i) Then
                    Return False
                End If
            Next
        ElseIf mask.Length <> model.Length Then
            Return False
        Else
            Dim m As Char() = mask.ToCharArray
            Dim candidate As Char() = model.ToCharArray
            For i As Integer = 0 To m.Length - 1
                Dim c As Char = m(i)
                If c <> ignore AndAlso c <> candidate(i) Then
                    Return False
                End If
            Next
        End If
        Return True
    End Function


End Module
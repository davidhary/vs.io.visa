Imports NationalInstruments.VisaNS

Namespace NationalInstruments.Examples.SimpleAsynchronousReadWrite

    Public Class MainForm
        Inherits FormBase

#Region " Windows Form Designer generated code "

        Public Sub New()
            'This call is required by the Windows Form Designer.
            InitializeComponent()

            'Add any initialization after the InitializeComponent() call
        End Sub

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If Not (components Is Nothing) Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer
        Private mbSession As MessageBasedSession
        Private lastResourceString As String = Nothing
        Private asyncHandle As IAsyncResult = Nothing
        Private WithEvents WriteTextBox As System.Windows.Forms.TextBox
        Private WithEvents ReadTextBox As System.Windows.Forms.TextBox

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Private WithEvents WriteButton As System.Windows.Forms.Button
        Private WithEvents ReadButton As System.Windows.Forms.Button
        Private WithEvents OpenSessionButton As System.Windows.Forms.Button
        Private WithEvents ClearButton As System.Windows.Forms.Button
        Private WithEvents CloseSessionButton As System.Windows.Forms.Button
        Private WithEvents StringToWriteLabel As System.Windows.Forms.Label
        Private WithEvents StringToReadLabel As System.Windows.Forms.Label
        Private WithEvents TerminateButton As System.Windows.Forms.Button
        Private WithEvents ElementsTransferredLabel As System.Windows.Forms.Label
        Private WithEvents ElementsTransferredTextBox As System.Windows.Forms.TextBox
        Private WithEvents LastIOStatusTextBox As System.Windows.Forms.TextBox
        Private WithEvents LastIOStatusLabel As System.Windows.Forms.Label

        <System.Diagnostics.DebuggerStepThrough()> 
        Private Sub InitializeComponent()
            Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(MainForm))
            Me.writeButton = New System.Windows.Forms.Button
            Me.readButton = New System.Windows.Forms.Button
            Me.openSessionButton = New System.Windows.Forms.Button
            Me.writeTextBox = New System.Windows.Forms.TextBox
            Me.readTextBox = New System.Windows.Forms.TextBox
            Me.clearButton = New System.Windows.Forms.Button
            Me.closeSessionButton = New System.Windows.Forms.Button
            Me.stringToWriteLabel = New System.Windows.Forms.Label
            Me.stringToReadLabel = New System.Windows.Forms.Label
            Me.terminateButton = New System.Windows.Forms.Button
            Me.elementsTransferredLabel = New System.Windows.Forms.Label
            Me.elementsTransferredTextBox = New System.Windows.Forms.TextBox
            Me.lastIOStatusTextBox = New System.Windows.Forms.TextBox
            Me.lastIOStatusLabel = New System.Windows.Forms.Label
            Me.SuspendLayout()
            '
            'writeButton
            '
            Me.writeButton.Enabled = False
            Me.writeButton.Location = New System.Drawing.Point(5, 83)
            Me.writeButton.Name = "writeButton"
            Me.writeButton.Size = New System.Drawing.Size(74, 23)
            Me.writeButton.TabIndex = 3
            Me.writeButton.Text = "Write"
            '
            'readButton
            '
            Me.readButton.Enabled = False
            Me.readButton.Location = New System.Drawing.Point(79, 83)
            Me.readButton.Name = "readButton"
            Me.readButton.Size = New System.Drawing.Size(74, 23)
            Me.readButton.TabIndex = 4
            Me.readButton.Text = "Read"
            '
            'openSessionButton
            '
            Me.openSessionButton.Location = New System.Drawing.Point(5, 5)
            Me.openSessionButton.Name = "openSessionButton"
            Me.openSessionButton.Size = New System.Drawing.Size(92, 22)
            Me.openSessionButton.TabIndex = 0
            Me.openSessionButton.Text = "Open Session"
            '
            'writeTextBox
            '
            Me.writeTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or
                                            System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.writeTextBox.Enabled = False
            Me.writeTextBox.Location = New System.Drawing.Point(5, 54)
            Me.writeTextBox.Name = "writeTextBox"
            Me.writeTextBox.Size = New System.Drawing.Size(275, 20)
            Me.writeTextBox.TabIndex = 2
            Me.writeTextBox.Text = "*IDN?\n"
            '
            'readTextBox
            '
            Me.readTextBox.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) Or
                                            System.Windows.Forms.AnchorStyles.Left) Or
                                        System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.readTextBox.Location = New System.Drawing.Point(5, 136)
            Me.readTextBox.Multiline = True
            Me.readTextBox.Name = "readTextBox"
            Me.readTextBox.ReadOnly = True
            Me.readTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
            Me.readTextBox.Size = New System.Drawing.Size(275, 158)
            Me.readTextBox.TabIndex = 6
            Me.readTextBox.TabStop = False
            Me.readTextBox.Text = ""
            '
            'clearButton
            '
            Me.clearButton.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) Or
                                           System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.clearButton.Enabled = False
            Me.clearButton.Location = New System.Drawing.Point(6, 344)
            Me.clearButton.Name = "clearButton"
            Me.clearButton.Size = New System.Drawing.Size(275, 24)
            Me.clearButton.TabIndex = 6
            Me.clearButton.Text = "Clear"
            '
            'closeSessionButton
            '
            Me.closeSessionButton.Enabled = False
            Me.closeSessionButton.Location = New System.Drawing.Point(97, 5)
            Me.closeSessionButton.Name = "closeSessionButton"
            Me.closeSessionButton.Size = New System.Drawing.Size(92, 22)
            Me.closeSessionButton.TabIndex = 1
            Me.closeSessionButton.Text = "Close Session"
            '
            'stringToWriteLabel
            '
            Me.stringToWriteLabel.Location = New System.Drawing.Point(5, 40)
            Me.stringToWriteLabel.Name = "stringToWriteLabel"
            Me.stringToWriteLabel.Size = New System.Drawing.Size(91, 14)
            Me.stringToWriteLabel.TabIndex = 8
            Me.stringToWriteLabel.Text = "String to Write:"
            '
            'stringToReadLabel
            '
            Me.stringToReadLabel.Location = New System.Drawing.Point(5, 122)
            Me.stringToReadLabel.Name = "stringToReadLabel"
            Me.stringToReadLabel.Size = New System.Drawing.Size(101, 14)
            Me.stringToReadLabel.TabIndex = 9
            Me.stringToReadLabel.Text = "String to Read:"
            '
            'terminateButton
            '
            Me.terminateButton.Enabled = False
            Me.terminateButton.Location = New System.Drawing.Point(205, 83)
            Me.terminateButton.Name = "terminateButton"
            Me.terminateButton.Size = New System.Drawing.Size(74, 23)
            Me.terminateButton.TabIndex = 5
            Me.terminateButton.Text = "Terminate"
            '
            'elementsTransferredLabel
            '
            Me.elementsTransferredLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
            Me.elementsTransferredLabel.Location = New System.Drawing.Point(5, 308)
            Me.elementsTransferredLabel.Name = "elementsTransferredLabel"
            Me.elementsTransferredLabel.Size = New System.Drawing.Size(116, 11)
            Me.elementsTransferredLabel.TabIndex = 11
            Me.elementsTransferredLabel.Text = "Elements Transferred:"
            '
            'elementsTransferredTextBox
            '
            Me.elementsTransferredTextBox.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
            Me.elementsTransferredTextBox.Location = New System.Drawing.Point(5, 321)
            Me.elementsTransferredTextBox.Name = "elementsTransferredTextBox"
            Me.elementsTransferredTextBox.ReadOnly = True
            Me.elementsTransferredTextBox.Size = New System.Drawing.Size(104, 20)
            Me.elementsTransferredTextBox.TabIndex = 12
            Me.elementsTransferredTextBox.TabStop = False
            Me.elementsTransferredTextBox.Text = ""
            '
            'lastIOStatusTextBox
            '
            Me.lastIOStatusTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) Or
                                                   System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lastIOStatusTextBox.Location = New System.Drawing.Point(113, 321)
            Me.lastIOStatusTextBox.Name = "lastIOStatusTextBox"
            Me.lastIOStatusTextBox.ReadOnly = True
            Me.lastIOStatusTextBox.Size = New System.Drawing.Size(168, 20)
            Me.lastIOStatusTextBox.TabIndex = 14
            Me.lastIOStatusTextBox.TabStop = False
            Me.lastIOStatusTextBox.Text = ""
            '
            'lastIOStatusLabel
            '
            Me.lastIOStatusLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
            Me.lastIOStatusLabel.Location = New System.Drawing.Point(113, 308)
            Me.lastIOStatusLabel.Name = "lastIOStatusLabel"
            Me.lastIOStatusLabel.Size = New System.Drawing.Size(116, 11)
            Me.lastIOStatusLabel.TabIndex = 13
            Me.lastIOStatusLabel.Text = "Last I/O Status:"
            '
            'MainForm
            '
            Me.ClientSize = New System.Drawing.Size(292, 376)
            Me.Controls.Add(Me.lastIOStatusTextBox)
            Me.Controls.Add(Me.lastIOStatusLabel)
            Me.Controls.Add(Me.elementsTransferredTextBox)
            Me.Controls.Add(Me.elementsTransferredLabel)
            Me.Controls.Add(Me.terminateButton)
            Me.Controls.Add(Me.stringToReadLabel)
            Me.Controls.Add(Me.stringToWriteLabel)
            Me.Controls.Add(Me.closeSessionButton)
            Me.Controls.Add(Me.clearButton)
            Me.Controls.Add(Me.readTextBox)
            Me.Controls.Add(Me.writeTextBox)
            Me.Controls.Add(Me.openSessionButton)
            Me.Controls.Add(Me.readButton)
            Me.Controls.Add(Me.writeButton)
            Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
            Me.MaximizeBox = False
            Me.MinimumSize = New System.Drawing.Size(295, 403)
            Me.Name = "MainForm"
            Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
            Me.Text = "Simple Asynchronous Read/Write"
            Me.ResumeLayout(False)

        End Sub

#End Region

        <STAThread()> 
        Public Shared Sub Main()
            Application.Run(New MainForm)
        End Sub

        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub OpenSession_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openSessionButton.Click
            Using sr As New SelectResource
                If lastResourceString <> Nothing Then
                    sr.ResourceName = lastResourceString
                End If
                Dim result As DialogResult = sr.ShowDialog(Me)
                If result = Windows.Forms.DialogResult.OK Then
                    lastResourceString = sr.ResourceName
                    Windows.Forms.Cursor.Current = Cursors.WaitCursor
                    Try
                        mbSession = CType(ResourceManager.GetLocalManager().Open(sr.ResourceName), MessageBasedSession)
#If NETFX2_0 Then
                        'For .NET Framework 2.0, use SynchronizeCallbacks to specify that the object 
                        'marshals callbacks across threads appropriately.
                        mbSession.SynchronizeCallbacks = True
#Else
                    'For .NET Framework 1.1, set SynchronizingObject to the Windows Form to specify 
                    'that the object marshals callbacks across threads appropriately.
                    mbSession.SynchronizingObject = Me
#End If
                        SetupControlState(True)
                    Catch exp As InvalidCastException
                        MessageBox.Show("Resource selected must be a message-based session", "Incompatible Resource", MessageBoxButtons.OK,
                                    MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
                    Catch exp As Exception
                        MessageBox.Show(exp.Message, "Exception Occurred", MessageBoxButtons.OK,
                                        MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
                    Finally
                        Windows.Forms.Cursor.Current = Cursors.Default
                    End Try
                End If
            End Using
        End Sub


        Private Sub CloseSession_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles closeSessionButton.Click
            SetupControlState(False)
            mbSession.Dispose()
            mbSession = Nothing
        End Sub

        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub Write_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles writeButton.Click
            Try
                SetupWaitingControlState(True)
                Dim textToWrite As String = ReplaceCommonEscapeSequences(writeTextBox.Text)
                asyncHandle = mbSession.BeginWrite(
                    textToWrite,
                    New AsyncCallback(AddressOf OnWriteComplete),
                    CType(textToWrite.Length, Object))
            Catch exp As Exception
                MessageBox.Show(exp.Message, "Exception Occurred", MessageBoxButtons.OK,
                                MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            End Try
        End Sub


        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub Read_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles readButton.Click
            Try
                SetupWaitingControlState(True)
                asyncHandle = mbSession.BeginRead(
                    mbSession.DefaultBufferSize,
                    New AsyncCallback(AddressOf OnReadComplete),
                    Nothing)
            Catch exp As Exception
                MessageBox.Show(exp.Message, "Exception Occurred", MessageBoxButtons.OK,
                                MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            End Try
        End Sub


        Private Sub Clear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clearButton.Click
            ClearControls()
        End Sub


        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub Terminate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles terminateButton.Click
            SetupWaitingControlState(False)
            Try
                mbSession.Terminate(asyncHandle)
            Catch exp As Exception
                MessageBox.Show(exp.Message, "Exception Occurred", MessageBoxButtons.OK,
                                MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            End Try
        End Sub

        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub OnWriteComplete(ByVal result As IAsyncResult)
            Try
                SetupWaitingControlState(False)
                mbSession.EndWrite(result)
                lastIOStatusTextBox.Text = mbSession.LastStatus.ToString()
                elementsTransferredTextBox.Text = CType(result.AsyncState, Integer).ToString()
            Catch exp As Exception
                MessageBox.Show(exp.Message, "Exception Occurred", MessageBoxButtons.OK,
                                MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            End Try
        End Sub

        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub OnReadComplete(ByVal result As IAsyncResult)
            Try
                SetupWaitingControlState(False)
                lastIOStatusTextBox.Text = mbSession.LastStatus.ToString()
                Dim responseString As String = mbSession.EndReadString(result)
                readTextBox.Text = InsertCommonEscapeSequences(responseString)
                elementsTransferredTextBox.Text = responseString.Length.ToString()
            Catch exp As Exception
                MessageBox.Show(exp.Message, "Exception Occurred", MessageBoxButtons.OK,
                                MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            End Try
        End Sub


        Private Sub SetupControlState(ByVal isSessionOpen As Boolean)
            openSessionButton.Enabled = Not isSessionOpen
            closeSessionButton.Enabled = isSessionOpen
            writeButton.Enabled = isSessionOpen
            readButton.Enabled = isSessionOpen
            writeTextBox.Enabled = isSessionOpen
            clearButton.Enabled = isSessionOpen
            If isSessionOpen Then
                ClearControls()
                writeTextBox.Focus()
            End If
        End Sub


        Private Sub SetupWaitingControlState(ByVal operationIsInProgress As Boolean)
            terminateButton.Enabled = operationIsInProgress
            writeButton.Enabled = Not operationIsInProgress
            readButton.Enabled = Not operationIsInProgress
        End Sub


        Private Shared Function ReplaceCommonEscapeSequences(ByVal s As String) As String
            If (s <> Nothing) Then
                Return s.Replace("\n", vbLf).Replace("\r", vbCr)
            Else
                Return Nothing
            End If
        End Function

        Private Shared Function InsertCommonEscapeSequences(ByVal s As String) As String
            If (s <> Nothing) Then
                Return s.Replace(vbLf, "\n").Replace(vbCr, "\r")
            Else
                Return Nothing
            End If
        End Function


        Private Sub ClearControls()
            readTextBox.Text = String.Empty
            lastIOStatusTextBox.Text = String.Empty
            elementsTransferredTextBox.Text = String.Empty
        End Sub

    End Class

End Namespace
Example Title:           Register Based Operations
                         
Example Filename:        RegisterBasedOperations.sln
                         
Category:                VISA
                         
Description:             This example demonstrates how to use In/Out/MoveIn/MoveOut/Move on register-based 
			 sessions.              
                         
Software Group:          Measurement Studio                          
                         
Required Software:       NI-VISA
                         
Language:                Visual Basic .NET, Visual C#
                         
Language Version:        7.0
                         
Hardware Group:          Any board that supports register-based sessions such as VXI
                         
Driver Name:             Driver for the selected board
                         
Driver Version:          
                         
Required Hardware:       Any board that supports register-based sessions
﻿Namespace ThermoStream

    ''' <summary> Information about the version of a InTest Thermo Stream instrument. </summary>
    ''' <license> (c) 2013 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="9/22/2013" by="David" revision="3.0.5013"> Created. </history>
    Public Class VersionInfo
        Inherits Global.isr.IO.Visa.VersionInfoBase

        ''' <summary> Default constructor. </summary>
        Public Sub New()
            MyBase.new()
        End Sub

        Public Overrides Sub ParseFirmwareRevision(revision As String)
        End Sub

    End Class

End Namespace


Imports NationalInstruments
Imports System.ComponentModel

Namespace ThermoStream

    ''' <summary> Defines a System Subsystem for a InTest Thermo Stream instrument. </summary>
    ''' <license> (c) 2013 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="9/22/2013" by="David" revision="3.0.5013"> Created. </history>
    Public Class SystemSubsystem
        Inherits Visa.Scpi.SystemSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> Initializes a new instance of the <see cref="SystemSubsystem" /> class. </summary>
        ''' <param name="statusSubsystem "> A reference to a <see cref="IO.VISA.StatusSubsystemBase">message based
        ''' session</see>. </param>
        Public Sub New(ByVal statusSubsystem As IO.Visa.StatusSubsystemBase)
            MyBase.New(statusSubsystem)
            Me._AddPresettableValues()
        End Sub

#End Region

#Region " I PRESETTABLE "

        ''' <summary> Adds Presettable values. </summary>
        Private Sub _AddPresettableValues()
            Me._AutoTuningEnabled = New PresettableBoolean("AutoTuningEnabled", New Boolean?)
            MyBase.AddValue(Me._AutoTuningEnabled)
            Me._AutoTuningEnabled.Command = New Command("LRNM", CommandFormats.Standard Or CommandFormats.QuerySupported Or
                                                        CommandFormats.WriteSupported Or CommandFormats.NakedOneZero)
        End Sub

        ''' <summary> Sets the subsystem values to their known execution reset state. </summary>
        Public Overrides Sub ResetKnownState()
            MyBase.ResetKnownState()
            Me.CoolingEnabled = New Boolean?
        End Sub

#End Region

#Region " PUBLISHER "

        ''' <summary> Publishes all values by raising the property changed events. </summary>
        Public Overrides Sub Publish()
            If Me.Publishable Then
                For Each p As Reflection.PropertyInfo In Reflection.MethodInfo.GetCurrentMethod.DeclaringType.GetProperties()
                    Me.SafePostPropertyChanged(p.Name)
                Next
            End If
        End Sub

#End Region

#Region " COMMAND SYNTAX "

        ''' <summary> Clears the error queue and wait. </summary>
        ''' <remarks> Uses 'CLER' and wait 4 seconds after the error is cleared. </remarks>
        Public Sub ClearErrorQueueAndWait()
            If Me.IsDeviceOpen Then
                Me.Session.Write("CLER")
                Dim stopWatch As Diagnostics.Stopwatch = Diagnostics.Stopwatch.StartNew
                Do Until stopWatch.ElapsedMilliseconds() > 4000
                    Windows.Forms.Application.DoEvents()
                Loop
            End If
        End Sub

        ''' <summary> Gets the clear error queue command. </summary>
        ''' <remarks> This supposed to use 'CLER' and requires to wait 4 seconds after the error is
        ''' cleared! </remarks>
        ''' <value> The clear error queue command. </value>
        Protected Overrides ReadOnly Property ClearErrorQueueCommand As String
            Get
                Return ""
            End Get
        End Property

        ''' <summary> Gets the initialize memory command. </summary>
        ''' <value> The initialize memory command. </value>
        Protected Overrides ReadOnly Property InitializeMemoryCommand As String
            Get
                Return ""
            End Get
        End Property

        ''' <summary> Gets the last error query command. </summary>
        ''' <value> The last error query command. </value>
        Protected Overrides ReadOnly Property LastErrorQueryCommand As String
            Get
                If Debugger.IsAttached Then
                    Return Visa.Scpi.Syntax.LastErrorQueryCommand
                Else
                    Return ""
                End If
            End Get
        End Property

        ''' <summary> Gets line frequency query command. </summary>
        ''' <value> The line frequency query command. </value>
        Protected Overrides ReadOnly Property LineFrequencyQueryCommand As String
            Get
                Return ""
            End Get
        End Property

        ''' <summary> Gets the preset command. </summary>
        ''' <value> The preset command. </value>
        Protected Overrides ReadOnly Property PresetCommand As String
            Get
                Return ""
            End Get
        End Property

#End Region

#Region " SCPI VERSION "

        ''' <summary> The Auto Tuning Enabled. </summary>
        Private WithEvents _AutoTuningEnabled As PresettableBoolean
        ''' <summary> Gets the cached version level of the SCPI standard implemented by the device. </summary>
        ''' <value> The Auto Tuning Enabled. </value>
        Public ReadOnly Property AutoTuningEnabled As PresettableBoolean
            Get
                Return Me._AutoTuningEnabled
            End Get
        End Property

        ''' <summary> Auto Tuning Enabled property changed. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Property Changed event information. </param>
        Private Sub _AutoTuningEnabled_PropertyChanged(sender As Object, e As PropertyChangedEventArgs) Handles _AutoTuningEnabled.PropertyChanged
            Me.SafePostPropertyChanged(e)
        End Sub

        ''' <summary> Writes and reads back the auto Tuning enabled state. </summary>
        ''' <param name="value"> If set to <c>True</c> enable. </param>
        ''' <returns> <c>True</c> if auto Tuning is enabled; <c>False</c> if not or none if not set or
        ''' unknown. </returns>
        Public Function ApplyAutoTuningEnabled(ByVal value As Boolean) As Boolean?
            Me.WriteAutoTuningEnabled(value)
            If Me.LastStatus >= VisaNS.VisaStatusCode.Success Then
                Me.QueryAutoTuningEnabled()
            End If
            Return Me.AutoTuningEnabled.Value
        End Function

        ''' <summary> Queries the auto Tuning enabled state. </summary>
        ''' <returns> System.Nullable{System.Boolean}. </returns>
        ''' <remarks> Sends the 'LRNM?' query. </remarks>
        Public Function QueryAutoTuningEnabled() As Boolean?
            If Me.IsSessionOpen AndAlso Me.AutoTuningEnabled.Command.QuerySupported Then
                Me.AutoTuningEnabled.Value = Me.Session.Query(True, Me.AutoTuningEnabled.Command.QueryCommand)
            End If
            Return Me.AutoTuningEnabled.Value
        End Function

        ''' <summary> Writes the auto Tuning enabled state without reading back the actual value. </summary>
        ''' <param name="value"> if set to <c>True</c> [value]. </param>
        ''' <returns> <c>True</c> if auto Tuning is enabled; <c>False</c> if not or none if not set or
        ''' unknown. </returns>
        ''' <remarks> Sends the formatted command: 'LRNM {0:;1;0}' </remarks>
        Public Function WriteAutoTuningEnabled(ByVal value As Boolean) As Boolean?
            If Me.IsSessionOpen AndAlso Me.AutoTuningEnabled.Command.WriteSupported Then
                Me.Session.WriteLine(Me.AutoTuningEnabled.Command.WriteCommand, CType(value, Integer))
            End If
            If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
                Me.AutoTuningEnabled.Value = New Boolean?
            Else
                Me.AutoTuningEnabled.Value = value
            End If
            Return Me.AutoTuningEnabled.Value
        End Function

#End Region

#Region " COOLING ENABLED "

        Private _CoolingEnabled As Boolean?

        ''' <summary> Gets or sets a cached value indicating whether Cooling is enabled. </summary>
        ''' <value> <c>True</c> if Cooling is enabled; <c>False</c> if not or none if not set or
        ''' unknown. </value>
        Public Property CoolingEnabled() As Boolean?
            Get
                Return Me._CoolingEnabled
            End Get
            Protected Set(ByVal value As Boolean?)
                If Not Boolean?.Equals(Me.CoolingEnabled, value) Then
                    Me._CoolingEnabled = value
                    Me.SafePostPropertyChanged()
                End If
            End Set
        End Property

        ''' <summary> Writes and reads back the Cooling enabled state. </summary>
        ''' <param name="value"> If set to <c>True</c> enable. </param>
        ''' <returns> <c>True</c> if Cooling is enabled; <c>False</c> if not or none if not set or
        ''' unknown. </returns>
        Public Function ApplyCoolingEnabled(ByVal value As Boolean) As Boolean?
            Me.WriteCoolingEnabled(value)
            If Me.LastStatus >= VisaNS.VisaStatusCode.Success Then
                Me.QueryCoolingEnabled()
            End If
            Return Me.CoolingEnabled
        End Function

        ''' <summary> Queries the Cooling enabled state. </summary>
        ''' <returns> <c>True</c> if Cooling is enabled; <c>False</c> if not or none if not set or
        ''' unknown. </returns>
        Public Function QueryCoolingEnabled() As Boolean?
            If Me.IsSessionOpen Then
                Me.CoolingEnabled = Me.Session.Query(True, "COOL?")
            End If
            Return Me.CoolingEnabled
        End Function

        ''' <summary> Writes the Cooling enabled state without reading back the actual value. </summary>
        ''' <param name="value"> if set to <c>True</c> [value]. </param>
        ''' <returns> <c>True</c> if Cooling is enabled; <c>False</c> if not or none if not set or
        ''' unknown. </returns>
        Public Function WriteCoolingEnabled(ByVal value As Boolean) As Boolean?
            If Me.IsSessionOpen Then
                Me.Session.WriteLine("COOL {0:'1';'1';'0'}", CType(value, Integer))
            End If
            If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
                Me.CoolingEnabled = New Boolean?
            Else
                Me.CoolingEnabled = value
            End If
            Return Me.CoolingEnabled
        End Function

#End Region

#Region " DEVICE CONTROL ENABLED "

        Private _DeviceControlEnabled As Boolean?

        ''' <summary> Gets or sets a value indicating whether Device Control is enabled. </summary>
        ''' <value> <c>True</c> if Device Control mode is enabled; otherwise, <c>False</c>. </value>
        Public Property DeviceControlEnabled() As Boolean?
            Get
                Return Me._DeviceControlEnabled
            End Get
            Protected Set(ByVal value As Boolean?)
                If Not Boolean?.Equals(Me.DeviceControlEnabled, value) Then
                    Me._DeviceControlEnabled = value
                    Me.SafePostPropertyChanged()
                End If
            End Set
        End Property

        ''' <summary> Writes and reads back the Device Control enabled state. </summary>
        ''' <param name="value"> If set to <c>True</c> enable. </param>
        ''' <returns> <c>True</c> if Device Control is enabled; <c>False</c> if not or none if not set or
        ''' unknown. </returns>
        Public Function ApplyDeviceControlEnabled(ByVal value As Boolean) As Boolean?
            Me.WriteDeviceControlEnabled(value)
            If Me.LastStatus >= VisaNS.VisaStatusCode.Success Then
                Me.QueryDeviceControlEnabled()
            End If
            Return Me.DeviceControlEnabled
        End Function

        ''' <summary> Queries the Device Control enabled state. </summary>
        ''' <returns> <c>True</c> if Device Control is enabled; <c>False</c> if not or none if not set or
        ''' unknown. </returns>
        Public Function QueryDeviceControlEnabled() As Boolean?
            If Me.IsSessionOpen Then
                Me.DeviceControlEnabled = Me.Session.Query(True, "DUTM?")
            End If
            Return Me.DeviceControlEnabled
        End Function

        ''' <summary> Writes the Device Control enabled state without reading back the actual value. </summary>
        ''' <param name="value"> if set to <c>True</c> [value]. </param>
        ''' <returns> <c>True</c> if Device Control is enabled; <c>False</c> if not or none if not set or
        ''' unknown. </returns>
        Public Function WriteDeviceControlEnabled(ByVal value As Boolean) As Boolean?
            Me.DeviceControlEnabled = New Boolean?
            If Me.IsSessionOpen Then
                Me.Session.WriteLine("DUTM {0:'1';'1';'0'}", CType(value, Integer))
            End If
            If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
                Me.DeviceControlEnabled = New Boolean?
            Else
                Me.DeviceControlEnabled = value
            End If
            Return Me.DeviceControlEnabled
        End Function

#End Region

    End Class

End Namespace

Imports NationalInstruments
Namespace ThermoStream

    ''' <summary> Defines a Status Subsystem for a InTest Thermo Stream instrument. </summary>
    ''' <license> (c) 2013 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="9/22/2013" by="David" revision="3.0.5013"> Created. </history>
    Public Class StatusSubsystem
        Inherits Visa.SCPI.StatusSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> Initializes a new instance of the <see cref="StatusSubsystem" /> class. </summary>
        ''' <param name="visaSession"> A reference to a <see cref="Visa.Session">message based
        ''' session</see>. </param>
        Public Sub New(ByVal visaSession As Visa.Session)
            MyBase.New(visaSession)
            Me.VersionInfo = New VersionInfo
        End Sub

#End Region

#Region " PUBLISHER "

        ''' <summary> Publishes all values by raising the property changed events. </summary>
        Public Overrides Sub Publish()
            If Me.Publishable Then
                For Each p As Reflection.PropertyInfo In Reflection.MethodInfo.GetCurrentMethod.DeclaringType.GetProperties()
                    Me.SafePostPropertyChanged(p.Name)
                Next
            End If
        End Sub

#End Region

#Region " COMMAND SYNTAX "

        ''' <summary> Gets the preset command. </summary>
        ''' <value> The preset command. </value>
        Protected Overrides ReadOnly Property PresetCommand As String
            Get
                Return "" ' Visa.Scpi.Syntax.StatusPresetCommand
            End Get
        End Property

#Region " STANDARD STATUS  "

        ''' <summary> Gets the clear execution state command. </summary>
        ''' <value> The clear execution state command. </value>
        Protected Overrides ReadOnly Property ClearExecutionStateCommand As String
            Get
                Return Ieee488.Syntax.ClearExecutionStateCommand
            End Get
        End Property

        ''' <summary> Gets the reset known state command. </summary>
        ''' <value> The reset known state command. </value>
        Protected Overrides ReadOnly Property ResetKnownStateCommand As String
            Get
                Return Ieee488.Syntax.ResetKnownStateCommand
            End Get
        End Property

        ''' <summary> Gets the clear error queue command. </summary>
        ''' <value> The clear error queue command. </value>
        Protected Overrides ReadOnly Property ClearErrorQueueCommand As String
            Get
                Return ""  ' Visa.Scpi.Syntax.ClearErrorQueueCommand
            End Get
        End Property

        ''' <summary> Gets the error queue query command. </summary>
        ''' <value> The error queue query command. </value>
        Protected Overrides ReadOnly Property ErrorQueueQueryCommand As String
            Get
                Return "" ' Visa.Scpi.Syntax.ErrorQueueQueryCommand
            End Get
        End Property

        ''' <summary> Gets the bits that would be set for detecting if an error is available. </summary>
        ''' <value> The error available bits. </value>
        Public Overrides ReadOnly Property ErrorAvailableBits As ServiceRequests
            Get
                Return ServiceRequests.ErrorAvailable
            End Get
        End Property

        ''' <summary> Gets the identity query command. </summary>
        ''' <value> The identity query command. </value>
        Protected Overrides ReadOnly Property IdentityQueryCommand As String
            Get
                Return Ieee488.Syntax.IdentityQueryCommand
            End Get
        End Property

        ''' <summary> Gets the bits that would be set for detecting if an Measurement is available. </summary>
        ''' <value> The Measurement available bits. </value>
        Public Overrides ReadOnly Property MeasurementAvailableBits As ServiceRequests
            Get
                Return ServiceRequests.MeasurementEvent
            End Get
        End Property

        ''' <summary> Gets the bits that would be set for detecting if an Message is available. </summary>
        ''' <value> The Message available bits. </value>
        Public Overrides ReadOnly Property MessageAvailableBits As ServiceRequests
            Get
                Return ServiceRequests.MessageAvailable
            End Get
        End Property

        ''' <summary> Gets the operation completed query command. </summary>
        ''' <value> The operation completed query command. </value>
        Protected Overrides ReadOnly Property OperationCompletedQueryCommand As String
            Get
                Return Ieee488.Syntax.OperationCompletedQueryCommand
            End Get
        End Property

        ''' <summary> Gets the bits that would be set for detecting if a Standard Event is available. </summary>
        ''' <value> The Standard Event available bits. </value>
        Public Overrides ReadOnly Property StandardEventAvailableBits As ServiceRequests
            Get
                Return ServiceRequests.StandardEvent
            End Get
        End Property

        ''' <summary> Gets the standard event status query command. </summary>
        ''' <value> The standard event status query command. </value>
        Protected Overrides ReadOnly Property StandardEventQueryCommand As String
            Get
                Return Ieee488.Syntax.StandardEventQueryCommand
            End Get
        End Property

        ''' <summary> Gets the standard event enable query command. </summary>
        ''' <value> The standard event enable query command. </value>
        Protected Overrides ReadOnly Property StandardEventEnableQueryCommand As String
            Get
                Return Ieee488.Syntax.StandardEventEnableQueryCommand
            End Get
        End Property

        ''' <summary> Gets the standard service enable command format. </summary>
        ''' <value> The standard service enable command format. </value>
        Protected Overrides ReadOnly Property StandardServiceEnableCommandFormat As String
            Get
                Return Ieee488.Syntax.StandardServiceEnableCommandFormat
            End Get
        End Property

        ''' <summary> Gets the standard service enable and complete command format. </summary>
        ''' <value> The standard service enable command and complete format. </value>
        Protected Overrides ReadOnly Property StandardServiceEnableCompleteCommandFormat As String
            Get
                Return Ieee488.Syntax.StandardServiceEnableCompleteCommandFormat
            End Get
        End Property

        ''' <summary> Gets the service request enable command format. </summary>
        ''' <value> The service request enable command format. </value>
        Protected Overrides ReadOnly Property ServiceRequestEnableCommandFormat As String
            Get
                Return Ieee488.Syntax.ServiceRequestEnableCommandFormat
            End Get
        End Property

#End Region

#Region " MEASUREMENT REGISTER EVENTS "

        ''' <summary> Gets the measurement status query command. </summary>
        ''' <value> The measurement status query command. </value>
        Protected Overrides ReadOnly Property MeasurementStatusQueryCommand As String
            Get
                Return "" ' Visa.Scpi.Syntax.MeasurementEventQueryCommand
            End Get
        End Property

        ''' <summary> Gets the measurement event condition query command. </summary>
        ''' <value> The measurement event condition query command. </value>
        Protected Overrides ReadOnly Property MeasurementEventConditionQueryCommand As String
            Get
                Return "" ' Visa.Scpi.Syntax.MeasurementEventConditionQueryCommand
            End Get
        End Property

#End Region

#Region " OPERATION REGISTER EVENTS "

        ''' <summary> Gets the operation event enable Query command </summary>
        ''' <value> The operation event enable Query command. </value>
        Protected Overrides ReadOnly Property OperationEventEnableQueryCommand As String
            Get
                Return "" 'Visa.Scpi.Syntax.OperationEventEnableQueryCommand
            End Get
        End Property

        ''' <summary> Gets the operation event enable command format. </summary>
        ''' <value> The operation event enable command format. </value>
        Protected Overrides ReadOnly Property OperationEventEnableCommandFormat As String
            Get
                Return "" 'Visa.Scpi.Syntax.OperationEventEnableCommandFormat
            End Get
        End Property

        ''' <summary> Gets the operation event status query command. </summary>
        ''' <value> The operation event status query command. </value>
        Protected Overrides ReadOnly Property OperationEventStatusQueryCommand As String
            Get
                Return "" 'Visa.Scpi.Syntax.OperationEventQueryCommand
            End Get
        End Property

#End Region

#Region " QUESTIONABLE REGISTER "

        ''' <summary> Gets the questionable status query command. </summary>
        ''' <value> The questionable status query command. </value>
        Protected Overrides ReadOnly Property QuestionableStatusQueryCommand As String
            Get
                Return "" 'Visa.Scpi.Syntax.QuestionableEventQueryCommand
            End Get
        End Property

#End Region

#End Region

#Region " IDENTITY "

        ''' <summary> Parse version information. </summary>
        ''' <param name="value"> The value. </param>
        Public Overrides Sub ParseVersionInfo(value As String)
            Me.VersionInfo.Parse(value)
        End Sub

        ''' <summary> Gets or sets the information describing the version. </summary>
        ''' <value> Information describing the version. </value>
        Public Property VersionInfo As VersionInfo

#End Region

    End Class

End Namespace

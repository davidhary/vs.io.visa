Imports System.ComponentModel
Imports System.Windows.Forms
Imports isr.Core.Controls.ControlExtensions
Imports isr.Core.Controls.ComboBoxExtensions
Imports isr.Core.Controls.CheckBoxExtensions
Imports isr.Core.Controls.NumericUpDownExtensions
Imports isr.Core.Controls.SafeSetterExtensions
Imports isr.Core.Controls.ToolStripExtensions
Imports isr.Core.Pith
Imports isr.Core.Pith.StopwatchExtensions
Imports isr.Core.Pith.ExceptionExtensions
Namespace ThermoStream

    ''' <summary> Provides a user interface for the Thermo Stream Device. </summary>
    ''' <license> (c) 2005 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="01/15/2015" by="David" revision="2.0.2936.x"> Create based on the 27xx
    ''' system classes. </history>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId:="ThermoStream")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling")>
    <System.ComponentModel.DisplayName("Thermo Stream Panel"),
      System.ComponentModel.Description("In Test Thermo Stream Device Panel"),
      System.Drawing.ToolboxBitmap(GetType(ThermoStream.ThermoStreamPanel))>
    Public Class ThermoStreamPanel
#Const designMode1 = True
#Region " BASE FROM WRAPPER "
        ' Designing requires changing the condition to True.
#If designMode Then
        Inherits Visa.Instrument.ResourcePanelBaseWrapper
#Else
        Inherits Visa.Instrument.ResourcePanelBase
#End If
#End Region

#Region " CONSTRUCTION and CLEANUP "

        Private initializingComponents As Boolean
        ''' <summary> Default constructor. </summary>
        Public Sub New()
            MyBase.New()

            initializingComponents = True
            ' This call is required by the Windows Form Designer.
            InitializeComponent()
            initializingComponents = False

            ' instantiate the reference to the Device
            Me.Device = New ThermoStream.Device()

            Me.onDisplayDeviceOpenChanged(Me.IsDeviceOpen)
            Me.initializeUserInterface()
            Me.onMeasurementAvailable("")

            Me._SetpointWindowNumeric.ReadOnly = True
            Me._SetpointNumeric.ReadOnly = True
            Me._SoakTimeNumeric.ReadOnly = True

#If designMode Then
            Debug.Assert(False, "Illegal call; reset design mode")
#End If

        End Sub

        ''' <summary> Disposes managed resources. </summary>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub OnDisposeManagedResources()
            If Me.Device IsNot Nothing Then
                Try
                    ' remove event handlers.
                    Me.DeviceClosing(Me, System.EventArgs.Empty)
                    Me._Device.Dispose()
                    Me._Device = Nothing
                Catch ex As Exception
                    Debug.Assert(Not Debugger.IsAttached,
                                 "Exception occurred disposing thermal stream panel resources", "Exception {0}", ex.ToFullBlownString)
                End Try
            End If
        End Sub

        ''' <summary> Enables or disables controls based on the device open state. </summary>
        ''' <param name="deviceIsOpen"> <c>true</c> if device is open. </param>
        Private Sub OnDisplayDeviceOpenChanged(ByVal deviceIsOpen As Boolean)
            Me._Tabs.Enabled = True
            For Each t As Windows.Forms.TabPage In Me._Tabs.TabPages
                If t IsNot Me._MessagesTabPage Then
                    For Each c As Windows.Forms.Control In t.Controls : c.RecursivelyEnable(deviceIsOpen) : Next
                End If
            Next
        End Sub

        ''' <summary> Initialize User interface. </summary>
        Private Sub InitializeUserInterface()
            Me.StatusRegisterToolStripStatusLabel.Visible = True
            Me.StandardRegisterToolStripStatusLabel.Visible = False
            Me.IdentityToolStripStatusLabel.Visible = False
            Me.DisplaySensorTypes()
        End Sub

#End Region

#Region " DEVICE "

        Private _Device As ThermoStream.Device

        ''' <summary> Gets or sets a reference to the InTest Thermo Stream Device. </summary>
        ''' <value> The device. </value>
        <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
        Public Overloads Property Device() As ThermoStream.Device
            Get
                Return Me._Device
            End Get
            Friend Set(ByVal value As ThermoStream.Device)
                If value IsNot Nothing Then
                    Me._Device = value
                    MyBase.Device = value
                End If
            End Set
        End Property

#End Region

#Region " DEVICE EVENT HANDLERS "

        ''' <summary> Handle the device property changed event. </summary>
        ''' <param name="device">    The device. </param>
        ''' <param name="propertyName"> Name of the property. </param>
        Private Sub OnDevicePropertyChanged(ByVal device As Device, ByVal propertyName As String)
            If device Is Nothing OrElse propertyName Is Nothing Then
                Return
            Else
                Select Case propertyName
                    Case "Enabled"
                        Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Hardware {0};. ",
                                                   IIf(device.Enabled, "Enabled", "Disabled"))
                    Case "ResourcesSearchPattern"
                        MyBase.Connector.ResourcesSearchPattern = device.ResourcesSearchPattern
                    Case "ServiceRequestFailureMessage"
                        If Not String.IsNullOrWhiteSpace(device.ServiceRequestFailureMessage) Then
                            Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, device.ServiceRequestFailureMessage)
                        End If
                    Case "SessionPropertyChangeHandlerEnabled"
                        ' Me._HandleServiceRequestsCheckBox.Checked = device.SessionPropertyChangeHandlerEnabled
                End Select
            End If
        End Sub

        ''' <summary> Device property changed. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Property Changed event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Protected Overrides Sub DevicePropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
            Try
                If sender IsNot Nothing AndAlso e IsNot Nothing Then
                    Me.OnDevicePropertyChanged(TryCast(sender, Device), e.PropertyName)
                    Application.DoEvents()
                End If
            Catch ex As Exception
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception handling property changed Event;. Failed property {0}. {1}",
                                           e.PropertyName, ex.ToFullBlownString)
            End Try
        End Sub

        ''' <summary> Device service requested. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Message based session event information. </param>
        Protected Overrides Sub DeviceServiceRequested(ByVal sender As Object, ByVal e As NationalInstruments.VisaNS.MessageBasedSessionEventArgs)
            MyBase.DeviceServiceRequested(sender, e)
        End Sub

        ''' <summary> Event handler. Called upon device opening. </summary>
        ''' <param name="sender"> <see cref="System.Object"/> instance of this
        ''' <see cref="System.Windows.Forms.Control"/> </param>
        ''' <param name="e">      Event information. </param>
        Protected Overrides Sub DeviceOpening(ByVal sender As Object, ByVal e As System.EventArgs)
            MyBase.DeviceOpening(sender, e)
        End Sub

        ''' <summary> Event handler. Called when device opened. </summary>
        ''' <param name="sender"> <see cref="System.Object"/> instance of this
        ''' <see cref="System.Windows.Forms.Control"/> </param>
        ''' <param name="e">      Event information. </param>
        Protected Overrides Sub DeviceOpened(ByVal sender As Object, ByVal e As System.EventArgs)
            AddHandler Me.Device.ThermoStreamSubsystem.PropertyChanged, AddressOf Me.ThermoStreamSubsystemPropertyChanged
            AddHandler Me.Device.StatusSubsystem.PropertyChanged, AddressOf Me.StatusSubsystemPropertyChanged
            AddHandler Me.Device.SystemSubsystem.PropertyChanged, AddressOf Me.SystemSubsystemPropertyChanged

            MyBase.DeviceOpened(sender, e)
            Me.onDisplayDeviceOpenChanged(Me.IsDeviceOpen)
            ' this defines the readings.
            Me.Device.InitializeKnownState()
            ' TO DO: Enable? should this be the first call of this function. 
            ' Me.EnableServiceRequestEventHandler()
        End Sub

        ''' <summary> Event handler. Called when device is closing. </summary>
        ''' <param name="sender"> <see cref="System.Object"/> instance of this
        ''' <see cref="System.Windows.Forms.Control"/> </param>
        ''' <param name="e">      Event information. </param>
        Protected Overrides Sub DeviceClosing(ByVal sender As Object, ByVal e As System.EventArgs)
            MyBase.DeviceClosing(sender, e)
            If Me.IsDeviceOpen Then
                RemoveHandler Me.Device.ThermoStreamSubsystem.PropertyChanged, AddressOf Me.ThermoStreamSubsystemPropertyChanged
                RemoveHandler Me.Device.StatusSubsystem.PropertyChanged, AddressOf Me.StatusSubsystemPropertyChanged
                RemoveHandler Me.Device.SystemSubsystem.PropertyChanged, AddressOf Me.SystemSubsystemPropertyChanged
            End If
        End Sub

        ''' <summary> Event handler. Called when device is closed. </summary>
        ''' <param name="sender"> <see cref="System.Object"/> instance of this
        ''' <see cref="System.Windows.Forms.Control"/> </param>
        ''' <param name="e">      Event information. </param>
        Protected Overrides Sub DeviceClosed(ByVal sender As Object, ByVal e As System.EventArgs)
            Me.onDisplayDeviceOpenChanged(Me.IsDeviceOpen)
            MyBase.DeviceClosed(sender, e)
        End Sub

#End Region

#Region " SUBSYSTEMS "

#Region " THERMO STREAM "

        ''' <summary> Degrees caption. </summary>
        ''' <param name="value"> The <see cref="TraceMessage">message</see> to display and
        ''' log. </param>
        ''' <returns> A String. </returns>
        Private Shared Function DegreesCaption(ByVal value As String) As String
            Return String.Format("{0} {1}C", value, Convert.ToChar(&H2070))
        End Function

        ''' <summary> Executes the measurement available action. </summary>
        Private Sub OnMeasurementAvailable(ByVal value As Double?)
            If value.HasValue Then
                Me.onMeasurementAvailable(CStr(value.Value))
            Else
                Me.onMeasurementAvailable("")
            End If
        End Sub

        ''' <summary> Executes the measurement available action. </summary>
        Private Sub OnMeasurementAvailable(ByVal value As String)
            Const clear As String = "    "
            If String.IsNullOrWhiteSpace(value) Then
                Me._ReadingToolStripStatusLabel.Text = ThermoStreamPanel.DegreesCaption("-.-")
                Me._ComplianceToolStripStatusLabel.Text = clear
                Me._TbdToolStripStatusLabel.Text = clear
            Else
                Me._ReadingToolStripStatusLabel.SafeTextSetter(ThermoStreamPanel.DegreesCaption(value))
                Me._ComplianceToolStripStatusLabel.Text = "  "
                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                           "Instruments parsed reading elements.")
            End If
        End Sub

        ''' <summary> Handles the visa status available action. </summary>
        ''' <param name="subsystem"> The subsystem. </param>
        Private Sub OnVisaStatusAvailable(ByVal subsystem As ThermoStreamSubsystem)
            If subsystem.LastStatus > NationalInstruments.VisaNS.VisaStatusCode.Success Then
                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                           "Visa operation warning;. Visa Status: {0}",
                                           VisaException.BuildVisaStatusDetails(subsystem.LastStatus))
            ElseIf subsystem.LastStatus < NationalInstruments.VisaNS.VisaStatusCode.Success Then
                Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId,
                                           "Visa operation failed;. Visa Status: {0}",
                                           VisaException.BuildVisaStatusDetails(subsystem.LastStatus))
            End If
        End Sub

        ''' <summary> Handle the Sense subsystem property changed event. </summary>
        ''' <param name="subsystem">    The subsystem. </param>
        ''' <param name="propertyName"> Name of the property. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")>
        Private Sub OnSubsystemPropertyChanged(ByVal subsystem As ThermoStreamSubsystem, ByVal propertyName As String)
            If subsystem Is Nothing OrElse propertyName Is Nothing Then
                Return
            Else
                Select Case propertyName
                    Case "IsHeadUp"
                        Me._HeadDownCheckBox.SafeCheckedSetter(Not subsystem.IsHeadUp)
                    Case "IsReady"
                        Me._ReadyCheckBox.SafeCheckedSetter(subsystem.IsReady)
                    Case "CycleCount"
                        Me._CycleCountNumeric.SafeValueSetter(CType(subsystem.CycleCount, Decimal?))
                    Case "DeviceSensorType"
                        Me._DeviceSensorTypeSelector.ComboBox.SafeSelectValue(subsystem.DeviceSensorType.GetValueOrDefault(0))
                    Case "DeviceThermalConstant"
                        Me._DeviceThermalConstantSelector.SafeValueSetter(CType(subsystem.DeviceThermalConstant, Decimal?))
                    Case "MaximumTestTime"
                        Me._MaxTestTimeNumeric.SafeValueSetter(CType(subsystem.MaximumTestTime, Decimal?))
                    Case "RampRate"
                        Me._RampRateNumeric.SafeValueSetter(CType(subsystem.RampRate, Decimal?))
                    Case "SetpointNumber"
                        Me._SetpointNumberNumeric.SafeValueSetter(CType(subsystem.SetpointNumber, Decimal?))
                    Case "Setpoint"
                        Me._SetpointNumeric.SafeValueSetter(CType(subsystem.Setpoint, Decimal?))
                    Case "SetpointWindow"
                        Me._SetpointWindowNumeric.SafeValueSetter(CType(subsystem.SetpointWindow, Decimal?))
                    Case "SoakTime"
                        Me._SoakTimeNumeric.SafeValueSetter(CType(subsystem.SoakTime, Decimal?))
                    Case "IsAtTemperature"
                        Me._AtTempCheckBox.SafeCheckedSetter(subsystem.IsAtTemperature)
                    Case "IsCycleCompleted"
                        Me._CycleCompletedCheckBox1.CheckBoxControl.SafeCheckedSetter(subsystem.IsCycleCompleted)
                    Case "IsCyclesCompleted"
                        Me._CyclesCompletedCheckBox1.CheckBoxControl.SafeCheckedSetter(subsystem.IsCyclesCompleted)
                    Case "IsCyclingStopped"
                        Me._CyclingStoppedCheckBox.CheckBoxControl.SafeCheckedSetter(subsystem.IsCyclingStopped)
                    Case "IsTestTimeElapsed"
                        Me._TestTimeElapsedCheckBox.SafeCheckedSetter(subsystem.IsTestTimeElapsed)
                    Case "IsNotAtTemperature"
                        Me._NotAtTempCheckBox.SafeCheckedSetter(subsystem.IsNotAtTemperature)
                    Case "LastStatus"
                        Me.onVisaStatusAvailable(subsystem)
                    Case "Temperature"
                        Me.onMeasurementAvailable(subsystem.Temperature)
                    Case "MeasurementAvailable"
                        Me.onMeasurementAvailable(subsystem.Temperature)
                End Select
            End If
        End Sub

        ''' <summary> Sense Current subsystem property changed. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Property Changed event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub ThermoStreamSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
            Try
                If sender IsNot Nothing AndAlso e IsNot Nothing Then
                    Me.OnSubsystemPropertyChanged(TryCast(sender, ThermoStreamSubsystem), e.PropertyName)
                    Application.DoEvents()
                End If
            Catch ex As Exception
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception handling Sense Current Subsystem property changed Event;. Failed property {0}. {1}",
                                           e.PropertyName, ex.ToFullBlownString)
            End Try
        End Sub

#End Region

#Region " STATUS "

        ''' <summary> Displays the status register status using hex format. </summary>
        ''' <param name="value"> The register value. </param>
        Public Overrides Sub DisplayStatusRegisterStatus(ByVal value As Integer)
            Me._StatusByteLabel.Text = String.Format("{0,8}", Convert.ToString(value, 2)).Replace(" ", "0")
            MyBase.DisplayStatusRegisterStatus(value)
            Application.DoEvents()
        End Sub

        ''' <summary> Handle the Status subsystem property changed event. </summary>
        ''' <param name="subsystem">    The subsystem. </param>
        ''' <param name="propertyName"> Name of the property. </param>
        Private Sub OnSubsystemPropertyChanged(ByVal subsystem As StatusSubsystem, ByVal propertyName As String)
            If subsystem Is Nothing OrElse propertyName Is Nothing Then
                Return
            Else
                Select Case propertyName
                    Case "Identity"
                        If Not String.IsNullOrWhiteSpace(subsystem.Identity) Then
                            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                                       "{0} identified as {1}.", Me.ResourceName, subsystem.Identity)

                        End If
                    Case "DeviceErrors"
                        If Not String.IsNullOrWhiteSpace(subsystem.DeviceErrors) Then
                            Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId,
                                                       "{0} Device errors: {1}", Me.ResourceName, subsystem.DeviceErrors)
                        End If
                    Case "ErrorAvailable"
                        If Me.IsDeviceOpen AndAlso Me.Device.StatusSubsystem.ErrorAvailable Then
                            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Error available;. ")
                            Me.Device.SystemSubsystem.QueryLastError()
                        End If
                    Case "ServiceRequestStatus"
                        Me.DisplayStatusRegisterStatus(Me.Device.StatusSubsystem.ServiceRequestStatus)
                End Select
            End If
        End Sub

        ''' <summary> Status subsystem property changed. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Property Changed event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub StatusSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
            Try
                If sender IsNot Nothing AndAlso e IsNot Nothing Then
                    Me.OnSubsystemPropertyChanged(TryCast(sender, StatusSubsystem), e.PropertyName)
                    Application.DoEvents()
                End If
            Catch ex As Exception
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception handling Status Subsystem property changed Event;. Failed property {0}. {1}",
                                           e.PropertyName, ex.ToFullBlownString)
            End Try
        End Sub
#End Region

#Region " SYSTEM "

        ''' <summary> Reports the last error. </summary>
        ''' <param name="subsystem"> The subsystem. </param>
        Private Sub OnLastError(ByVal subsystem As SystemSubsystem)
            If subsystem Is Nothing Then
                Throw New ArgumentNullException("subsystem")
            End If
            If Me.IsDeviceOpen AndAlso subsystem.LastError IsNot Nothing Then
                If subsystem.LastError.ErrorNumber = 0 Then
                    Me._LastErrorTextBox.ForeColor = Drawing.Color.Aquamarine
                Else
                    Me._LastErrorTextBox.ForeColor = Drawing.Color.OrangeRed
                    Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Error;. Last error: {0}",
                                               subsystem.LastError.CompoundErrorMessage)
                End If
                Me._LastErrorTextBox.Text = subsystem.LastError.CompoundErrorMessage
            Else
                Me._LastErrorTextBox.ForeColor = Drawing.Color.Aquamarine
                Me._LastErrorTextBox.Text = ""
            End If

        End Sub

        ''' <summary> Handle the System subsystem property changed event. </summary>
        ''' <param name="subsystem">    The subsystem. </param>
        ''' <param name="propertyName"> Name of the property. </param>
        Private Sub OnSubsystemPropertyChanged(ByVal subsystem As SystemSubsystem, ByVal propertyName As String)
            If subsystem Is Nothing OrElse propertyName Is Nothing Then
                Return
            Else
                Select Case propertyName
                    Case "LastError"
                        Me.onLastError(subsystem)
                End Select
            End If
        End Sub

        ''' <summary> System subsystem property changed. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Property Changed event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub SystemSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
            Try
                If sender IsNot Nothing AndAlso e IsNot Nothing Then
                    Me.OnSubsystemPropertyChanged(TryCast(sender, SystemSubsystem), e.PropertyName)
                    Application.DoEvents()
                End If
            Catch ex As Exception
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception handling System Subsystem property changed Event;. Failed property {0}. {1}",
                                           e.PropertyName, ex.ToFullBlownString)
            End Try
        End Sub
#End Region

#End Region

#Region " TRACE MESSAGES HANDLERS "

        ''' <summary> Displays the <paramref name="value">message</paramref>. </summary>
        ''' <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
        Protected Overrides Sub DisplayMessage(value As TraceMessage)
            If value IsNot Nothing AndAlso MyBase.ShouldShow(value.EventType) Then
                Me._MessagesBox.AddMessage(value)
            End If
        End Sub

        ''' <summary> Gets the trace display level. </summary>
        ''' <value> The trace display level. </value>
        Public Property TraceDisplayLevel As TraceEventType = TraceEventType.Verbose

        Protected Overrides ReadOnly Property TraceShowLevel As System.Diagnostics.TraceEventType
            Get
                Return Me.TraceDisplayLevel
            End Get
        End Property

#End Region

#Region " DISPLAY: TITLE "

        ''' <summary> Gets or sets the title. </summary>
        ''' <value> The title. </value>
        <Category("Appearance"), Description("The title of this panel"),
            Browsable(True),
            DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
            DefaultValue("Thermo Stream")>
        Public Property Title As String
            Get
                Return Me._TitleLabel.Text
            End Get
            Set(value As String)
                Me._TitleLabel.Text = value
                Me._TitleLabel.Visible = Not String.IsNullOrWhiteSpace(value)
            End Set
        End Property

#End Region

#Region " CONTROL EVENT HANDLERS: RESET "

        ''' <summary> Event handler. Called by _SessionTraceEnableCheckBox for checked changed events. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _SessionTraceEnableCheckBox_CheckedChanged(ByVal sender As Object, e As System.EventArgs) Handles _SessionTraceEnableCheckBox.CheckedChanged
            If initializingComponents Then Return
            Try
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                Me.ErrorProvider.Clear()
                If Not Me.DesignMode AndAlso sender IsNot Nothing Then
                    Dim checkBox As Windows.Forms.CheckBox = CType(sender, Windows.Forms.CheckBox)
                    If checkBox.Enabled Then
                        Me.Device.SessionPropertyChangeHandlerEnabled = checkBox.Checked
                        If Me.Device.SessionPropertyChangeHandlerEnabled = checkBox.Checked Then
                            Me.Device.SessionMessagesTraceEnabled = checkBox.Checked
                        Else
                            Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId,
                                                       "Failed to toggle the session property handler")
                        End If
                    End If
                End If
            Catch ex As Exception
                Me.Annunciate(sender, ex.Message)
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception occurred initiating a measurement;. {0}", ex.ToFullBlownString)
            Finally
                Me.Cursor = System.Windows.Forms.Cursors.Default
            End Try
        End Sub

        ''' <summary> Event handler. Called by interfaceClearButton for click events. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _InterfaceClearButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _InterfaceClearButton.Click
            Try
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                Me.ErrorProvider.Clear()
                If Me.IsDeviceOpen AndAlso Me.Device.IsSessionOpen Then
                    Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                               "Clearing interface '{0}';. ", Me.Device.Session.InterfaceResourceName)
                    Me.Device.SystemSubsystem.ClearInterface()
                End If
            Catch ex As Exception
                Me.Annunciate(sender, ex.Message)
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception occurred clearing interface;. {0}", ex.ToFullBlownString)
            Finally
                Me.Cursor = System.Windows.Forms.Cursors.Default
            End Try
        End Sub

        ''' <summary> Event handler. Called by _SelectiveDeviceClearButton for click events. </summary>
        ''' <param name="sender"> <see cref="System.Object"/> instance of this
        ''' <see cref="System.Windows.Forms.Control"/> </param>
        ''' <param name="e">      Event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _SelectiveDeviceClearButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _SelectiveDeviceClearButton.Click
            Try
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                Me.ErrorProvider.Clear()
                If Me.IsDeviceOpen AndAlso Me.Device.IsSessionOpen Then
                    Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                               "'{0}' Clearing Device '{1}';. ",
                                                Me.Device.Session.InterfaceResourceName, Me.ResourceName)
                    Me.Device.SystemSubsystem.ClearDevice()
                End If
            Catch ex As Exception
                Me.Annunciate(sender, ex.Message)
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception occurred sending SDC;. {0}", ex.ToFullBlownString)
            Finally
                Me.Cursor = System.Windows.Forms.Cursors.Default
            End Try
        End Sub

        ''' <summary> Issue RST. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _ResetButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ResetButton.Click
            Try
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                Me.ErrorProvider.Clear()
                If Me.IsDeviceOpen Then
                    Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                               "Resetting known state for '{0}';. ", Me.ResourceName)
                    Me.Device.ResetKnownState()
                End If
            Catch ex As Exception
                Me.Annunciate(sender, ex.Message)
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception occurred resetting known state;. {0}", ex.ToFullBlownString)
            Finally
                Me.Cursor = System.Windows.Forms.Cursors.Default
            End Try
        End Sub

        ''' <summary> Event handler. Called by _InitializeKnownStateButton for click events. </summary>
        ''' <param name="sender"> <see cref="System.Object"/> instance of this
        ''' <see cref="System.Windows.Forms.Control"/> </param>
        ''' <param name="e">      Event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _InitializeKnownStateButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _InitializeKnownStateButton.Click
            Try
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                Me.ErrorProvider.Clear()
                If Me.IsDeviceOpen Then
                    Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                               "Initializing known state for '{0}';. ", Me.ResourceName)
                    Me.Device.InitializeKnownState()
                End If
            Catch ex As Exception
                Me.Annunciate(sender, ex.Message)
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception occurred initializing known state;. {0}", ex.ToFullBlownString)
            Finally
                Me.Cursor = System.Windows.Forms.Cursors.Default
            End Try
        End Sub

#End Region

#Region " CONTROL EVENT HANDLERS: MAIN PANEL "

        ''' <summary> Reads last error button click. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _ReadLastErrorButton_Click(sender As System.Object, e As System.EventArgs) Handles _ReadLastErrorButton.Click
            Try
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                Me.ErrorProvider.Clear()
                If Me.IsDeviceOpen Then
                    Me.Device.SystemSubsystem.WriteLastErrorQuery()
                    Dim srq As isr.IO.Visa.ServiceRequests = Me.readServiceRequest
                    If (srq And ServiceRequests.MessageAvailable) = 0 Then
                        Me.Annunciate(sender, "Nothing to read")
                    Else
                        Me.Device.SystemSubsystem.ReadLastError()
                    End If
                End If
            Catch ex As Exception
                Me.Annunciate(sender, ex.Message)
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception occurred initializing known state;. {0}", ex.ToFullBlownString)
            Finally
                Me.Cursor = System.Windows.Forms.Cursors.Default
            End Try
        End Sub

        ''' <summary> Clears the error queue button click. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _ClearErrorQueueButton_Click(sender As System.Object, e As System.EventArgs) Handles _ClearErrorQueueButton.Click
            Try
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                Me.ErrorProvider.Clear()
                If Me.IsDeviceOpen Then
                    Me.Device.SystemSubsystem.ClearErrorQueueAndWait()
                    Me.readServiceRequest()
                End If
            Catch ex As Exception
                Me.Annunciate(sender, ex.Message)
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception occurred initializing known state;. {0}", ex.ToFullBlownString)
            Finally
                Me.Cursor = System.Windows.Forms.Cursors.Default
            End Try
        End Sub

        ''' <summary> Event handler. Called by InitButton for click events. Initiates a reading for
        ''' retrieval by way of the service request event. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _InitializeButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _InitializeButton.Click
            Try
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                Me.ErrorProvider.Clear()

                If Me.Device.IsDeviceOpen Then

                    ' clear execution state before enabling events
                    Me.Device.ClearExecutionState()

                    ' set the service request
                    Me.Device.ThermoStreamSubsystem.WriteTemperatureEventEnableBitmask(255)
                    Me.Device.StatusSubsystem.EnableServiceRequest(Visa.ServiceRequests.All And Not Visa.ServiceRequests.MessageAvailable)
                    Me.Device.ClearExecutionState()
                    Me.readServiceRequest()
                End If

            Catch ex As Exception
                Me.Annunciate(sender, ex.Message)
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception occurred initiating a measurement;. {0}", ex.ToFullBlownString)
            Finally
                Me.Cursor = System.Windows.Forms.Cursors.Default
            End Try

        End Sub

        ''' <summary> Event handler. Called by _ReadButton for click events. Query the Device for a
        ''' reading. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _ReadButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ReadButton.Click
            Try
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                Me.ErrorProvider.Clear()
                If sender IsNot Nothing AndAlso Me.IsDeviceOpen Then
                    ' update display if changed.
                    Me.Device.ThermoStreamSubsystem.QueryTemperature()
                    Me.readServiceRequest()
                    Application.DoEvents()
                End If
            Catch ex As Exception
                Me.Annunciate(sender, ex.Message)
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception occurred reading temperature;. {0}", ex.ToFullBlownString)
            Finally
                Me.Cursor = System.Windows.Forms.Cursors.Default
            End Try

        End Sub

        ''' <summary> Event handler. Called by _HandleServiceRequestsCheckBox for check state changed
        ''' events. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _HandleServiceRequestsCheckBox_CheckStateChanged(sender As Object, e As System.EventArgs)
            If initializingComponents Then Return
            If sender IsNot Nothing AndAlso Me.IsDeviceOpen Then
                Dim checkBox As CheckBox = TryCast(sender, CheckBox)
                If checkBox IsNot Nothing AndAlso Not checkBox.Checked = Me.Device.ServiceRequestEventHandlerEnabled Then
                    If checkBox IsNot Nothing AndAlso checkBox.Checked Then
                        Me.EnableServiceRequestEventHandler()
                        Me.Device.StatusSubsystem.EnableServiceRequest(ServiceRequests.All)
                    Else
                        Me.Device.StatusSubsystem.EnableServiceRequest(ServiceRequests.None)
                        Me.DisableServiceRequestEventHandler()
                    End If
                    Me.Device.StatusSubsystem.ReadRegisters()
                    Application.DoEvents()
                End If
            End If
        End Sub

        ''' <summary> Reads service request. </summary>
        ''' <returns> The service request. </returns>
        Private Function ReadServiceRequest() As isr.IO.Visa.ServiceRequests
            Dim srq As isr.IO.Visa.ServiceRequests = ServiceRequests.None
            If Me.IsDeviceOpen Then
                ' it takes a few ms for the event to register. 
                Diagnostics.Stopwatch.StartNew.Wait(TimeSpan.FromMilliseconds(Me._SrqRefratoryTimeNumeric.Value))
                srq = Me.Device.StatusSubsystem.ReadServiceRequestStatus
                ' Me.DisplayStatusRegisterStatus(srq)
                Application.DoEvents()
            End If
            Return srq
        End Function

        ''' <summary> Reads status byte button click. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Event information. </param>
        Private Sub _ReadStatusByteButton_Click(sender As System.Object, e As System.EventArgs) Handles _ReadStatusByteButton.Click
            If Me.IsDeviceOpen Then
                Me.readServiceRequest()
            End If
        End Sub

        ''' <summary> Sends a button click. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _SendButton_Click(sender As System.Object, e As System.EventArgs) Handles _SendButton.Click
            Try
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                Me.ErrorProvider.Clear()
                If sender IsNot Nothing AndAlso Me.IsDeviceOpen Then
                    If Not String.IsNullOrWhiteSpace(Me._SendComboBox.Text) Then
                        Me.Device.Session.WriteLine(Me._SendComboBox.Text)
                        Me.readServiceRequest()
                    End If
                End If
            Catch ex As Exception
                Me.Annunciate(sender, ex.Message)
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception occurred moving to the next setpoint;. {0}", ex.ToFullBlownString)
            Finally
                Me.Cursor = System.Windows.Forms.Cursors.Default
            End Try
        End Sub

        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _ReceiveButton_Click(sender As Object, e As System.EventArgs) Handles _ReceiveButton.Click
            Try
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                Me.ErrorProvider.Clear()
                If sender IsNot Nothing AndAlso Me.IsDeviceOpen Then
                    Dim srq As isr.IO.Visa.ServiceRequests = Me.Device.StatusSubsystem.ReadServiceRequestStatus()
                    If (srq And ServiceRequests.MessageAvailable) = 0 Then
                        Me.Annunciate(sender, "Nothing to read")
                    Else
                        Me._ReceiveTextBox.Text = Me.Device.Session.ReadLineTrimEnd
                        Me.readServiceRequest()
                    End If
                End If
            Catch ex As Exception
                Me.Annunciate(sender, ex.Message)
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception occurred moving to the next setpoint;. {0}", ex.ToFullBlownString)
            Finally
                Me.Cursor = System.Windows.Forms.Cursors.Default
            End Try
        End Sub

#End Region

#Region " CONTROL EVENT HANDLERS: CYCLE PANEL "

        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _FindLastSetpointButton_Click(sender As System.Object, e As System.EventArgs) Handles _FindLastSetpointButton1.Click
            Try
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                Me.ErrorProvider.Clear()
                If sender IsNot Nothing AndAlso Me.IsDeviceOpen Then
                    Me._FindLastSetpointButton1.Text = String.Format("Find Last Setpoint: {0}", Me.Device.ThermoStreamSubsystem.FindLastSetpointNumber)
                    Me.readServiceRequest()
                End If
            Catch ex As Exception
                Me.Annunciate(sender, ex.Message)
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception occurred finding last set point;. {0}", ex.ToFullBlownString)
            Finally
                Me.Cursor = System.Windows.Forms.Cursors.Default
            End Try
        End Sub

        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _QueryTempEvents(sender As System.Object)
            Try
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                Me.ErrorProvider.Clear()
                If sender IsNot Nothing AndAlso Me.IsDeviceOpen Then
                    Me.Device.ThermoStreamSubsystem.QueryTemperatureEventStatus()
                    Me.readServiceRequest()
                End If
            Catch ex As Exception
                Me.Annunciate(sender, ex.Message)
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception occurred reading temperature events;. {0}", ex.ToFullBlownString)
            Finally
                Me.Cursor = System.Windows.Forms.Cursors.Default
            End Try
        End Sub

        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _QueryAuxiliaryStatus(sender As System.Object)
            Try
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                Me.ErrorProvider.Clear()
                If sender IsNot Nothing AndAlso Me.IsDeviceOpen Then
                    Me.Device.ThermoStreamSubsystem.QueryAuxiliaryEventStatus()
                    Me.readServiceRequest()
                End If
            Catch ex As Exception
                Me.Annunciate(sender, ex.Message)
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception occurred reading auxiliary event status;. {0}", ex.ToFullBlownString)
            Finally
                Me.Cursor = System.Windows.Forms.Cursors.Default
            End Try
        End Sub

        Private Sub _QueryTempEventButton_Click(sender As System.Object, e As System.EventArgs)
            Me._QueryTempEvents(sender)
        End Sub

        Private Sub _QueryAuxiliaryStatusButton_Click(sender As System.Object, e As System.EventArgs) Handles _QueryStatusButton.Click
            Me._QueryAuxiliaryStatus(sender)
            Me._QueryTempEvents(sender)
        End Sub


        ''' <summary> Setpoint number numeric value selected. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _SetpointNumberNumeric_ValueSelected(sender As System.Object, e As System.EventArgs) Handles _SetpointNumberNumeric.ValueSelected
            Try
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                Me.ErrorProvider.Clear()
                If sender IsNot Nothing AndAlso Me.IsDeviceOpen Then
                    With Me.Device.ThermoStreamSubsystem
                        If Me._SetpointNumberNumeric.SelectedValue.HasValue Then
                            .WriteSetpointNumber(CInt(Me._SetpointNumberNumeric.Value))
                            Application.DoEvents()
                        End If
                        .QuerySetpointNumber()
                        Application.DoEvents()
                        .ReadCurrentSetpointValues()
                        Application.DoEvents()
                    End With
                    Me.readServiceRequest()
                End If
            Catch ex As Exception
                Me.Annunciate(sender, ex.Message)
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception occurred selecting setpoint;. {0}", ex.ToFullBlownString)
            Finally
                Me.Cursor = System.Windows.Forms.Cursors.Default
            End Try

        End Sub

        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub QueryHeadStatus(sender As System.Object)
            Try
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                Me.ErrorProvider.Clear()
                If sender IsNot Nothing AndAlso Me.IsDeviceOpen Then
                    With Me.Device.ThermoStreamSubsystem
                        .QueryHeadDown()
                    End With
                    Me.readServiceRequest()
                End If
            Catch ex As Exception
                Me.Annunciate(sender, ex.Message)
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception occurred reading head status;. {0}", ex.ToFullBlownString)
            Finally
                Me.Cursor = System.Windows.Forms.Cursors.Default
            End Try
        End Sub

        ''' <summary> Reads set point button click. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _ReadSetpointButton_Click(sender As System.Object, e As System.EventArgs) Handles _ReadSetpointButton.Click
            Try
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                Me.ErrorProvider.Clear()
                If sender IsNot Nothing AndAlso Me.IsDeviceOpen Then
                    With Me.Device.ThermoStreamSubsystem
                        .ReadCurrentSetpointValues()
                    End With
                    Me.readServiceRequest()
                End If
            Catch ex As Exception
                Me.Annunciate(sender, ex.Message)
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception occurred reading the setpoint values;. {0}", ex.ToFullBlownString)
            Finally
                Me.Cursor = System.Windows.Forms.Cursors.Default
            End Try
        End Sub

        ''' <summary> Next button click. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _NextButton_Click(sender As System.Object, e As System.EventArgs) Handles _NextSetpointButton.Click
            Try
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                Me.ErrorProvider.Clear()
                If sender IsNot Nothing AndAlso Me.IsDeviceOpen Then
                    With Me.Device.ThermoStreamSubsystem
                        .NextSetpoint()
                    End With
                    Me.readServiceRequest()
                End If
            Catch ex As Exception
                Me.Annunciate(sender, ex.Message)
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception occurred moving to the next setpoint;. {0}", ex.ToFullBlownString)
            Finally
                Me.Cursor = System.Windows.Forms.Cursors.Default
            End Try
        End Sub

        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _MaxTestTimeNumeric_ValueSelected(sender As System.Object, e As System.EventArgs) Handles _MaxTestTimeNumeric.ValueSelected
            Try
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                Me.ErrorProvider.Clear()
                If sender IsNot Nothing AndAlso Me.IsDeviceOpen Then
                    With Me.Device.ThermoStreamSubsystem
                        If Me._MaxTestTimeNumeric.SelectedValue.HasValue Then
                            .WriteMaximumTestTime(Me._MaxTestTimeNumeric.SelectedValue.Value)
                        End If
                        .QueryMaximumTestTime()
                    End With
                    Me.readServiceRequest()
                End If
            Catch ex As Exception
                Me.Annunciate(sender, ex.Message)
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception occurred setting and reading the maximum test time;. {0}", ex.ToFullBlownString)
            Finally
                Me.Cursor = System.Windows.Forms.Cursors.Default
            End Try
        End Sub

        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _CycleCountNumeric_ValueSelected(sender As System.Object, e As System.EventArgs) Handles _CycleCountNumeric.ValueSelected
            Try
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                Me.ErrorProvider.Clear()
                If sender IsNot Nothing AndAlso Me.IsDeviceOpen AndAlso Me._CycleCountNumeric.SelectedValue.HasValue Then
                    With Me.Device.ThermoStreamSubsystem
                        .ApplyCycleCount(CInt(Me._CycleCountNumeric.SelectedValue.Value))
                        .QueryCycleCount()
                    End With
                    Me.readServiceRequest()
                End If
            Catch ex As Exception
                Me.Annunciate(sender, ex.Message)
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception occurred setting and reading the cycle count;. {0}", ex.ToFullBlownString)
            Finally
                Me.Cursor = System.Windows.Forms.Cursors.Default
            End Try
        End Sub

        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _StartButton_Click(sender As System.Object, e As System.EventArgs) Handles _StartCycleButton.Click
            Try
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                Me.ErrorProvider.Clear()
                If sender IsNot Nothing AndAlso Me.IsDeviceOpen Then
                    With Me.Device.ThermoStreamSubsystem
                        .StartCycling()
                    End With
                    Me.readServiceRequest()
                End If
            Catch ex As Exception
                Me.Annunciate(sender, ex.Message)
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception occurred starting cycling;. {0}", ex.ToFullBlownString)
            Finally
                Me.Cursor = System.Windows.Forms.Cursors.Default
            End Try
        End Sub

        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _StopCycleButton_Click(sender As System.Object, e As System.EventArgs) Handles _StopCycleButton.Click
            Try
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                Me.ErrorProvider.Clear()
                If sender IsNot Nothing AndAlso Me.IsDeviceOpen Then
                    With Me.Device.ThermoStreamSubsystem
                        .StopCycling()
                    End With
                    Me.readServiceRequest()
                End If
            Catch ex As Exception
                Me.Annunciate(sender, ex.Message)
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception occurred Stopping cycling;. {0}", ex.ToFullBlownString)
            Finally
                Me.Cursor = System.Windows.Forms.Cursors.Default
            End Try
        End Sub

        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub ResetSystemMode(ByVal resetOperator As Boolean, sender As System.Object)
            Try
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                Me.ErrorProvider.Clear()
                If sender IsNot Nothing AndAlso Me.IsDeviceOpen Then
                    With Me.Device.ThermoStreamSubsystem
                        If resetOperator Then
                            .ResetOperatorScreen()
                        Else
                            .ResetCycleScreen()
                        End If
                        Me.readServiceRequest()
                        Diagnostics.Stopwatch.StartNew.Wait(TimeSpan.FromMilliseconds(100))
                        .QuerySystemScreen()
                        If .OperatorScreen.HasValue Then
                            Me._ResetOperatorModeButton.Text = String.Format("Operator Mode: {0}", IIf(.OperatorScreen.Value, "ON", "OFF"))
                        Else
                            Me._ResetOperatorModeButton.Text = String.Format("Operator Mode: {0}", "N/A")
                        End If
                        If .CycleScreen.HasValue Then
                            Me._ResetCycleModeButton.Text = String.Format("Cycle Mode: {0}", IIf(.CycleScreen.Value, "ON", "OFF"))
                        Else
                            Me._ResetCycleModeButton.Text = String.Format("Cycle Mode: {0}", "N/A")

                        End If
                    End With
                    Me.readServiceRequest()
                End If
            Catch ex As Exception
                Me.Annunciate(sender, ex.Message)
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception occurred Stopping cycling;. {0}", ex.ToFullBlownString)
            Finally
                Me.Cursor = System.Windows.Forms.Cursors.Default
            End Try
        End Sub

        Private Sub _ResetOperatorModeButton_Click(sender As System.Object, e As System.EventArgs) Handles _ResetOperatorModeButton.Click
            Me.ResetSystemMode(True, sender)
        End Sub

        Private Sub _ResetCycleModeButton_Click(sender As System.Object, e As System.EventArgs) Handles _ResetCycleModeButton.Click
            Me.ResetSystemMode(False, sender)
        End Sub

#End Region

#Region " ANNUNCIATE "

        Private Sub Annunciate(ByVal sender As Object, ByVal message As String)
            Me.Annunciate(TryCast(sender, Control), message)
        End Sub

        Private Sub Annunciate(ByVal sender As Control, ByVal message As String)
            If sender IsNot Nothing Then Me.ErrorProvider.SetError(sender, message)
        End Sub

#End Region

#Region " SENSOR "

        Dim _DeviceSensorType As isr.Core.Pith.EnumExtender(Of DeviceSensorType)
        ''' <summary> Displays a sensor types. </summary>
        Private Sub DisplaySensorTypes()
            Me._DeviceSensorType = New isr.Core.Pith.EnumExtender(Of DeviceSensorType)
            Me._DeviceSensorType.ListValues(Me._DeviceSensorTypeSelector.ComboBox)
        End Sub

        Private Sub _DeviceThermalConstantSelector_ValueSelected(sender As System.Object, e As System.EventArgs) Handles _DeviceThermalConstantSelector.ValueSelected
        End Sub

        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _DeviceSensorTypeSelector_ValueSelected(sender As System.Object, e As System.EventArgs) Handles _DeviceSensorTypeSelector.ValueSelected
            Try
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                Me.ErrorProvider.Clear()
                If Me._DeviceSensorType IsNot Nothing Then

                End If
                If Me._DeviceSensorType IsNot Nothing AndAlso sender IsNot Nothing AndAlso
                    Me.IsDeviceOpen Then
                    Dim v As DeviceSensorType = Me._DeviceSensorType.SelectedValue(Me._DeviceSensorTypeSelector.ComboBox)
                    With Me.Device.ThermoStreamSubsystem
                        .ApplyDeviceSensorType(v)
                        .QueryDeviceSensorType()
                        If .DeviceSensorType.GetValueOrDefault(0) = DeviceSensorType.None Then
                            .ApplyDeviceControl(False)
                        Else
                            .ApplyDeviceControl(True)
                        End If
                        .QueryDeviceControl()
                        .QueryDeviceThermalConstant()
                    End With
                    Me.readServiceRequest()
                End If
            Catch ex As Exception
                Me.Annunciate(sender, ex.Message)
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception occurred setting and reading the device sensor type;. {0}", ex.ToFullBlownString)
            Finally
                Me.Cursor = System.Windows.Forms.Cursors.Default
            End Try
        End Sub

#End Region

    End Class

End Namespace

Imports NationalInstruments
Namespace T1750

    ''' <summary> Defines a Status Subsystem for a Tegam 1750 Resistance Measuring System. </summary>
    ''' <license> (c) 2013 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="10/7/2013" by="David" revision=""> Created. </history>
    Public Class StatusSubsystem
        Inherits Visa.R2D2.StatusSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> Initializes a new instance of the <see cref="StatusSubsystem" /> class. </summary>
        ''' <param name="visaSession"> A reference to a <see cref="Visa.Session">message based
        ''' session</see>. </param>
        Public Sub New(ByVal visaSession As Visa.Session)
            MyBase.New(visaSession)

            ' clear all unsupported commands.

            ' reset and clear not supported

            MyBase.ServiceRequestEnableBitmask = CType(63, ServiceRequests)

            Me.OperationCompleted = True
            Me.VersionInfo = New VersionInfo()
        End Sub

#End Region

#Region " I PRESETTABLE "

        ''' <summary> Sets the subsystem values to their known execution reset state. Reset is accomplished
        ''' using Clear Active State. </summary>
        Public Overrides Sub ResetKnownState()
            MyBase.ResetKnownState()
            MyBase.ClearActiveState()
            Me.OperationCompleted = True
        End Sub

#End Region

#Region " PUBLISHER "

        ''' <summary> Publishes all values by raising the property changed events. </summary>
        Public Overrides Sub Publish()
            If Me.Publishable Then
                For Each p As Reflection.PropertyInfo In Reflection.MethodInfo.GetCurrentMethod.DeclaringType.GetProperties()
                    Me.SafePostPropertyChanged(p.Name)
                Next
            End If
        End Sub

#End Region

#Region " COMMAND SYNTAX "

        ''' <summary> Gets the preset command. </summary>
        ''' <value> The preset command. </value>
        Protected Overrides ReadOnly Property PresetCommand As String
            Get
                Return ""
            End Get
        End Property

#Region " STANDARD STATUS  "

        ''' <summary> Gets the clear execution state command. </summary>
        ''' <value> The clear execution state command. </value>
        Protected Overrides ReadOnly Property ClearExecutionStateCommand As String
            Get
                Return "" ' Ieee488.Syntax.ClearExecutionStateCommand
            End Get
        End Property

        ''' <summary> Gets the reset known state command. </summary>
        ''' <value> The reset known state command. </value>
        Protected Overrides ReadOnly Property ResetKnownStateCommand As String
            Get
                ' reset is done using clear active state.
                Return ""
            End Get
        End Property

        ''' <summary> Gets the clear error queue command. </summary>
        ''' <value> The clear error queue command. </value>
        Protected Overrides ReadOnly Property ClearErrorQueueCommand As String
            Get
                Return ""
            End Get
        End Property

        ''' <summary> Gets the error queue query command. </summary>
        ''' <value> The error queue query command. </value>
        Protected Overrides ReadOnly Property ErrorQueueQueryCommand As String
            Get
                ' errors are read using the system subsystem
                Return ""
            End Get
        End Property

        ''' <summary> Gets the bits that would be set for detecting if an error is available. </summary>
        ''' <value> The error available bits. </value>
        Public Overrides ReadOnly Property ErrorAvailableBits As ServiceRequests
            Get
                Return ServiceRequests.StandardEvent
            End Get
        End Property

        ''' <summary> Gets the identity query command. </summary>
        ''' <value> The identity query command. </value>
        Protected Overrides ReadOnly Property IdentityQueryCommand As String
            Get
                Return "U2x"
            End Get
        End Property

        ''' <summary> Gets the bits that would be set for detecting if an Measurement is available. </summary>
        ''' <value> The Measurement available bits. </value>
        Public Overrides ReadOnly Property MeasurementAvailableBits As ServiceRequests
            Get
                Return ServiceRequests.MeasurementEvent
            End Get
        End Property

        ''' <summary> Gets the bits that would be set for detecting if an Message is available. </summary>
        ''' <value> The Message available bits. </value>
        Public Overrides ReadOnly Property MessageAvailableBits As ServiceRequests
            Get
                Return ServiceRequests.None
            End Get
        End Property


        ''' <summary> Gets the operation completed query command. </summary>
        ''' <value> The operation completed query command. </value>
        Protected Overrides ReadOnly Property OperationCompletedQueryCommand As String
            Get
                ' not supported. Set to completed.
                Return ""
            End Get
        End Property

        ''' <summary> Gets the bits that would be set for detecting if a Standard Event is available. </summary>
        ''' <value> The Standard Event available bits. </value>
        Public Overrides ReadOnly Property StandardEventAvailableBits As ServiceRequests
            Get
                Return ServiceRequests.None
            End Get
        End Property

        ''' <summary> Gets the standard event status query command. </summary>
        ''' <value> The standard event status query command. </value>
        Protected Overrides ReadOnly Property StandardEventQueryCommand As String
            Get
                'Return Ieee488.Syntax.StandardEventQueryCommand
                Return ""
            End Get
        End Property

        ''' <summary> Gets the standard event enable query command. </summary>
        ''' <value> The standard event enable query command. </value>
        Protected Overrides ReadOnly Property StandardEventEnableQueryCommand As String
            Get
                Return "" ' Ieee488.Syntax.StandardEventEnableQueryCommand
            End Get
        End Property

        ''' <summary> Gets the standard service enable command format. </summary>
        ''' <value> The standard service enable command format. </value>
        Protected Overrides ReadOnly Property StandardServiceEnableCommandFormat As String
            Get
                Return "" ' Ieee488.Syntax.StandardServiceEnableCommandFormat
            End Get
        End Property

        ''' <summary> Gets the standard service enable and complete command format. </summary>
        ''' <value> The standard service enable command and complete format. </value>
        Protected Overrides ReadOnly Property StandardServiceEnableCompleteCommandFormat As String
            Get
                Return "" ' Ieee488.Syntax.StandardServiceEnableCompleteCommandFormat
            End Get
        End Property

        ''' <summary> Gets the service request enable command format. </summary>
        ''' <value> The service request enable command format. </value>
        Protected Overrides ReadOnly Property ServiceRequestEnableCommandFormat As String
            Get
                ' MyBase.ServiceRequestEnableCommandFormat = "M{0:00}x"
                Return "M{0:00}x"
            End Get
        End Property

#End Region

#End Region

#Region " IDENTITY "

        ''' <summary> Parses version information. </summary>
        ''' <param name="value"> The value. </param>
        Public Overrides Sub ParseVersionInfo(value As String)
            Me.VersionInfo.Parse(value)
        End Sub

        ''' <summary> Gets or sets the information describing the version. </summary>
        ''' <value> Information describing the version. </value>
        Public Property VersionInfo As VersionInfo

#End Region

    End Class

End Namespace

Namespace K2700

    ''' <summary> Holds a single set of 27xx instrument reading elements. </summary>
    ''' <license> (c) 2005 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="01/15/08" by="David" revision="2.0.2936.x"> Create based on the 24xx
    ''' system classes. </history>
    Public Class Readings
        Inherits isr.IO.Visa.ReadingsBase(Of Double)

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> Constructs this class. </summary>
        Public Sub New()

            ' instantiate the base class
            MyBase.New()

            Me._Reading = New isr.IO.Visa.MeasurandDouble()
            Me._Reading.DisplayCaption.Units.PhysicalUnits = isr.Core.primitives.PhysicalUnit.Current
            Me._Reading.SaveCaption.Units = Me._Reading.DisplayCaption.Units
            Me._Reading.ComplianceLimit = isr.IO.Visa.Infinity
            Me._Reading.HighLimit = isr.IO.Visa.Infinity
            Me._Reading.LowLimit = isr.IO.Visa.NegativeInfinity
            Me._Reading.ReadingLength = 15

            Me._Timestamp = New isr.IO.Visa.ReadingDouble()
            Me._Timestamp.DisplayCaption.Units.PhysicalUnits = isr.Core.Primitives.PhysicalUnit.Time
            Me._Timestamp.SaveCaption.Units = Me._Timestamp.DisplayCaption.Units
            Me._Timestamp.ReadingLength = 7

            Me._ReadingNumber = New isr.IO.Visa.ReadingLong()
            Me._ReadingNumber.DisplayCaption.Units.PhysicalUnits = isr.Core.Primitives.PhysicalUnit.None
            Me._ReadingNumber.SaveCaption.Units = Me._ReadingNumber.DisplayCaption.Units
            Me._ReadingNumber.ReadingLength = 4

            Me._Channel = New isr.IO.Visa.ReadingLong()
            Me._Channel.DisplayCaption.Units.PhysicalUnits = isr.Core.Primitives.PhysicalUnit.None
            Me._Channel.SaveCaption.Units = Me._Channel.DisplayCaption.Units
            Me._Channel.ReadingLength = 3

            Me._Limits = New isr.IO.Visa.ReadingLong()
            Me._Limits.DisplayCaption.Units.PhysicalUnits = isr.Core.Primitives.PhysicalUnit.None
            Me._Limits.SaveCaption.Units = Me._Limits.DisplayCaption.Units
            Me._Limits.ReadingLength = 4

        End Sub

        ''' <summary> Create a copy of the model. </summary>
        ''' <param name="model"> The model. </param>
        Public Sub New(ByVal model As Readings)

            ' instantiate the base class
            MyBase.New()

            If model IsNot Nothing Then
                Me._Reading = New isr.IO.Visa.MeasurandDouble(model._Reading)
                Me._Timestamp = New isr.IO.Visa.ReadingDouble(model._Timestamp)
                Me._ReadingNumber = New isr.IO.Visa.ReadingLong(model._ReadingNumber)
                Me._Channel = New isr.IO.Visa.ReadingLong(model._Channel)
                Me._Limits = New isr.IO.Visa.ReadingLong(model._Limits)
                Me.Elements = model._elements
            End If

        End Sub

        ''' <summary> Clones this class. </summary>
        ''' <returns> A copy of this object. </returns>
        Public Function Clone() As Readings
            Return New Readings(Me)
        End Function

#End Region

#Region " PARSE "

        ''' <summary> Parses reading data into a readings array. </summary>
        ''' <param name="baseReading">    Specifies the base reading which includes the limits for all
        ''' reading elements. </param>
        ''' <param name="readingRecords"> The reading records. </param>
        ''' <returns> Readings[][]. </returns>
        ''' <exception cref="System.ArgumentNullException"> readingRecords, baseReading. </exception>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope",
            Justification:="Object is returned")>
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Multi")>
        Public Shared Function ParseMulti(ByVal baseReading As Readings, ByVal readingRecords As String) As Readings()

            If readingRecords Is Nothing Then
                Throw New ArgumentNullException("readingRecords")
            ElseIf readingRecords.Length = 0 Then
                Dim r As Readings() = {}
                Return r
            ElseIf baseReading Is Nothing Then
                Throw New ArgumentNullException("baseReading")
            End If

            Dim readings As String() = readingRecords.Split(","c)
            If readings.Length < baseReading.ElementsCount Then
                Dim r As Readings() = {}
                Return r
            End If

            Dim readingsArray(readings.Length \ baseReading.ElementsCount - 1) As Readings
            Dim j As Integer = 0
            For i As Integer = 0 To readings.Length - 1 Step baseReading.ElementsCount
                Dim reading As New Readings(baseReading)
                reading.Parse(readings, i)
                readingsArray(j) = reading
                j += 1
            Next
            Return readingsArray

        End Function

#End Region

#Region " READING ELEMENTS "

        Private _Elements As isr.IO.Visa.ReadingElements

        ''' <summary> Gets or sets the reading elements. </summary>
        ''' <value> The elements. </value>
        ''' <remarks> Adds reading elements in the order they are returned by the instrument so as to
        ''' automate parsing of these data. </remarks>
        Public Property Elements() As isr.IO.Visa.ReadingElements
            Get
                Return Me._elements
            End Get
            Set(value As isr.IO.Visa.ReadingElements)
                If Not value.Equals(Me.Elements) Then
                    Me._elements = value
                    Me.Readings = New ReadingCollection
                    If (value And isr.IO.Visa.ReadingElements.Reading) <> 0 Then
                        MyBase.AddReading(Me.Reading)
                    End If
                    If (value And isr.IO.Visa.ReadingElements.Timestamp) <> 0 Then
                        MyBase.AddReading(Me.Timestamp)
                    End If
                    If (value And isr.IO.Visa.ReadingElements.ReadingNumber) <> 0 Then
                        MyBase.AddReading(Me.ReadingNumber)
                    End If
                    If (value And isr.IO.Visa.ReadingElements.Channel) <> 0 Then
                        MyBase.AddReading(Me.Channel)
                    End If
                    If (value And isr.IO.Visa.ReadingElements.Limits) <> 0 Then
                        MyBase.AddReading(Me.Limits)
                    End If
                    If (value And isr.IO.Visa.ReadingElements.Units) <> 0 Then
                        MyBase.AddReading(Me.Units)
                    End If
                End If
            End Set
        End Property

        ''' <summary>Gets or sets the <see cref="isr.IO.Visa.Readinglong">channel number</see>.</summary>
        Public Property Channel() As isr.IO.Visa.ReadingLong

        ''' <summary>Gets or sets the <see cref="isr.IO.Visa.Readinglong">limits</see>.</summary>
        Public Property Limits() As isr.IO.Visa.ReadingLong

        ''' <summary>Gets or sets the DMM <see cref="isr.IO.Visa.ReadingDouble">reading</see>.</summary>
        Public Property Reading() As isr.IO.Visa.MeasurandDouble

        ''' <summary>Gets or sets the <see cref="isr.IO.Visa.Readinglong">reading number</see>.</summary>
        Public Property ReadingNumber() As isr.IO.Visa.ReadingLong

        ''' <summary>Gets or sets the timestamp <see cref="isr.IO.Visa.ReadingDouble">reading</see>.</summary>
        Public Property Timestamp() As isr.IO.Visa.ReadingDouble

        ''' <summary>Gets or sets the units <see cref="isr.IO.Visa.StringReading">reading</see>.</summary>
        Public Property Units() As isr.IO.Visa.StringReading

#End Region

    End Class

End Namespace
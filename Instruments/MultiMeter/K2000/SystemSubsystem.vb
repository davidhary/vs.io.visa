Imports NationalInstruments
Namespace K2000

    ''' <summary> Defines a System Subsystem for a Keithley 2000 instrument. </summary>
    ''' <license> (c) 2013 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="9/22/2013" by="David" revision="3.0.5013"> Created. </history>
    Public Class SystemSubsystem
        Inherits Visa.Scpi.SystemSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> Initializes a new instance of the <see cref="SystemSubsystem" /> class. </summary>
        ''' <param name="statusSubsystem "> A reference to a <see cref="IO.VISA.StatusSubsystemBase">message based
        ''' session</see>. </param>
        Public Sub New(ByVal statusSubsystem As IO.Visa.StatusSubsystemBase)
            MyBase.New(statusSubsystem)
            Me._AddPresettableValues()
        End Sub

#End Region

#Region " I PRESETTABLE "

        ''' <summary> Adds Presettable values. </summary>
        Private Sub _AddPresettableValues()
            Me.ScpiRevision.Command = New Command(":SYST:VERS", CommandFormats.Standard Or CommandFormats.QuerySupported)
        End Sub

        ''' <summary> Sets the subsystem values to their known execution reset state. </summary>
        Public Overrides Sub ResetKnownState()
            MyBase.ResetKnownState()
            Me.AutoZeroEnabled = True
            Me.BeeperEnabled = True
            Me.FourWireSenseEnabled = False
            Me.QueryFrontSwitched()
        End Sub

#End Region

#Region " PUBLISHER "

        ''' <summary> Publishes all values by raising the property changed events. </summary>
        Public Overrides Sub Publish()
            If Me.Publishable Then
                For Each p As Reflection.PropertyInfo In Reflection.MethodInfo.GetCurrentMethod.DeclaringType.GetProperties()
                    Me.SafePostPropertyChanged(p.Name)
                Next
            End If
        End Sub

#End Region

#Region " COMMAND SYNTAX "

        ''' <summary> Gets the clear error queue command. </summary>
        ''' <value> The clear error queue command. </value>
        Protected Overrides ReadOnly Property ClearErrorQueueCommand As String
            Get
                Return Visa.Scpi.Syntax.ClearSystemErrorQueueCommand
            End Get
        End Property

        ''' <summary> Gets the initialize memory command. </summary>
        ''' <value> The initialize memory command. </value>
        Protected Overrides ReadOnly Property InitializeMemoryCommand As String
            Get
                Return Visa.Scpi.Syntax.InitializeMemoryCommand
            End Get
        End Property

        ''' <summary> Gets the last error query command. </summary>
        ''' <value> The last error query command. </value>
        Protected Overrides ReadOnly Property LastErrorQueryCommand As String
            Get
                Return Visa.Scpi.Syntax.LastErrorQueryCommand
            End Get
        End Property

        ''' <summary> Gets line frequency query command. </summary>
        ''' <value> The line frequency query command. </value>
        Protected Overrides ReadOnly Property LineFrequencyQueryCommand As String
            Get
                Return Visa.Scpi.Syntax.ReadLineFrequencyCommand
            End Get
        End Property

        ''' <summary> Gets the preset command. </summary>
        ''' <value> The preset command. </value>
        Protected Overrides ReadOnly Property PresetCommand As String
            Get
                Return Visa.Scpi.Syntax.SystemPresetCommand
            End Get
        End Property

#End Region

#Region " AUTO ZERO ENABLED "

        Private _AutoZeroEnabled As Boolean?

        ''' <summary> Gets or sets a cached value indicating whether auto zero is enabled. </summary>
        ''' <value> <c>True</c> if auto zero is enabled; <c>False</c> if not or none if not set or
        ''' unknown. </value>
        Public Property AutoZeroEnabled() As Boolean?
            Get
                Return Me._AutoZeroEnabled
            End Get
            Protected Set(ByVal value As Boolean?)
                If Not Boolean?.Equals(Me.AutoZeroEnabled, value) Then
                    Me._AutoZeroEnabled = value
                    Me.SafePostPropertyChanged()
                End If
            End Set
        End Property

        ''' <summary> Writes and reads back the auto zero enabled state. </summary>
        ''' <param name="value"> If set to <c>True</c> enable. </param>
        ''' <returns> <c>True</c> if auto zero is enabled; <c>False</c> if not or none if not set or
        ''' unknown. </returns>
        Public Function ApplyAutoZeroEnabled(ByVal value As Boolean) As Boolean?
            Me.WriteAutoZeroEnabled(value)
            If Me.LastStatus >= VisaNS.VisaStatusCode.Success Then
                Me.QueryAutoZeroEnabled()
            End If
            Return Me.AutoZeroEnabled
        End Function

        ''' <summary> Queries the auto zero enabled state. </summary>
        ''' <returns> <c>True</c> if auto zero is enabled; <c>False</c> if not or none if not set or
        ''' unknown. </returns>
        Public Function QueryAutoZeroEnabled() As Boolean?
            If Me.IsSessionOpen Then
                Me.AutoZeroEnabled = Me.Session.Query(True, ":SYST:AZER?")
            End If
            Return Me.AutoZeroEnabled
        End Function

        ''' <summary> Writes the auto zero enabled state without reading back the actual value. </summary>
        ''' <param name="value"> if set to <c>True</c> [value]. </param>
        ''' <returns> <c>True</c> if auto zero is enabled; <c>False</c> if not or none if not set or
        ''' unknown. </returns>
        Public Function WriteAutoZeroEnabled(ByVal value As Boolean) As Boolean?
            If Me.IsSessionOpen Then
                Me.Session.WriteLine(":SYST:AZER {0:'ON';'ON';'OFF'}", CType(value, Integer))
            End If
            If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
                Me.AutoZeroEnabled = New Boolean?
            Else
                Me.AutoZeroEnabled = value
            End If
            Return Me.AutoZeroEnabled
        End Function

#End Region

#Region " BEEPER ENABLED "

        Private _BeeperEnabled As Boolean?

        ''' <summary> Gets or sets a cached value indicating whether Beeper is enabled. </summary>
        ''' <value> <c>True</c> if Beeper is enabled; <c>False</c> if not or none if not set or
        ''' unknown. </value>
        Public Property BeeperEnabled() As Boolean?
            Get
                Return Me._BeeperEnabled
            End Get
            Protected Set(ByVal value As Boolean?)
                If Not Boolean?.Equals(Me.BeeperEnabled, value) Then
                    Me._BeeperEnabled = value
                    Me.SafePostPropertyChanged()
                End If
            End Set
        End Property

        ''' <summary> Writes and reads back the Beeper enabled state. </summary>
        ''' <param name="value"> If set to <c>True</c> enable. </param>
        ''' <returns> <c>True</c> if Beeper is enabled; <c>False</c> if not or none if not set or
        ''' unknown. </returns>
        Public Function ApplyBeeperEnabled(ByVal value As Boolean) As Boolean?
            Me.WriteBeeperEnabled(value)
            If Me.LastStatus >= VisaNS.VisaStatusCode.Success Then
                Me.QueryBeeperEnabled()
            End If
            Return Me.BeeperEnabled
        End Function

        ''' <summary> Queries the Beeper enabled state. </summary>
        ''' <returns> <c>True</c> if Beeper is enabled; <c>False</c> if not or none if not set or
        ''' unknown. </returns>
        Public Function QueryBeeperEnabled() As Boolean?
            If Me.IsSessionOpen Then
                Me.BeeperEnabled = Me.Session.Query(True, ":SYST:BEEP:STAT?")
            End If
            Return Me.BeeperEnabled
        End Function

        ''' <summary> Writes the Beeper enabled state without reading back the actual value. </summary>
        ''' <param name="value"> if set to <c>True</c> [value]. </param>
        ''' <returns> <c>True</c> if Beeper is enabled; <c>False</c> if not or none if not set or
        ''' unknown. </returns>
        Public Function WriteBeeperEnabled(ByVal value As Boolean) As Boolean?
            If Me.IsSessionOpen Then
                Me.Session.WriteLine(":SYST:BEEP:STAT {0:'1';'1';'0'}", CType(value, Integer))
            End If
            If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
                Me.BeeperEnabled = New Boolean?
            Else
                Me.BeeperEnabled = value
            End If
            Return Me.BeeperEnabled
        End Function

        ''' <summary> Commands the instrument to issue a Beep on the instrument. </summary>
        ''' <param name="frequency"> Specifies the frequency of the beep. </param>
        ''' <param name="duration">  Specifies the duration of the beep. </param>
        Public Sub BeepImmediately(ByVal frequency As Integer, ByVal duration As Single)
            Me.Session.WriteLine(":SYST:BEEP:IMM {0}, {1}", frequency, duration)
        End Sub

#End Region

#Region " FOUR WIRE SENSE ENABLED "

        Private _FourWireSenseEnabled As Boolean?

        ''' <summary> Gets or sets a value indicating whether four wire sense is enabled. </summary>
        ''' <value> <c>True</c> if four wire sense mode is enabled; otherwise, <c>False</c>. </value>
        Public Property FourWireSenseEnabled() As Boolean?
            Get
                Return Me._FourWireSenseEnabled
            End Get
            Protected Set(ByVal value As Boolean?)
                If Not Boolean?.Equals(Me.FourWireSenseEnabled, value) Then
                    Me._FourWireSenseEnabled = value
                    Me.SafePostPropertyChanged()
                End If
            End Set
        End Property

        ''' <summary> Writes and reads back the Four Wire Sense enabled state. </summary>
        ''' <param name="value"> If set to <c>True</c> enable. </param>
        ''' <returns> <c>True</c> if Four Wire Sense is enabled; <c>False</c> if not or none if not set or
        ''' unknown. </returns>
        Public Function ApplyFourWireSenseEnabled(ByVal value As Boolean) As Boolean?
            Me.WriteFourWireSenseEnabled(value)
            If Me.LastStatus >= VisaNS.VisaStatusCode.Success Then
                Me.QueryWireSenseEnabled()
            End If
            Return Me.FourWireSenseEnabled
        End Function

        ''' <summary> Queries the Four Wire Sense enabled state. </summary>
        ''' <returns> <c>True</c> if Four Wire Sense is enabled; <c>False</c> if not or none if not set or
        ''' unknown. </returns>
        Public Function QueryWireSenseEnabled() As Boolean?
            If Me.IsSessionOpen Then
                Me.FourWireSenseEnabled = Me.Session.Query(True, ":SYST:RSEN?")
            End If
            Return Me.FourWireSenseEnabled
        End Function

        ''' <summary> Writes the Four Wire Sense enabled state without reading back the actual value. </summary>
        ''' <param name="value"> if set to <c>True</c> [value]. </param>
        ''' <returns> <c>True</c> if Four Wire Sense is enabled; <c>False</c> if not or none if not set or
        ''' unknown. </returns>
        Public Function WriteFourWireSenseEnabled(ByVal value As Boolean) As Boolean?
            Me.FourWireSenseEnabled = New Boolean?
            If Me.IsSessionOpen Then
                Me.Session.WriteLine(":SYST:RSEN {0:'ON';'ON';'OFF'}", CType(value, Integer))
            End If
            If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
                Me.FourWireSenseEnabled = New Boolean?
            Else
                Me.FourWireSenseEnabled = value
            End If
            Return Me.FourWireSenseEnabled
        End Function

#End Region

#Region " Front Switched "

        Private _FrontSwitched As Boolean?

        ''' <summary> Gets or sets a cached value indicating whether Front is Switched. </summary>
        ''' <value> <c>True</c> if Front is Switched; <c>False</c> if not or none if not set or
        ''' unknown. </value>
        Public Property FrontSwitched() As Boolean?
            Get
                Return Me._FrontSwitched
            End Get
            Protected Set(ByVal value As Boolean?)
                If Not Boolean?.Equals(Me.FrontSwitched, value) Then
                    Me._FrontSwitched = value
                    Me.SafePostPropertyChanged()
                End If
            End Set
        End Property

        ''' <summary> Queries the Front Switched state. </summary>
        ''' <returns> <c>True</c> if Front is Switched; <c>False</c> if not or none if not set or
        ''' unknown. </returns>
        Public Function QueryFrontSwitched() As Boolean?
            If Me.IsSessionOpen Then
                Me.FrontSwitched = Me.Session.Query(True, ":SYST:FRSW?")
            End If
            Return Me.FrontSwitched
        End Function

#End Region

    End Class

End Namespace

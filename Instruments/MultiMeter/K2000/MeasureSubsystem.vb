Namespace K2000

    ''' <summary> Defines a Measure Subsystem for a Keithley 2000 instrument. </summary>
    ''' <license> (c) 2012 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="9/26/2012" by="David" revision="1.0.4652"> Created. </history>
    Public Class MeasureSubsystem
        Inherits Visa.Scpi.MeasureSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> Initializes a new instance of the <see cref="MeasureSubsystem" /> class. </summary>
        ''' <param name="statusSubsystem "> A reference to a <see cref="IO.VISA.StatusSubsystemBase">message based
        ''' session</see>. </param>
        Public Sub New(ByVal statusSubsystem As IO.Visa.StatusSubsystemBase)
            MyBase.New(statusSubsystem)
            Me._readings = New Readings
            ' this is the default setting for the format sub system
            Me.Readings.Elements = ReadingElements.Reading
        End Sub

#End Region

#Region " I PRESETTABLE "

#End Region

#Region " PUBLISHER "

        ''' <summary> Publishes all values by raising the property changed events. </summary>
        Public Overrides Sub Publish()
            If Me.Publishable Then
                For Each p As Reflection.PropertyInfo In Reflection.MethodInfo.GetCurrentMethod.DeclaringType.GetProperties()
                    Me.SafePostPropertyChanged(p.Name)
                Next
            End If
        End Sub

#End Region

#Region " COMMAND SYNTAX "

        ''' <summary> Gets the preset command. </summary>
        ''' <value> The preset command. </value>
        Protected Overrides ReadOnly Property PresetCommand As String
            Get
                Return ""
            End Get
        End Property

#Region "  INIT, READ, FETCH "

        ''' <summary> Gets the initiate command. </summary>
        ''' <value> The initiate command. </value>
        Protected Overrides ReadOnly Property InitiateCommand As String
            Get
                Return ""
            End Get
        End Property

        ''' <summary> Gets the fetch command. </summary>
        ''' <value> The fetch command. </value>
        Protected Overrides ReadOnly Property FetchCommand As String
            Get
                Return ":READ?"
            End Get
        End Property

        ''' <summary> Gets the read command. </summary>
        ''' <value> The read command. </value>
        Protected Overrides ReadOnly Property ReadCommand As String
            Get
                Return ":READ?"
            End Get
        End Property

#End Region

#End Region

#Region " PARSE READING "

        Private _Readings As Readings

        ''' <summary> Returns the readings. </summary>
        ''' <returns> The readings. </returns>
        Public Function Readings() As Readings
            Return Me._readings
        End Function

        ''' <summary> Parses a new set of reading elements. </summary>
        ''' <param name="reading"> Specifies the measurement text to parse into the new reading. </param>
        Public Overrides Sub ParseReading(ByVal reading As String)

            ' Take a reading and parse the results
            If Me.Readings.TryParse(reading) Then

            End If

#If False Then
            ' TO_DO: 
            ' Move to the status event property changed event.
            ' When reading is parsed, the measurement available property changes. Use that to report the following.
            If Me.Reading.Reading.MeasurandStatus.HitCompliance Then

                Me.OnMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceId, "INSTRUMENT HIT COMPLIANCE",
                                      "Real Compliance.  Instrument sensed an output overflow of the measured value.")

            ElseIf Me.Reading.Reading.MeasurandStatus.HitRangeCompliance Then

                Me.OnMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceId, "INSTRUMENT HIT RANGE COMPLIANCE",
                                      "Range Compliance.  Instrument sensed an output overflow of the measured value.")

            ElseIf Me.Reading.Reading.MeasurandStatus.HitLevelCompliance Then

                Me.OnMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceId, "INSTRUMENT HIT LEVEL COMPLIANCE",
                                      "Level Compliance.  Instrument sensed an output overflow of the measured value.")


            Else

                Me.OnMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceId, "INSTRUMENT PARSED DATA", "Instruments parsed reading elements.")

            End If
#End If

        End Sub

#End Region

    End Class

End Namespace

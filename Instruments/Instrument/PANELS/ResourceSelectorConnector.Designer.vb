Imports System.Drawing
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ResourceSelectorConnectorBase

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                Me.onDisposeManagedResources()
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ResourceSelectorConnectorBase))
        Me._toolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._ResourceNamesComboBox = New System.Windows.Forms.ComboBox()
        Me._FindButton = New System.Windows.Forms.Button()
        Me._imageList = New System.Windows.Forms.ImageList(Me.components)
        Me._ConnectToggle = New System.Windows.Forms.CheckBox()
        Me._ClearButton = New System.Windows.Forms.Button()
        Me._mainTableLayoutPanel = New System.Windows.Forms.TableLayoutPanel()
        Me._ErrorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        Me._mainTableLayoutPanel.SuspendLayout()
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_ResourceNamesComboBox
        '
        Me._ResourceNamesComboBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._ResourceNamesComboBox.Font = New System.Drawing.Font(Me.Font, System.Drawing.FontStyle.Bold)
        Me._ResourceNamesComboBox.Location = New System.Drawing.Point(38, 9)
        Me._ResourceNamesComboBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._ResourceNamesComboBox.Name = "_ResourceNamesComboBox"
        Me._ResourceNamesComboBox.Size = New System.Drawing.Size(239, 25)
        Me._ResourceNamesComboBox.TabIndex = 5
        Me._toolTip.SetToolTip(Me._ResourceNamesComboBox, "Select an item from the list")
        '
        '_FindButton
        '
        Me._FindButton.Dock = System.Windows.Forms.DockStyle.Fill
        Me._FindButton.ImageIndex = 3
        Me._FindButton.ImageList = Me._imageList
        Me._FindButton.Location = New System.Drawing.Point(283, 6)
        Me._FindButton.Margin = New System.Windows.Forms.Padding(3, 1, 3, 1)
        Me._FindButton.Name = "_FindButton"
        Me._FindButton.Size = New System.Drawing.Size(29, 31)
        Me._FindButton.TabIndex = 6
        Me._toolTip.SetToolTip(Me._FindButton, "Search for items")
        Me._FindButton.UseVisualStyleBackColor = True
        '
        '_imageList
        '
        Me._imageList.ImageStream = CType(resources.GetObject("_imageList.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me._imageList.TransparentColor = System.Drawing.SystemColors.Control
        Me._imageList.Images.SetKeyName(0, "")
        Me._imageList.Images.SetKeyName(1, "")
        Me._imageList.Images.SetKeyName(2, "")
        Me._imageList.Images.SetKeyName(3, "")
        '
        '_ConnectToggle
        '
        Me._ConnectToggle.Appearance = System.Windows.Forms.Appearance.Button
        Me._ConnectToggle.Dock = System.Windows.Forms.DockStyle.Fill
        Me._ConnectToggle.Enabled = False
        Me._ConnectToggle.ImageIndex = 1
        Me._ConnectToggle.ImageList = Me._imageList
        Me._ConnectToggle.Location = New System.Drawing.Point(318, 6)
        Me._ConnectToggle.Margin = New System.Windows.Forms.Padding(3, 1, 3, 1)
        Me._ConnectToggle.Name = "_ConnectToggle"
        Me._ConnectToggle.Size = New System.Drawing.Size(29, 31)
        Me._ConnectToggle.TabIndex = 7
        Me._toolTip.SetToolTip(Me._ConnectToggle, "Depress to connect or release to disconnect")
        Me._ConnectToggle.UseVisualStyleBackColor = True
        '
        '_ClearButton
        '
        Me._ClearButton.Dock = System.Windows.Forms.DockStyle.Fill
        Me._ClearButton.Enabled = False
        Me._ClearButton.ImageIndex = 0
        Me._ClearButton.ImageList = Me._imageList
        Me._ClearButton.Location = New System.Drawing.Point(3, 6)
        Me._ClearButton.Margin = New System.Windows.Forms.Padding(3, 1, 3, 1)
        Me._ClearButton.Name = "_ClearButton"
        Me._ClearButton.Size = New System.Drawing.Size(29, 31)
        Me._ClearButton.TabIndex = 8
        Me._toolTip.SetToolTip(Me._ClearButton, "Clear Selected Item")
        Me._ClearButton.UseVisualStyleBackColor = True
        '
        '_mainTableLayoutPanel
        '
        Me._mainTableLayoutPanel.ColumnCount = 4
        Me._mainTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._mainTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._mainTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._mainTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._mainTableLayoutPanel.Controls.Add(Me._ResourceNamesComboBox, 1, 1)
        Me._mainTableLayoutPanel.Controls.Add(Me._ClearButton, 0, 1)
        Me._mainTableLayoutPanel.Controls.Add(Me._ConnectToggle, 3, 1)
        Me._mainTableLayoutPanel.Controls.Add(Me._FindButton, 2, 1)
        Me._mainTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._mainTableLayoutPanel.Location = New System.Drawing.Point(0, 0)
        Me._mainTableLayoutPanel.Margin = New System.Windows.Forms.Padding(0)
        Me._mainTableLayoutPanel.Name = "_mainTableLayoutPanel"
        Me._mainTableLayoutPanel.RowCount = 3
        Me._mainTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._mainTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._mainTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._mainTableLayoutPanel.Size = New System.Drawing.Size(350, 43)
        Me._mainTableLayoutPanel.TabIndex = 10
        '
        '_ErrorProvider
        '
        Me._ErrorProvider.ContainerControl = Me
        '
        'ResourceSelectorConnector
        '
        Me.BackColor = System.Drawing.Color.Transparent
        Me.Controls.Add(Me._mainTableLayoutPanel)
        Me.Name = "ResourceSelectorConnector"
        Me.Size = New System.Drawing.Size(350, 43)
        Me._mainTableLayoutPanel.ResumeLayout(False)
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _ToolTip As System.Windows.Forms.ToolTip
    Private WithEvents _ImageList As System.Windows.Forms.ImageList
    Private WithEvents _ResourceNamesComboBox As System.Windows.Forms.ComboBox
    Private WithEvents _FindButton As System.Windows.Forms.Button
    Private WithEvents _ConnectToggle As System.Windows.Forms.CheckBox
    Private WithEvents _ClearButton As System.Windows.Forms.Button
    Private WithEvents _MainTableLayoutPanel As System.Windows.Forms.TableLayoutPanel
    Private WithEvents _ErrorProvider As System.Windows.Forms.ErrorProvider

End Class

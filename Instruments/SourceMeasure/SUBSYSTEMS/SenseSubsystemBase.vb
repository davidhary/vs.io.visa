Imports NationalInstruments
Imports isr.Core.Pith
Imports isr.Core.Pith.EnumExtensions
''' <summary> Defines the contract that must be implemented by a Sense Subsystem. </summary>
''' <license> (c) 2012 Integrated Scientific ReSenses, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="9/26/2012" by="David" revision="1.0.4652"> Created. </history>
Public MustInherit Class SenseSubsystemBase
    Inherits Visa.SenseSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="SenseSubsystemBase" /> class. </summary>
    ''' <param name="statusSubsystem "> A reference to a <see cref="IO.VISA.StatusSubsystemBase">status subsystem</see>. </param>
    Protected Sub New(ByVal statusSubsystem As IO.Visa.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Sets the subsystem values to their known execution reset state. </summary>
    Public Overrides Sub ResetKnownState()
        MyBase.ResetKnownState()
        Me.FunctionModes = SenseFunctionModes.VoltageDC
        Me.ConcurrentSenseEnabled = New Boolean?
    End Sub

#End Region

#Region " AUTO RANGE ENABLED "

    ''' <summary> Auto Range enabled. </summary>
    Private _AutoRangeEnabled As Boolean?

    ''' <summary> Gets or sets the cached Auto Range Enabled sentinel. </summary>
    ''' <value> <c>null</c> if Auto Range Enabled is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>. </value>
    Public Property AutoRangeEnabled As Boolean?
        Get
            Return Me._AutoRangeEnabled
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.AutoRangeEnabled, value) Then
                Me._AutoRangeEnabled = value
                Me.SafePostPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Auto Range Enabled sentinel. </summary>
    ''' <param name="value">  if set to <c>True</c> if enabling; False if disabling. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function ApplyAutoRangeEnabled(ByVal value As Boolean) As Boolean?
        Me.WriteAutoRangeEnabled(value)
        If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
            Return Me.AutoRangeEnabled
        Else
            Return Me.QueryAutoRangeEnabled()
        End If
    End Function

    ''' <summary> Gets or sets the automatic Range enabled query command. </summary>
    ''' <value> The automatic Range enabled query command. </value>
    ''' <remarks> SCPI: "system:RANG:AUTO?" </remarks>
    Protected MustOverride ReadOnly Property AutoRangeEnabledQueryCommand As String

    ''' <summary> Queries the Auto Range Enabled sentinel. Also sets the
    ''' <see cref="AutoRangeEnabled">Enabled</see> sentinel. </summary>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function QueryAutoRangeEnabled() As Boolean?
        Me.AutoRangeEnabled = MyBase.Query(Me.AutoRangeEnabled, Me.AutoRangeEnabledQueryCommand)
        Return Me.AutoRangeEnabled
    End Function

    ''' <summary> Gets or sets the automatic Range enabled command Format. </summary>
    ''' <value> The automatic Range enabled query command. </value>
    ''' <remarks> SCPI: "system:RANGE:AUTO {0:'ON';'ON';'OFF'}" </remarks>
    Protected MustOverride ReadOnly Property AutoRangeEnabledCommandFormat As String

    ''' <summary> Writes the Auto Range Enabled sentinel. Does not read back from the instrument. </summary>
    ''' <param name="value"> if set to <c>True</c> is enabled. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function WriteAutoRangeEnabled(ByVal value As Boolean) As Boolean?
        Me.AutoRangeEnabled = MyBase.Write(value, Me.AutoRangeEnabledCommandFormat)
        Return Me.AutoRangeEnabled
    End Function

#End Region

#Region " CONCURRENT FUNCTION MODE "

    ''' <summary> State of the output on. </summary>
    Private _ConcurrentSenseEnabled As Boolean?

    ''' <summary> Gets or sets the cached Concurrent Function Mode Enabled sentinel. </summary>
    ''' <value> <c>null</c> if mode is not known; <c>True</c> if concurrent; otherwise, <c>False</c>. </value>
    Public Property ConcurrentSenseEnabled As Boolean?
        Get
            Return Me._ConcurrentSenseEnabled
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.ConcurrentSenseEnabled, value) Then
                Me._ConcurrentSenseEnabled = value
                Me.SafePostPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Queries the Concurrent Function Mode Enabled sentinel. 
    '''           Also sets the <see cref="ConcurrentSenseEnabled">Enabled</see> sentinel. </summary>
    ''' <returns> <c>null</c> if mode is not known; <c>True</c> if concurrent; otherwise, <c>False</c>. </returns>
    Public MustOverride Function QueryConcurrentSenseEnabled() As Boolean?

    ''' <summary> Writes the Concurrent Function Mode Enabled sentinel. Does not read back from the instrument. </summary>
    ''' <param name="value"> if set to <c>True</c> is concurrent. </param>
    ''' <returns> <c>null</c> if mode is not known; <c>True</c> if concurrent; otherwise, <c>False</c>. </returns>
    Public MustOverride Function WriteConcurrentSenseEnabled(ByVal value As Boolean) As Boolean?

    ''' <summary> Writes and reads back the Concurrent Function Mode Enabled sentinel. </summary>
    ''' <param name="value"> if set to <c>True</c> is concurrent. </param>
    ''' <returns> <c>null</c> if mode is not known; <c>True</c> if concurrent; otherwise, <c>False</c>. </returns>
    Public Function ApplyConcurrentSenseEnabled(ByVal value As Boolean) As Boolean?
        Me.WriteConcurrentSenseEnabled(value)
        If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
            Return Me.ConcurrentSenseEnabled
        Else
            Return Me.QueryConcurrentSenseEnabled()
        End If
    End Function

#End Region

#Region " POWER LINE CYCLES (NPLC) "

    ''' <summary> The Power Line Cycles. </summary>
    Private _PowerLineCycles As Double?

    ''' <summary> Gets or sets the cached sense PowerLineCycles. </summary>
    ''' <value> <c>null</c> if value is not known. </value>
    Public Overloads Property PowerLineCycles As Double?
        Get
            Return Me._PowerLineCycles
        End Get
        Protected Set(ByVal value As Double?)
            If Not Nullable.Equals(Me.PowerLineCycles, value) Then
                Me._PowerLineCycles = value
                Me.SafePostPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the sense PowerLineCycles. </summary>
    ''' <param name="value"> The Power Line Cycles. </param>
    ''' <returns> The Power Line Cycles. </returns>
    Public Function ApplyPowerLineCycles(ByVal value As Double) As Double?
        Me.WritePowerLineCycles(value)
        If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
            Return Me.PowerLineCycles
        Else
            Return Me.QueryPowerLineCycles
        End If
    End Function

    ''' <summary> Gets or sets The Power Line Cycles query command. </summary>
    ''' <value> The Power Line Cycles query command. </value>
    Protected MustOverride ReadOnly Property PowerLineCyclesQueryCommand As String

    ''' <summary> Queries The Power Line Cycles. </summary>
    ''' <returns> The Power Line Cycles or none if unknown. </returns>
    Public Function QueryPowerLineCycles() As Double?
        Me.PowerLineCycles = MyBase.Query(Me.PowerLineCycles, Me.PowerLineCyclesQueryCommand)
        Return Me.PowerLineCycles
    End Function

    ''' <summary> Gets or sets The Power Line Cycles command format. </summary>
    ''' <value> The Power Line Cycles command format. </value>
    Protected MustOverride ReadOnly Property PowerLineCyclesCommandFormat As String

    ''' <summary> Writes The Power Line Cycles without reading back the value from the device. </summary>
    ''' <remarks> This command sets The Power Line Cycles. </remarks>
    ''' <param name="value"> The Power Line Cycles. </param>
    ''' <returns> The Power Line Cycles. </returns>
    Public Function WritePowerLineCycles(ByVal value As Double) As Double?
        Me.PowerLineCycles = MyBase.Write(value, Me.PowerLineCyclesCommandFormat)
        Return Me.PowerLineCycles
    End Function

#End Region

#Region " PROTECTION LEVEL "

    ''' <summary> The Current Limit. </summary>
    Private _ProtectionLevel As Double?

    ''' <summary> Gets or sets the cached source current Limit for a voltage source. Set to
    ''' <see cref="isr.IO.Visa.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
    ''' <see cref="isr.IO.Visa.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum. </summary>
    ''' <value> <c>null</c> if value is not known. </value>
    Public Overloads Property ProtectionLevel As Double?
        Get
            Return Me._ProtectionLevel
        End Get
        Protected Set(ByVal value As Double?)
            If Not Nullable.Equals(Me.ProtectionLevel, value) Then
                Me._ProtectionLevel = value
                Me.SafePostPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the protection level. </summary>
    ''' <param name="value"> the protection level. </param>
    ''' <returns> the protection level. </returns>
    Public Function ApplyProtectionLevel(ByVal value As Double) As Double?
        Me.WriteProtectionLevel(value)
        If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
            Return Me.ProtectionLevel
        Else
            Return Me.QueryProtectionLevel
        End If
    End Function

    ''' <summary> Gets or sets the protection level query command. </summary>
    ''' <value> the protection level query command. </value>
    Protected MustOverride ReadOnly Property ProtectionLevelQueryCommand As String

    ''' <summary> Queries the protection level. </summary>
    ''' <returns> the protection level or none if unknown. </returns>
    Public Function QueryProtectionLevel() As Double?
        Me.ProtectionLevel = MyBase.Query(Me.ProtectionLevel, Me.ProtectionLevelQueryCommand)
        Return Me.ProtectionLevel
    End Function

    ''' <summary> Gets or sets the protection level command format. </summary>
    ''' <value> the protection level command format. </value>
    Protected MustOverride ReadOnly Property ProtectionLevelCommandFormat As String

    ''' <summary> Writes the protection level without reading back the value from the device. </summary>
    ''' <remarks> This command sets the protection level. </remarks>
    ''' <param name="value"> the protection level. </param>
    ''' <returns> the protection level. </returns>
    Public Function WriteProtectionLevel(ByVal value As Double) As Double?
        Me.ProtectionLevel = MyBase.Write(value, Me.ProtectionLevelCommandFormat)
        Return Me.ProtectionLevel
    End Function

#End Region

#Region " RANGE "

    ''' <summary> The Current Range. </summary>
    Private _Range As Double?

    ''' <summary> Gets or sets the cached sense current range. Set to
    ''' <see cref="isr.IO.Visa.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
    ''' <see cref="isr.IO.Visa.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum. </summary>
    ''' <value> <c>null</c> if value is not known. </value>
    Public Overloads Property Range As Double?
        Get
            Return Me._Range
        End Get
        Protected Set(ByVal value As Double?)
            If Not Nullable.Equals(Me.Range, value) Then
                Me._Range = value
                Me.SafePostPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the sense current Range. </summary>
    ''' <remarks> The value is in Amperes. At *RST, the range is set to Auto and the specific range is unknown. </remarks>
    ''' <param name="value"> The current Range. </param>
    ''' <returns> The Current Range. </returns>
    Public Function ApplyRange(ByVal value As Double) As Double?
        Me.WriteRange(value)
        If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
            Return Me.Range
        Else
            Return Me.QueryRange
        End If
    End Function

    ''' <summary> Queries the current Range. </summary>
    ''' <returns> The current Range or none if unknown. </returns>
    Public MustOverride Function QueryRange() As Double?

    ''' <summary> Writes the current Range without reading back the value from the device. </summary>
    ''' <remarks> This command sets the current Range. The value is in Amperes. 
    '''           At *RST, the range is auto and the values is not known. </remarks>
    ''' <param name="value"> The current Range. </param>
    ''' <returns> The Current Range. </returns>
    Public MustOverride Function WriteRange(ByVal value As Double) As Double?

#End Region

#Region " FUNCTION MODES "

    ''' <summary> Builds the Modes record for the specified Modes. </summary>
    ''' <param name="Modes"> Sense Function Modes. </param>
    ''' <returns> The record. </returns>
    Public Shared Function BuildRecord(ByVal modes As SenseFunctionModes) As String
        If modes = SenseFunctionModes.None Then
            Return String.Empty
        Else
            Dim reply As New System.Text.StringBuilder
            For Each code As Integer In [Enum].GetValues(GetType(SenseFunctionModes))
                If (modes And code) <> 0 Then
                    Dim value As String = CType(code, SenseFunctionModes).ExtractBetween()
                    If Not String.IsNullOrWhiteSpace(value) Then
                        If reply.Length > 0 Then reply.Append(",")
                        reply.Append(value)
                    End If
                End If
            Next
            Return reply.ToString
        End If
    End Function

    ''' <summary> Returns the <see cref="SenseFunctionModes"></see> from the specified value. </summary>
    ''' <param name="value"> The Modes. </param>
    ''' <returns> The sense function mode. </returns>
    Public Shared Function ParseSenseFunctionMode(ByVal value As String) As SenseFunctionModes
        If String.IsNullOrWhiteSpace(value) Then
            Return SenseFunctionModes.None
        Else
            Dim se As New StringEnumerator(Of SenseFunctionModes)
            Return se.ParseContained(value.BuildDelimitedValue)
        End If
    End Function

    ''' <summary> Get the composite Sense Function Modes based on the message from the instrument. </summary>
    ''' <param name="record"> Specifies the comma delimited Modes record. </param>
    ''' <returns> The sense function modes. </returns>
    Public Shared Function ParseSenseFunctionModes(ByVal record As String) As SenseFunctionModes
        Dim parsed As SenseFunctionModes = SenseFunctionModes.None
        If Not String.IsNullOrWhiteSpace(record) Then
            For Each modeValue As String In record.Split(","c)
                parsed = parsed Or SenseSubsystemBase.ParseSenseFunctionMode(modeValue)
            Next
        End If
        Return parsed
    End Function

    ''' <summary> The Sense Function Modes. </summary>
    Private _FunctionModes As SenseFunctionModes?

    ''' <summary> Gets or sets the cached Sense Function Modes. </summary>
    ''' <value> The Function Modes or null if unknown. </value>
    Public Property FunctionModes As SenseFunctionModes?
        Get
            Return Me._FunctionModes
        End Get
        Protected Set(ByVal value As SenseFunctionModes?)
            If Not Nullable.Equals(Me.FunctionModes, value) Then
                Me._FunctionModes = value
                Me.SafePostPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Queries the Sense Function Modes. </summary>
    ''' <returns> The Function Modes or null if unknown. </returns>
    Public MustOverride Function QueryFunctionModes() As SenseFunctionModes?

    ''' <summary> Writes the Sense Function Modes. Does not read back from the instrument. </summary>
    ''' <param name="value"> The Sense Function Mode. </param>
    ''' <returns> The Function Modes or null if unknown. </returns>
    Public MustOverride Function WriteFunctionModes(ByVal value As SenseFunctionModes) As SenseFunctionModes?

    ''' <summary> Writes and reads back the Sense Function Modes. </summary>
    ''' <param name="value"> The <see cref="SenseFunctionModes">Function Modes</see>. </param>
    ''' <returns> The Sense Function Modes or null if unknown. </returns>
    Public Function ApplyFunctionModes(ByVal value As SenseFunctionModes) As SenseFunctionModes?
        Me.WriteFunctionModes(value)
        If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
            Return Me.FunctionModes
        Else
            Return Me.QueryFunctionModes()
        End If
    End Function

    Private _SupportsMultiFunctions As Boolean

    ''' <summary> Gets or sets the condition telling if the instrument supports multi-functions. For
    ''' example, the 2400 source-measure instrument support measuring voltage, current, and
    ''' resistance concurrently whereas the 2700 supports a single function at a time. </summary>
    ''' <value> The supports multi functions. </value>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Multi")>
    Public Property SupportsMultiFunctions() As Boolean
        Get
            Return Me._supportsMultiFunctions
        End Get
        Set(ByVal value As Boolean)
            If Not Me.SupportsMultiFunctions.Equals(value) Then
                Me._supportsMultiFunctions = value
                Me.SafePostPropertyChanged()
            End If
        End Set
    End Property

    Private _SupportedFunctionModes As SenseFunctionModes
    ''' <summary>
    ''' Gets or sets the supported function modes.
    ''' This is a subset of the functions supported by the instrument.
    ''' </summary>
    Public Property SupportedFunctionModes() As SenseFunctionModes
        Get
            Return _SupportedFunctionModes
        End Get
        Set(ByVal value As SenseFunctionModes)
            If Not Me.SupportedFunctionModes.Equals(value) Then
                Me._SupportedFunctionModes = value
                Me.SafePostPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " UNITS "

    Private Shared _UnitParserHash As Dictionary(Of SenseFunctionModes, Arebis.TypedUnits.Unit)
    ''' <summary> Builds Unit Parser hash. </summary>
    ''' <returns> A Dictionary for translating SCPI unit names to <see cref="Arebis.StandardUnits">standard units</see>. </returns>
    Public Shared Function BuildUnitParserHash() As Dictionary(Of SenseFunctionModes, Arebis.TypedUnits.Unit)
        Dim dix2 As New Dictionary(Of SenseFunctionModes, Arebis.TypedUnits.Unit)
        Dim dix3 As Dictionary(Of SenseFunctionModes, Arebis.TypedUnits.Unit) = dix2
        dix3.Add(SenseFunctionModes.Continuity, Arebis.StandardUnits.ElectricUnits.Ohm)
        dix3.Add(SenseFunctionModes.Current, Arebis.StandardUnits.ElectricUnits.Ampere)
        dix3.Add(SenseFunctionModes.CurrentAC, Arebis.StandardUnits.ElectricUnits.Ampere)
        dix3.Add(SenseFunctionModes.CurrentDC, Arebis.StandardUnits.ElectricUnits.Ampere)
        dix3.Add(SenseFunctionModes.FourWireResistance, Arebis.StandardUnits.ElectricUnits.Ohm)
        dix3.Add(SenseFunctionModes.Frequency, Arebis.StandardUnits.FrequencyUnits.Hertz)
        dix3.Add(SenseFunctionModes.Period, Arebis.StandardUnits.TimeUnits.Second)
        dix3.Add(SenseFunctionModes.Resistance, Arebis.StandardUnits.ElectricUnits.Ohm)
        dix3.Add(SenseFunctionModes.Temperature, Arebis.StandardUnits.TemperatureUnits.DegreeCelsius)
        dix3.Add(SenseFunctionModes.TimestampElement, Arebis.StandardUnits.TimeUnits.Second)
        dix3.Add(SenseFunctionModes.Voltage, Arebis.StandardUnits.ElectricUnits.Volt)
        dix3.Add(SenseFunctionModes.VoltageAC, Arebis.StandardUnits.ElectricUnits.Volt)
        dix3.Add(SenseFunctionModes.VoltageDC, Arebis.StandardUnits.ElectricUnits.Volt)
        dix3 = Nothing
        Return dix2
    End Function

    ''' <summary> Parses the function modes to create the corresponding units. 
    '''           Assume a single function mode is defined. </summary>
    ''' <param name="functionMode"> The function mode. </param>
    ''' <param name="unit">         [in,out] The unit. </param>
    ''' <returns> <c>True</c> if parsed. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="1#")>
    Public Overloads Shared Function TryParse(ByVal functionMode As SenseFunctionModes, ByRef unit As Arebis.TypedUnits.Unit) As Boolean
        If SenseSubsystemBase._UnitParserHash Is Nothing Then
            SenseSubsystemBase._UnitParserHash = SenseSubsystemBase.BuildUnitParserHash
        End If
        If SenseSubsystemBase._UnitParserHash.ContainsKey(functionMode) Then
            unit = SenseSubsystemBase._UnitParserHash(functionMode)
        Else
            unit = Nothing
        End If
        Return unit IsNot Nothing
    End Function

    ''' <summary> Parses the function to create the units. </summary>
    ''' <param name="functionMode"> The function mode. </param>
    ''' <param name="units">        [in,out] The units. </param>
    ''' <returns> <c>True</c> if parsed. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="1#")>
    Public Overloads Shared Function TryParse(ByVal functionMode As SenseFunctionModes, ByRef units As List(Of Arebis.TypedUnits.Unit)) As Boolean
        If SenseSubsystemBase._UnitParserHash Is Nothing Then
            SenseSubsystemBase._UnitParserHash = SenseSubsystemBase.BuildUnitParserHash
        End If
        If units Is Nothing Then
            units = New List(Of Arebis.TypedUnits.Unit)
        Else
            units.Clear()
        End If
        For Each fm As SenseFunctionModes In [Enum].GetValues(GetType(SenseFunctionModes))
            fm = fm And functionMode
            If (fm <> 0) AndAlso SenseSubsystemBase._UnitParserHash.ContainsKey(fm) Then
                Dim unit As Arebis.TypedUnits.Unit = SenseSubsystemBase._UnitParserHash(fm)
                If unit IsNot Nothing AndAlso units IsNot Nothing Then
                    units.Add(unit)
                End If
            End If
        Next
        Return units IsNot Nothing AndAlso units.Count > 0
    End Function

#End Region

End Class

''' <summary>Specifies the sense function modes.</summary>
<System.Flags()> Public Enum SenseFunctionModes
    <ComponentModel.Description("Not specified")> None = 0
    <ComponentModel.Description("Voltage ('VOLT')")> Voltage = 1
    <ComponentModel.Description("Current ('CURR')")> Current = 2 * SenseFunctionModes.Voltage
    <ComponentModel.Description("DC Voltage ('VOLT:DC')")> VoltageDC = 2 * SenseFunctionModes.Current
    <ComponentModel.Description("DC Current ('CURR:DC')")> CurrentDC = 2 * SenseFunctionModes.VoltageDC
    <ComponentModel.Description("AC Voltage ('VOLT:AC')")> VoltageAC = 2 * SenseFunctionModes.CurrentDC
    <ComponentModel.Description("AC Current ('CURR:AC')")> CurrentAC = 2 * SenseFunctionModes.VoltageAC
    <ComponentModel.Description("Resistance ('RES')")> Resistance = 2 * SenseFunctionModes.CurrentAC
    <ComponentModel.Description("Four-Wire Resistance ('FRES')")> FourWireResistance = 2 * SenseFunctionModes.Resistance
    <ComponentModel.Description("Temperature ('TEMP')")> Temperature = 2 * SenseFunctionModes.FourWireResistance
    <ComponentModel.Description("Frequency ('FREQ')")> Frequency = 2 * SenseFunctionModes.Temperature
    <ComponentModel.Description("Period ('PER')")> Period = 2 * SenseFunctionModes.Frequency
    <ComponentModel.Description("Continuity ('CONT')")> Continuity = 2 * SenseFunctionModes.Period
    <ComponentModel.Description("Timestamp element ('TIME')")> TimestampElement = 2 * SenseFunctionModes.Continuity
    <ComponentModel.Description("Status Element ('STAT')")> StatusElement = 2 * SenseFunctionModes.TimestampElement
    <ComponentModel.Description("Memory ('MEM')")> Memory = 2 * SenseFunctionModes.StatusElement
End Enum


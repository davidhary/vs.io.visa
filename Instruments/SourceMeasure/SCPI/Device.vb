﻿Imports NationalInstruments
Imports isr.Core.Pith.ExceptionExtensions
Namespace Scpi

    ''' <summary> Implements a generic source measure device. </summary>
    ''' <remarks> An instrument is defined, for the purpose of this library, as a device with a front
    ''' panel. </remarks>
    ''' <license> (c) 2013 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="9/10/2013" by="David" revision="3.0.5001"> Created. </history>
    Public Class Device
        Inherits DeviceBase

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> Initializes a new instance of the <see cref="SourceMeasure.Scpi.Device" /> class. </summary>
        Public Sub New()
            MyBase.New()
            Me.InitializeTimeout = TimeSpan.FromMilliseconds(5000)
            Me.ResourcesSearchPattern = Visa.InstrumentSearchPattern()
        End Sub

#Region " IDisposable Support "

        ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        ''' <remarks> Executes in two distinct scenarios as determined by its disposing parameter.  If True,
        ''' the method has been called directly or indirectly by a user's code--managed and unmanaged
        ''' resources can be disposed. If disposing equals False, the method has been called by the
        ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
        ''' resources can be disposed. </remarks>
        ''' <param name="disposing"> True if this method releases both managed and unmanaged resources;
        ''' False if this method releases only unmanaged resources. </param>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Protected Overrides Sub Dispose(disposing As Boolean)
            Try
                If Not MyBase.IsDisposed Then
                    If disposing Then
                        ' dispose managed state (managed objects).
                        Me.OnClosing(New ComponentModel.CancelEventArgs)
                    End If
                End If
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, "Exception disposing device", "Exception {0}", ex.ToFullBlownString)
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

#End Region

#End Region

#Region " I PRESETTABLE "

        ''' <summary> Clears the Device. Issues <see cref="StatusSubsystemBase.ClearActiveState">Selective
        ''' Device Clear</see>. </summary>
        Public Overrides Sub ClearActiveState()
            Me.StatusSubsystem.ClearActiveState()
        End Sub

#End Region

#Region " PUBLISHER "

        ''' <summary> Publishes all values by raising the property changed events. </summary>
        Public Overrides Sub Publish()
            Me.Subsystems.Publish()
            If Me.Publishable Then
                For Each p As Reflection.PropertyInfo In Reflection.MethodInfo.GetCurrentMethod.DeclaringType.GetProperties()
                    Me.SafePostPropertyChanged(p.Name)
                Next
            End If
        End Sub

#End Region

#Region " SESSION "

        Private _IsDeviceOpen As Boolean
        ''' <summary> Gets or sets a value indicating whether the device is open. This is
        '''           required when the device is used in emulation. </summary>
        ''' <value> <c>True</c> if the device has an open session; otherwise, <c>False</c>. </value>
        Public Overrides Property IsDeviceOpen As Boolean
            Get
                Return Me._IsDeviceOpen
            End Get
            Set(ByVal value As Boolean)
                If Not Me.IsDeviceOpen.Equals(value) Then
                    Me._IsDeviceOpen = value
                    Me.SafePostPropertyChanged()
                    If Me.StatusSubsystem IsNot Nothing Then
                        Me.StatusSubsystem.IsDeviceOpen = Me.IsDeviceOpen
                    End If
                End If
            End Set
        End Property

        ''' <summary> Allows the derived device to take actions after closing. </summary>
        Protected Overrides Sub OnClosed()
            MyBase.OnClosed()
        End Sub

        ''' <summary> Allows the derived device to take actions before closing. Removes subsystems and
        ''' event handlers. </summary>
        ''' <param name="e"> Event information to send to registered event handlers. </param>
        Protected Overrides Sub OnClosing(e As System.ComponentModel.CancelEventArgs)
            MyBase.OnClosing(e)
            If e Is Nothing OrElse Not e.Cancel Then
                If Me._MeasureVoltageSubsystem IsNot Nothing Then
                    Me._MeasureVoltageSubsystem.Dispose()
                    Me._MeasureVoltageSubsystem = Nothing
                End If
                If Me._MeasureCurrentSubsystem IsNot Nothing Then
                    Me._MeasureCurrentSubsystem.Dispose()
                    Me._MeasureCurrentSubsystem = Nothing
                End If
                If Me._MeasureSubsystem IsNot Nothing Then
                    Me._MeasureSubsystem.Dispose()
                    Me._MeasureSubsystem = Nothing
                End If
                If Me._TriggerSubsystem IsNot Nothing Then
                    Me._TriggerSubsystem.Dispose()
                    Me._TriggerSubsystem = Nothing
                End If
                If Me._SourceSubsystem IsNot Nothing Then
                    Me._SourceSubsystem.Dispose()
                    Me._SourceSubsystem = Nothing
                End If
                If Me._SourceVoltageSubsystem IsNot Nothing Then
                    Me._SourceVoltageSubsystem.Dispose()
                    Me._SourceVoltageSubsystem = Nothing
                End If
                If Me._SourceCurrentSubsystem IsNot Nothing Then
                    Me._SourceCurrentSubsystem.Dispose()
                    Me._SourceCurrentSubsystem = Nothing
                End If
                If Me._SenseSubsystem IsNot Nothing Then
                    Me._SenseSubsystem.Dispose()
                    Me._SenseSubsystem = Nothing
                End If
                If Me._SenseVoltageSubsystem IsNot Nothing Then
                    Me._SenseVoltageSubsystem.Dispose()
                    Me._SenseVoltageSubsystem = Nothing
                End If
                If Me._SenseResistanceSubsystem IsNot Nothing Then
                    Me._SenseResistanceSubsystem.Dispose()
                    Me._SenseResistanceSubsystem = Nothing
                End If
                If Me._SenseCurrentSubsystem IsNot Nothing Then
                    Me._SenseCurrentSubsystem.Dispose()
                    Me._SenseCurrentSubsystem = Nothing
                End If
                If Me._RouteSubsystem IsNot Nothing Then
                    Me._RouteSubsystem.Dispose()
                    Me._RouteSubsystem = Nothing
                End If
                If Me._OutputSubsystem IsNot Nothing Then
                    Me._OutputSubsystem.Dispose()
                    Me._OutputSubsystem = Nothing
                End If
                If Me._SystemSubsystem IsNot Nothing Then
                    'RemoveHandler Me.SystemSubsystem.PropertyChanged, AddressOf SystemSubsystemPropertyChanged
                    RemoveHandler Me.SystemSubsystem.TraceMessageAvailable, AddressOf Me.OnTraceMessageAvailable
                    Me._SystemSubsystem.Dispose()
                    Me._SystemSubsystem = Nothing
                End If
                If Me._StatusSubsystem IsNot Nothing Then
                    Me.StatusSubsystem.IsDeviceOpen = False
                    RemoveHandler Me.StatusSubsystem.PropertyChanged, AddressOf StatusSubsystemPropertyChanged
                    RemoveHandler Me.StatusSubsystem.TraceMessageAvailable, AddressOf Me.OnTraceMessageAvailable
                    Me._StatusSubsystem.Dispose()
                    Me._StatusSubsystem = Nothing
                End If
            End If
        End Sub

        ''' <summary> Allows the derived device to take actions after opening. Adds subsystems and event
        ''' handlers. </summary>
        Protected Overrides Sub OnOpened()

            ' prevent adding multiple times
            If MyBase.Subsystems IsNot Nothing AndAlso MyBase.Subsystems.Count <> 0 Then
                Debug.Assert(Not Debugger.IsAttached, "Unexpected attempt to add subsystems on top of subsystems")
            Else
                ' STATUS must be the first subsystem.
                Me._StatusSubsystem = New StatusSubsystem(MyBase.Session)
                MyBase.AddSubsystem(Me.StatusSubsystem)
                AddHandler Me.StatusSubsystem.PropertyChanged, AddressOf StatusSubsystemPropertyChanged
                AddHandler Me.StatusSubsystem.TraceMessageAvailable, AddressOf Me.OnTraceMessageAvailable

                Me._SystemSubsystem = New SystemSubsystem(Me.StatusSubsystem)
                MyBase.AddSubsystem(Me.SystemSubsystem)
                'AddHandler Me.SystemSubsystem.PropertyChanged, AddressOf SystemSubsystemPropertyChanged
                AddHandler Me.SystemSubsystem.TraceMessageAvailable, AddressOf Me.OnTraceMessageAvailable

                Me._OutputSubsystem = New OutputSubsystem(Me.StatusSubsystem)
                MyBase.AddSubsystem(Me.OutputSubsystem)

                Me._RouteSubsystem = New RouteSubsystem(Me.StatusSubsystem)
                MyBase.AddSubsystem(Me.RouteSubsystem)

                Me._SenseCurrentSubsystem = New SenseCurrentSubsystem(Me.StatusSubsystem)
                MyBase.AddSubsystem(Me.SenseCurrentSubsystem)

                Me._SenseVoltageSubsystem = New SenseVoltageSubsystem(Me.StatusSubsystem)
                MyBase.AddSubsystem(Me.SenseVoltageSubsystem)

                Me._SenseResistanceSubsystem = New SenseResistanceSubsystem(Me.StatusSubsystem)
                MyBase.AddSubsystem(Me.SenseResistanceSubsystem)

                Me._SenseSubsystem = New SenseSubsystem(Me.StatusSubsystem)
                MyBase.AddSubsystem(Me.SenseSubsystem)

                Me._SourceCurrentSubsystem = New SourceCurrentSubsystem(Me.StatusSubsystem)
                MyBase.AddSubsystem(Me.SourceCurrentSubsystem)

                Me._SourceVoltageSubsystem = New SourceVoltageSubsystem(Me.StatusSubsystem)
                MyBase.AddSubsystem(Me.SourceVoltageSubsystem)

                Me._SourceSubsystem = New SourceSubsystem(Me.StatusSubsystem)
                MyBase.AddSubsystem(Me.SourceSubsystem)

                Me._TriggerSubsystem = New TriggerSubsystem(Me.StatusSubsystem)
                MyBase.AddSubsystem(Me.TriggerSubsystem)

                Me._MeasureSubsystem = New MeasureSubsystem(Me.StatusSubsystem)
                MyBase.AddSubsystem(Me.MeasureSubsystem)

                Me._MeasureCurrentSubsystem = New MeasureCurrentSubsystem(Me.StatusSubsystem)
                MyBase.AddSubsystem(Me.MeasureCurrentSubsystem)

                Me._MeasureVoltageSubsystem = New MeasureVoltageSubsystem(Me.StatusSubsystem)
                MyBase.AddSubsystem(Me.MeasureVoltageSubsystem)
            End If

            Me.StatusSubsystem.EnableServiceRequest(ServiceRequests.All)
            MyBase.OnOpened()

        End Sub

        ''' <summary> Allows the derived device to take actions before opening. </summary>
        ''' <param name="e"> Event information to send to registered event handlers. </param>
        Protected Overrides Sub OnOpening(e As System.ComponentModel.CancelEventArgs)
            MyBase.OnOpening(e)
        End Sub

#End Region

#Region " SUBSYSTEMS "

        ''' <summary>
        ''' Gets or sets the Measure subsystem.
        ''' </summary>
        ''' <value>The Measure subsystem.</value>
        Public Property MeasureSubsystem As MeasureSubsystem

        ''' <summary>
        ''' Gets or sets the Measure Current subsystem.
        ''' </summary>
        ''' <value>The Measure Current subsystem.</value>
        Public Property MeasureCurrentSubsystem As MeasureCurrentSubsystem

        ''' <summary>
        ''' Gets or sets the Measure Voltage subsystem.
        ''' </summary>
        ''' <value>The Measure Voltage subsystem.</value>
        Public Property MeasureVoltageSubsystem As MeasureVoltageSubsystem

        ''' <summary>
        ''' Gets or sets the Output subsystem.
        ''' </summary>
        ''' <value>The Output subsystem.</value>
        Public Property OutputSubsystem As OutputSubsystem

        ''' <summary>
        ''' Gets or sets the Route subsystem.
        ''' </summary>
        ''' <value>The Route subsystem.</value>
        Public Property RouteSubsystem As RouteSubsystem

        ''' <summary>
        ''' Gets or sets the Sense Current subsystem.
        ''' </summary>
        ''' <value>The Sense Current subsystem.</value>
        Public Property SenseCurrentSubsystem As SenseCurrentSubsystem

        ''' <summary>
        ''' Gets or sets the Sense Resistance subsystem.
        ''' </summary>
        ''' <value>The Sense Resistance subsystem.</value>
        Public Property SenseResistanceSubsystem As SenseResistanceSubsystem

        ''' <summary>
        ''' Gets or sets the Sense Voltage subsystem.
        ''' </summary>
        ''' <value>The Sense Voltage subsystem.</value>
        Public Property SenseVoltageSubsystem As SenseVoltageSubsystem

        ''' <summary>
        ''' Gets or sets the Sense subsystem.
        ''' </summary>
        ''' <value>The Sense subsystem.</value>
        Public Property SenseSubsystem As SenseSubsystem

        ''' <summary>
        ''' Gets or sets the Source Current subsystem.
        ''' </summary>
        ''' <value>The Source Current subsystem.</value>
        Public Property SourceCurrentSubsystem As SourceCurrentSubsystem

        ''' <summary>
        ''' Gets or sets the Source Voltage subsystem.
        ''' </summary>
        ''' <value>The Source Voltage subsystem.</value>
        Public Property SourceVoltageSubsystem As SourceVoltageSubsystem

        ''' <summary>
        ''' Gets or sets the Source subsystem.
        ''' </summary>
        ''' <value>The Source subsystem.</value>
        Public Property SourceSubsystem As SourceSubsystem

        ''' <summary>
        ''' Gets or sets the Trigger Subsystem.
        ''' </summary>
        ''' <value>The Trigger Subsystem.</value>
        Public Property TriggerSubsystem As TriggerSubsystem

#Region " STATUS "

        ''' <summary>
        ''' Gets or sets the Status Subsystem.
        ''' </summary>
        ''' <value>The Status Subsystem.</value>
        Public Property StatusSubsystem As StatusSubsystem

        ''' <summary> Status subsystem property changed. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Property Changed event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub StatusSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
            Try
                If sender IsNot Nothing AndAlso
                    e IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(e.PropertyName) Then
                    Dim subsystem As StatusSubsystem = CType(sender, StatusSubsystem)
                    Select Case e.PropertyName
                        Case "Identity"
                            If Not String.IsNullOrWhiteSpace(subsystem.Identity) Then
                                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "{0} identified as {1}.",
                                                           Me.ResourceName, subsystem.Identity)

                            End If
#If False Then
                        Case "DeviceErrors"
                        Case "ErrorAvailable"
                        Case "ErrorAvailableBits"
                        Case "MeasurementAvailable"
                        Case "MeasurementAvailableBits"
                        Case "MeasurementEventCondition"
                        Case "MeasurementEventEnableBitmask"
                        Case "MeasurementEventStatus"
                        Case "MessageAvailable"
                        Case "MessageAvailableBits"
                        Case "OperationEventCondition"
                        Case "OperationEventEnableBitmask"
                        Case "OperationEventStatus"
                        Case "OperationNegativeTransitionEventEnableBitmask"
                        Case "OperationPositiveTransitionEventEnableBitmask"
                        Case "QuestionableEventCondition"
                        Case "QuestionableEventEnableBitmask"
                        Case "QuestionableEventStatus"
                        Case "ServiceRequestEnableBitmask"
                        Case "ServiceRequestStatus"
                        Case "StandardDeviceErrorAvailable"
                        Case "StandardDeviceErrorAvailableBits"
                        Case "StandardEventEnableBitmask"
                        Case "StandardEventStatus"
                        Case "VersionInfo"
#End If
                    End Select
                End If
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, "Exception handling property", "Exception handling '{0}' property change. {1}",
                             e.PropertyName, ex.ToFullBlownString)
            End Try
        End Sub

#End Region

#Region " SYSTEM "

        ''' <summary>
        ''' Gets or sets the System Subsystem.
        ''' </summary>
        ''' <value>The System Subsystem.</value>
        Public Property SystemSubsystem As SystemSubsystem

#If False Then
        ''' <summary> System subsystem property changed. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Property Changed event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub SystemSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
            Try
                If e IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(e.PropertyName) Then
                    Select Case e.PropertyName
                        Case "LastError"
                        Case "LineFrequency"
                        Case "StationLineFrequency"
                    End Select
                End If
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, "Exception handling property", "Exception handling '{0}' property change. {1}",
                             e.PropertyName, ex.ToFullBlownString)
            End Try
        End Sub
#End If

#End Region

#End Region

#Region " MIXED SUBSYSTEMS: OUTPUT / MEASURE "

        ''' <summary>
        ''' Sets the specified voltage and current limit and turns on the output.
        ''' </summary>
        ''' <param name="voltage">The voltage.</param>
        ''' <param name="currentLimit">The current limit.</param>
        ''' <param name="overvoltageLimit">The over voltage limit.</param>
        ''' <returns><c>True</c> if successful <see cref="VisaNS.VisaStatusCode">VISA Status Code</see>
        ''' was returned; <c>False</c> otherwise.</returns>
        ''' <remarks> See 2400 system manual page 102 of 592. </remarks>
        Public Function OutputOn(ByVal voltage As Double, ByVal currentLimit As Double, ByVal overvoltageLimit As Double) As Boolean

            ' turn output off.
            Me.OutputSubsystem.ApplyOutputOnState(False)

            Me.SourceSubsystem.WriteFunctionMode(SourceFunctionMode.VoltageDC)

            ' Set the voltage
            Me.SourceVoltageSubsystem.ApplyLevel(voltage)

            ' Set the over voltage level
            Me.SourceVoltageSubsystem.ApplyProtectionLevel(overvoltageLimit)

            ' Turn on over current protection
            Me.SenseCurrentSubsystem.ApplyProtectionLevel(currentLimit)

            Me.SenseSubsystem.ApplyFunctionModes(SenseFunctionModes.CurrentDC Or SenseFunctionModes.VoltageDC)

            Me.SenseSubsystem.ApplyConcurrentSenseEnabled(True)

            ' turn output on.
            Me.OutputSubsystem.ApplyOutputOnState(True)

            Me.StatusSubsystem.QueryOperationCompleted()

            ' operation completion is not sufficient to ensure that the Device has hit the correct voltage.

            Return Me.SystemSubsystem.LastStatus >= VisaNS.VisaStatusCode.Success

        End Function

#End Region

#Region " SERVICE REQUEST "

        ''' <summary> Reads the event registers after receiving a service request. </summary>
        Protected Overrides Sub ProcessServiceRequest()
            Me.StatusSubsystem.ReadRegisters()
            If Me.StatusSubsystem.MessageAvailable Then
                ' if we have a message this needs to be processed by the subsystem requesting the message.
                ' Only thereafter the registers should be read.
            End If
            If Not Me.StatusSubsystem.MessageAvailable AndAlso Me.StatusSubsystem.ErrorAvailable Then
                Me.StatusSubsystem.QueryDeviceErrors()
            End If
            If Me.StatusSubsystem.MeasurementAvailable Then
            End If
        End Sub

#End Region

    End Class

End Namespace

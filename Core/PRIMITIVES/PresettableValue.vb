''' <summary> Implements a generic Presettable Value class. </summary>
''' <typeparam name="T"> Specifies the type parameter of the generic class. </typeparam>
''' <license> (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="03/26/2015" by="David" revision="3.0.5563"> Created. </history>
Public Class PresettableValue(Of T As {Structure, IComparable(Of T), IEquatable(Of T), IFormattable})
    Inherits PresettableValueBase
    Implements IPresettable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="PresettableValue" /> class. </summary>
    Protected Sub New(ByVal propertyName As String)
        MyBase.New(propertyName)
        Me._Value = New Nullable(Of T)
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="PresettableValue" /> class. The copy constructor. </summary>
    ''' <param name="model"> The  <see cref="PresettableValue">PresettableValue</see> object from which to copy. </param>
    Public Sub New(ByVal propertyName As String, ByVal model As PresettableValue(Of T))
        Me.New(propertyName)
        If model IsNot Nothing Then
            Me._Value = model.Value
            Me._ClearValue = model.ClearValue
            Me._InitialValue = model.InitialValue
            Me._PresetValue = model.PresetValue
            Me._ResetValue = model.ResetValue
        End If
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="PresettableValue" /> class. </summary>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal propertyName As String, ByVal value As T)
        Me.New(propertyName)
        Me._Value = value
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="PresettableValue" /> class. </summary>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal propertyName As String, ByVal value As Nullable(Of T))
        Me.New(propertyName)
        Me._Value = value
    End Sub

#End Region

#Region " SHARED "

    ''' <summary> Compares two PresettableValues. </summary>
    ''' <param name="left">  Specifies the Presettable Value to compare. </param>
    ''' <param name="right"> Specifies the Presettable Value to compare with. </param>
    ''' <returns> <c>True</c> if the PresettableValues are equal. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes")>
    Public Overloads Shared Function Equals(ByVal left As Object, ByVal right As Object) As Boolean
        Return PresettableValue(Of T).Equals(TryCast(left, PresettableValue(Of T)), TryCast(right, PresettableValue(Of T)))
    End Function

    ''' <summary> Compares two PresettableValues. </summary>
    ''' <remarks> The two PresettableValues are the same if they have the same X and Y coordinates. </remarks>
    ''' <param name="left">  Specifies the Presettable Value to compare. </param>
    ''' <param name="right"> Specifies the Presettable Value to compare with. </param>
    ''' <returns> <c>True</c> if the PresettableValues are equal. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes")>
    Public Overloads Shared Function Equals(ByVal left As PresettableValue(Of T), ByVal right As PresettableValue(Of T)) As Boolean
        If left Is Nothing Then
            Return right Is Nothing
        ElseIf right Is Nothing Then
            Return False
        Else
            Return left.Value.Equals(right.Value)
        End If
    End Function

    ''' <summary> Implements the operator =. </summary>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(ByVal left As PresettableValue(Of T), ByVal right As PresettableValue(Of T)) As Boolean
        Return PresettableValue(Of T).Equals(left, right)
    End Operator

    ''' <summary> Implements the operator &lt;&gt;. </summary>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(ByVal left As PresettableValue(Of T), ByVal right As PresettableValue(Of T)) As Boolean
        Return ((CObj(left) IsNot CObj(right)) AndAlso (left Is Nothing OrElse Not left.Equals(right)))
    End Operator

    ''' <summary> Determines whether the specified <see cref="T:System.Object" /> is equal to the
    ''' current <see cref="T:System.Object" />. </summary>
    ''' <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
    ''' <see cref="T:System.Object" />. </param>
    ''' <returns> <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />; otherwise, false. </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return Me.Equals(TryCast(obj, PresettableValue(Of T)))
    End Function

    ''' <summary> Compares two PresettableValues. </summary>
    ''' <remarks> The two PresettableValues are the same if they have the same X and Y coordinates. </remarks>
    ''' <param name="other"> Specifies the other Presettable Value to compare. </param>
    ''' <returns> <c>True</c> if the PresettableValues are equal. </returns>
    Public Overloads Function Equals(ByVal other As PresettableValue(Of T)) As Boolean
        If other Is Nothing Then
            Return False
        Else
            Return PresettableValue(Of T).Equals(Me, other)
        End If
    End Function

    ''' <summary> Creates a unique hash code. </summary>
    ''' <returns> An <see cref="System.Int32">Int32</see> value. </returns>
    Public Overloads Overrides Function GetHashCode() As Int32
        Return Me.Value.GetHashCode
    End Function

#End Region

#Region " VALUE METHODS AND PROPERTIES "

    Private _Value As Nullable(Of T)

    ''' <summary> Gets or sets the value. </summary>
    ''' <value> The value. </value>
    Public Property Value() As Nullable(Of T)
        Get
            Return _Value
        End Get
        Set(value As Nullable(Of T))
            If Not Nullable.Equals(value, Me.Value) Then
                Me._Value = value
                Me.Publish()
            End If
        End Set
    End Property

    ''' <summary> Gets a value indicating whether this object has value. </summary>
    ''' <value> <c>true</c> if this object has value; otherwise <c>false</c> </value>
    Public Overrides ReadOnly Property HasValue As Boolean
        Get
            Return Me.Value.HasValue
        End Get
    End Property

    ''' <summary> Gets or sets the range. </summary>
    ''' <value> The range. </value>
    Public Property Range As isr.Core.Pith.Range(Of T)

    ''' <summary> Sets the value without publishing it. </summary>
    Public Sub SilentSetValue(ByVal value As T)
        Me._Value = value
    End Sub

    ''' <summary> Sets the value without publishing it. </summary>
    ''' <param name="value"> The value. </param>
    Public Sub SilentSetValue(ByVal value As Nullable(Of T))
        Me._Value = value
    End Sub

#End Region

#Region " PRESETTABLE VALUES AND METHODS "

    ''' <summary> Gets or sets the clear value. </summary>
    ''' <value> The clear value. </value>
    ''' <remarks> A Null clear value is not applied. </remarks>
    Public Property ClearValue As Nullable(Of T)

    ''' <summary> Gets or sets the initial value. </summary>
    ''' <value> The initial value. </value>
    ''' <remarks> A Null initial value is not applied. </remarks>
    Public Property InitialValue As Nullable(Of T)

    ''' <summary> Gets or sets the preset value. </summary>
    ''' <value> The preset value. </value>
    ''' <remarks> A Null preset value is not applied. </remarks>
    Public Property PresetValue As Nullable(Of T)

    ''' <summary> Gets or sets the reset value. </summary>
    ''' <value> The reset value. </value>
    ''' <remarks> A Null Reset value IS applied. </remarks>
    Public Property ResetValue As Nullable(Of T)

    ''' <summary> Sets  the value to its known execution clear state. </summary>
    ''' <remarks> A Null clear value is not applied. </remarks>
    Public Overrides Sub ClearExecutionState()
        If Me.ClearValue.HasValue Then
            Me.Value = Me.ClearValue.Value
        End If
    End Sub

    ''' <summary> Sets the value to a custom setting for the subsystem. </summary>
    ''' <remarks> A Null initial value is not applied. </remarks>
    Public Overrides Sub InitializeKnownState()
        If Me.InitialValue.HasValue Then
            Me.Value = Me.InitialValue.Value
        End If
    End Sub

    ''' <summary> Sets the value to its known execution preset state. </summary>
    ''' <remarks> A Null preset value is not applied. </remarks>
    Public Overrides Sub PresetKnownState()
        If Me.PresetValue.HasValue Then
            Me.Value = Me.PresetValue.Value
        End If
    End Sub

    ''' <summary> Sets the value to its known execution reset state. </summary>
    ''' <remarks> A Null reset value IS applied. </remarks>
    Public Overrides Sub ResetKnownState()
        Me.Value = Me.ResetValue
    End Sub

#End Region

End Class

''' <summary> Presettable Boolean. </summary>
''' <license> (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="03/26/2015" by="David" revision="3.0.5563"> Created. </history>
Public Class PresettableBoolean
    Inherits PresettableValueBase
    Implements IPresettable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="PresettableBoolean" /> class. </summary>
    Protected Sub New(ByVal propertyName As String)
        MyBase.New(propertyName)
        Me._Value = New Boolean?
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="PresettableBoolean" /> class. The copy constructor. </summary>
    ''' <param name="model"> The  <see cref="PresettableBoolean">PresettableBoolean</see> object from which to copy. </param>
    Public Sub New(ByVal propertyName As String, ByVal model As PresettableBoolean)
        Me.New(propertyName)
        If model IsNot Nothing Then
            Me._Value = model.Value
            Me._ClearValue = model.ClearValue
            Me._InitialValue = model.InitialValue
            Me._PresetValue = model.PresetValue
            Me._ResetValue = model.ResetValue
        End If
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="PresettableBoolean" /> class. </summary>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal propertyName As String, ByVal value As Boolean)
        Me.New(propertyName)
        Me._Value = value
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="PresettableBoolean" /> class. </summary>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal propertyName As String, ByVal value As Boolean?)
        Me.New(propertyName)
        Me._Value = value
    End Sub

#End Region

#Region " SHARED "

    ''' <summary> Compares two PresettableBooleans. </summary>
    ''' <param name="left">  Specifies the Presettable Value to compare. </param>
    ''' <param name="right"> Specifies the Presettable Value to compare with. </param>
    ''' <returns> <c>True</c> if the PresettableBooleans are equal. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes")>
    Public Overloads Shared Function Equals(ByVal left As Object, ByVal right As Object) As Boolean
        Return PresettableBoolean.Equals(TryCast(left, PresettableBoolean), TryCast(right, PresettableBoolean))
    End Function

    ''' <summary> Compares two PresettableBooleans. </summary>
    ''' <remarks> The two PresettableBooleans are the same if they have the same X and Y coordinates. </remarks>
    ''' <param name="left">  Specifies the Presettable Value to compare. </param>
    ''' <param name="right"> Specifies the Presettable Value to compare with. </param>
    ''' <returns> <c>True</c> if the PresettableBooleans are equal. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes")>
    Public Overloads Shared Function Equals(ByVal left As PresettableBoolean, ByVal right As PresettableBoolean) As Boolean
        If left Is Nothing Then
            Return right Is Nothing
        ElseIf right Is Nothing Then
            Return False
        Else
            Return Boolean.Equals(left.Value, right.Value)
        End If
    End Function

    ''' <summary> Implements the operator =. </summary>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(ByVal left As PresettableBoolean, ByVal right As PresettableBoolean) As Boolean
        Return PresettableBoolean.Equals(left, right)
    End Operator

    ''' <summary> Implements the operator &lt;&gt;. </summary>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(ByVal left As PresettableBoolean, ByVal right As PresettableBoolean) As Boolean
        Return ((CObj(left) IsNot CObj(right)) AndAlso (left Is Nothing OrElse Not left.Equals(right)))
    End Operator

    ''' <summary> Determines whether the specified <see cref="T:System.Object" /> is equal to the
    ''' current <see cref="T:System.Object" />. </summary>
    ''' <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
    ''' <see cref="T:System.Object" />. </param>
    ''' <returns> <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />; otherwise, false. </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return Me.Equals(TryCast(obj, PresettableBoolean))
    End Function

    ''' <summary> Compares two PresettableBooleans. </summary>
    ''' <remarks> The two PresettableBooleans are the same if they have the same X and Y coordinates. </remarks>
    ''' <param name="other"> Specifies the other Presettable Value to compare. </param>
    ''' <returns> <c>True</c> if the PresettableBooleans are equal. </returns>
    Public Overloads Function Equals(ByVal other As PresettableBoolean) As Boolean
        If other Is Nothing Then
            Return False
        Else
            Return PresettableBoolean.Equals(Me, other)
        End If
    End Function

    ''' <summary> Creates a unique hash code. </summary>
    ''' <returns> An <see cref="System.Int32">Int32</see> value. </returns>
    Public Overloads Overrides Function GetHashCode() As Int32
        Return Me.Value.GetHashCode
    End Function

#End Region

#Region " VALUE METHODS AND PROPERTIES "

    Private _Value As Boolean?

    ''' <summary> Gets or sets the value. </summary>
    ''' <value> The value. </value>
    Public Property Value() As Boolean?
        Get
            Return _Value
        End Get
        Set(value As Boolean?)
            If Not Nullable.Equals(value, Me.Value) Then
                Me._Value = value
                Me.Publish()
            End If
        End Set
    End Property

    ''' <summary> Sets the value without publishing it. </summary>
    Public Sub SilentSetValue(ByVal value As Boolean)
        Me._Value = value
    End Sub

    ''' <summary> Gets a value indicating whether this object has value. </summary>
    ''' <value> <c>true</c> if this object has value; otherwise <c>false</c> </value>
    Public Overrides ReadOnly Property HasValue As Boolean
        Get
            Return Me._Value.hasvalue
        End Get
    End Property

#End Region

#Region " PRESETTABLE VALUES AND METHODS "

    ''' <summary> Gets or sets the clear value. </summary>
    ''' <value> The clear value. </value>
    Public Property ClearValue As Boolean?

    ''' <summary> Gets or sets the initial value. </summary>
    ''' <value> The initial value. </value>
    Public Property InitialValue As Boolean?

    ''' <summary> Gets or sets the preset value. </summary>
    ''' <value> The preset value. </value>
    Public Property PresetValue As Boolean?

    ''' <summary> Gets or sets the reset value. </summary>
    ''' <value> The reset value. </value>
    Public Property ResetValue As Boolean?

    ''' <summary> Sets  the value to its known execution clear state. </summary>
    Public Overrides Sub ClearExecutionState()
        If Me.ClearValue.hasvalue Then
            Me.Value = Me.ClearValue
        End If
    End Sub

    ''' <summary> Sets the value to a custom setting for the subsystem. </summary>
    Public Overrides Sub InitializeKnownState()
        If Me.InitialValue.HasValue Then
            Me.Value = Me.InitialValue
        End If
    End Sub

    ''' <summary> Sets the value to its known execution preset state. </summary>
    Public Overrides Sub PresetKnownState()
        If Me.PresetValue.HasValue Then
            Me.Value = Me.PresetValue
        End If
    End Sub

    ''' <summary> Sets the value to its known execution reset state. </summary>
    Public Overrides Sub ResetKnownState()
        Me.Value = Me.ResetValue
    End Sub

#End Region

End Class

''' <summary> Presettable string. </summary>
''' <license> (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="03/26/2015" by="David" revision="3.0.5563"> Created. </history>
Public Class PresettableString
    Inherits PresettableValueBase
    Implements IPresettable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="PresettableString" /> class. </summary>
    Protected Sub New(ByVal propertyName As String)
        MyBase.New(propertyName)
        Me._Value = String.Empty
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="PresettableString" /> class. The copy constructor. </summary>
    ''' <param name="model"> The  <see cref="PresettableString">PresettableString</see> object from which to copy. </param>
    Public Sub New(ByVal propertyName As String, ByVal model As PresettableString)
        Me.New(propertyName)
        If model IsNot Nothing Then
            Me._Value = model.Value
            Me._ClearValue = model.ClearValue
            Me._InitialValue = model.InitialValue
            Me._PresetValue = model.PresetValue
            Me._ResetValue = model.ResetValue
        End If
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="PresettableString" /> class. </summary>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal propertyName As String, ByVal value As String)
        Me.New(propertyName)
        Me._Value = value
    End Sub

#End Region

#Region " SHARED "

    ''' <summary> Compares two PresettableStrings. </summary>
    ''' <param name="left">  Specifies the Presettable Value to compare. </param>
    ''' <param name="right"> Specifies the Presettable Value to compare with. </param>
    ''' <returns> <c>True</c> if the PresettableStrings are equal. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes")>
    Public Overloads Shared Function Equals(ByVal left As Object, ByVal right As Object) As Boolean
        Return PresettableString.Equals(TryCast(left, PresettableString), TryCast(right, PresettableString))
    End Function

    ''' <summary> Compares two PresettableStrings. </summary>
    ''' <remarks> The two PresettableStrings are the same if they have the same X and Y coordinates. </remarks>
    ''' <param name="left">  Specifies the Presettable Value to compare. </param>
    ''' <param name="right"> Specifies the Presettable Value to compare with. </param>
    ''' <returns> <c>True</c> if the PresettableStrings are equal. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes")>
    Public Overloads Shared Function Equals(ByVal left As PresettableString, ByVal right As PresettableString) As Boolean
        If left Is Nothing Then
            Return right Is Nothing
        ElseIf right Is Nothing Then
            Return False
        Else
            Return String.Equals(left.Value, right.Value)
        End If
    End Function

    ''' <summary> Implements the operator =. </summary>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(ByVal left As PresettableString, ByVal right As PresettableString) As Boolean
        Return PresettableString.Equals(left, right)
    End Operator

    ''' <summary> Implements the operator &lt;&gt;. </summary>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(ByVal left As PresettableString, ByVal right As PresettableString) As Boolean
        Return ((CObj(left) IsNot CObj(right)) AndAlso (left Is Nothing OrElse Not left.Equals(right)))
    End Operator

    ''' <summary> Determines whether the specified <see cref="T:System.Object" /> is equal to the
    ''' current <see cref="T:System.Object" />. </summary>
    ''' <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
    ''' <see cref="T:System.Object" />. </param>
    ''' <returns> <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />; otherwise, false. </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return Me.Equals(TryCast(obj, PresettableString))
    End Function

    ''' <summary> Compares two PresettableStrings. </summary>
    ''' <remarks> The two PresettableStrings are the same if they have the same X and Y coordinates. </remarks>
    ''' <param name="other"> Specifies the other Presettable Value to compare. </param>
    ''' <returns> <c>True</c> if the PresettableStrings are equal. </returns>
    Public Overloads Function Equals(ByVal other As PresettableString) As Boolean
        If other Is Nothing Then
            Return False
        Else
            Return PresettableString.Equals(Me, other)
        End If
    End Function

    ''' <summary> Creates a unique hash code. </summary>
    ''' <returns> An <see cref="System.Int32">Int32</see> value. </returns>
    Public Overloads Overrides Function GetHashCode() As Int32
        Return Me.Value.GetHashCode
    End Function

#End Region

#Region " VALUE METHODS AND PROPERTIES "

    Private _Value As String

    ''' <summary> Gets or sets the value. </summary>
    ''' <value> The value. </value>
    Public Property Value() As String
        Get
            Return _Value
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.Value) Then
                Me._Value = value
                Me.publish()
            End If
        End Set
    End Property

    ''' <summary> Sets the value without publishing it. </summary>
    Public Sub SilentSetValue(ByVal value As String)
        Me._Value = value
    End Sub

    ''' <summary> Gets a value indicating whether this object has value. </summary>
    ''' <value> <c>true</c> if this object has value; otherwise <c>false</c> </value>
    Public Overrides ReadOnly Property HasValue As Boolean
        Get
            Return Not String.IsNullOrEmpty(Me._Value)
        End Get
    End Property

#End Region

#Region " PRESETTABLE VALUES AND METHODS "

    ''' <summary> Gets or sets the clear value. </summary>
    ''' <value> The clear value. </value>
    Public Property ClearValue As String

    ''' <summary> Gets or sets the initial value. </summary>
    ''' <value> The initial value. </value>
    Public Property InitialValue As String

    ''' <summary> Gets or sets the preset value. </summary>
    ''' <value> The preset value. </value>
    Public Property PresetValue As String

    ''' <summary> Gets or sets the reset value. </summary>
    ''' <value> The reset value. </value>
    Public Property ResetValue As String

    ''' <summary> Sets  the value to its known execution clear state. </summary>
    Public Overrides Sub ClearExecutionState()
        If Not String.IsNullOrEmpty(Me.ClearValue) Then
            Me.Value = Me.ClearValue
        End If
    End Sub

    ''' <summary> Sets the value to a custom setting for the subsystem. </summary>
    Public Overrides Sub InitializeKnownState()
        If Not String.IsNullOrEmpty(Me.InitialValue) Then
            Me.Value = Me.InitialValue
        End If
    End Sub

    ''' <summary> Sets the value to its known execution preset state. </summary>
    Public Overrides Sub PresetKnownState()
        If Not String.IsNullOrEmpty(Me.PresetValue) Then
            Me.Value = Me.PresetValue
        End If
    End Sub

    ''' <summary> Sets the value to its known execution reset state. </summary>
    Public Overrides Sub ResetKnownState()
        Me.Value = Me.ResetValue
    End Sub

#End Region

End Class

''' <summary> Collection of Presettable values. </summary>
''' <license> (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="03/27/2015" by="David" revision="3.0.5563"> Created. </history>
Public Class PresettableValueCollection
    Inherits ObjectModel.Collection(Of PresettableValueBase)
    Implements IPresettable

    Public Sub ClearExecutionState() Implements IPresettable.ClearExecutionState
        For Each Item As PresettableValueBase In Me
            Item.ClearExecutionState()
        Next
    End Sub

    Public Sub InitializeKnownState() Implements IPresettable.InitializeKnownState
        For Each Item As PresettableValueBase In Me
            Item.InitializeKnownState()
        Next
    End Sub

    Public Sub PresetKnownState() Implements IPresettable.PresetKnownState
        For Each Item As PresettableValueBase In Me
            Item.PresetKnownState()
        Next
    End Sub

    Public Sub ResetKnownState() Implements IPresettable.ResetKnownState
        For Each Item As PresettableValueBase In Me
            Item.ResetKnownState()
        Next
    End Sub

    ''' <summary> Publishes this object. </summary>
    Public Sub Publish()
        For Each Item As PresettableValueBase In Me
            Item.Publish()
        Next
    End Sub

End Class
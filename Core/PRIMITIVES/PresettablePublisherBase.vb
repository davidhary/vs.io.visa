''' <summary> Defines the contract that must be implemented by Presettable Publishers. </summary>
''' <license> (c) 2013 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="11/4/2013" by="David" revision="3.0.5046"> Created. </history>
Public MustInherit Class PresettablePublisherBase
    Inherits TracePropertyPublisherBase
    Implements IPresettable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="PresettablePublisherBase">Presettable Publisher</see> class. </summary>
    Protected Sub New()
        MyBase.New()
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Sets subsystem values to their known execution clear state. </summary>
    Public MustOverride Sub ClearExecutionState() Implements IPresettable.ClearExecutionState

    ''' <summary> Performs a reset and additional custom setting for the subsystem. </summary>
    ''' <remarks> Use this method to customize the reset. </remarks>
    Public Overridable Sub InitializeKnownState() Implements IPresettable.InitializeKnownState
        Me.ResetKnownState()
    End Sub

    ''' <summary> Gets the subsystem value to their known execution preset state. </summary>
    Public MustOverride Sub PresetKnownState() Implements IPresettable.PresetKnownState

    ''' <summary> Sets the subsystem values to their known execution reset state. </summary>
    Public MustOverride Sub ResetKnownState() Implements IPresettable.ResetKnownState

#End Region

End Class


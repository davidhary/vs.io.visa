﻿Imports NationalInstruments

''' <summary> Interface for Presettable entities. </summary>
''' <license> (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/21/2005" by="David" revision="1.0.1847.x"> Created. </history>
Public Interface IPresettable

#Region " COMMANDS "

    ''' <summary> Sets subsystem values to their known execution clear state. </summary>
    Sub ClearExecutionState()

    ''' <summary> Performs a reset and additional custom setting for the subsystem. </summary>
    ''' <remarks> Use this method to customize the reset. </remarks>
    Sub InitializeKnownState()

    ''' <summary> Sets the subsystem value to their known execution preset state. </summary>
    Sub PresetKnownState()

    ''' <summary> Sets the subsystem values to their known execution reset state. </summary>
    Sub ResetKnownState()

#End Region

End Interface

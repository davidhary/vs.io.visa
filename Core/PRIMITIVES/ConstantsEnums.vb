﻿Imports NationalInstruments

#Region " SCPI EVENT FLAGS "

''' <summary> Gets or sets the status byte bits of the service request register. </summary>
''' <remarks>Extends the VISA 
'''   <see cref="VisaNS.StatusByteFlags.EventStatusRegister">event status flags</see>.
''' Because the VISA status returns a signed byte, we need to use Int16.
''' Enumerates the Status Byte Register Bits.
''' Use STB? or status.request_event to read this register.
''' Use *SRE or status.request_enable to enable these services.
''' This attribute is used to read the status byte, which is returned as a
''' numeric value. The binary equivalent of the returned value indicates which
''' register bits are set.
''' </remarks>
''' <license> (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
<System.Flags()> Public Enum ServiceRequests
    <ComponentModel.Description("None")> None = 0
    ''' <summary>
    ''' Bit B0, Measurement Summary Bit (MSB). Set summary bit indicates
    ''' that an enabled measurement event has occurred.
    ''' </summary>
    <ComponentModel.Description("Measurement Event (MSB)")> MeasurementEvent = &H1
    ''' <summary>
    ''' Bit B1, System Summary Bit (SSB). Set summary bit indicates
    ''' that an enabled system event has occurred.
    ''' </summary>
    <ComponentModel.Description("System Event (SSB)")> SystemEvent = &H2
    ''' <summary>
    ''' Bit B2, Error Available (EAV). Set summary bit indicates that
    ''' an error or status message is present in the Error Queue.
    ''' </summary>
    <ComponentModel.Description("Error Available (EAV)")> ErrorAvailable = &H4
    ''' <summary>
    ''' Bit B3, Questionable Summary Bit (QSB). Set summary bit indicates
    ''' that an enabled questionable event has occurred.
    ''' </summary>
    <ComponentModel.Description("Questionable Event (QSB)")> QuestionableEvent = &H8
    ''' <summary>
    ''' Bit B4 (16), Message Available (MAV). Set summary bit indicates that
    ''' a response message is present in the Output Queue.
    ''' </summary>
    <ComponentModel.Description("Message Available (MAV)")> MessageAvailable = &H10
    ''' <summary>Bit B5, Event Summary Bit (ESB). Set summary bit indicates 
    ''' that an enabled standard event has occurred.
    ''' </summary>
    <ComponentModel.Description("Standard Event (ESB)")> StandardEvent = &H20 ' (32) ESB
    ''' <summary>
    ''' Bit B6 (64), Request Service (RQS)/Master Summary Status (MSS).
    ''' Set bit indicates that an enabled summary bit of the Status Byte Register
    ''' is set. Depending on how it is used, Bit B6 of the Status Byte Register
    ''' is either the Request for Service (RQS) bit or the Master Summary Status
    ''' (MSS) bit: When using the GPIB serial poll sequence of the unit to obtain
    ''' the status byte (serial poll byte), B6 is the RQS bit. When using
    ''' status.condition or the *STB? common command to read the status byte,
    ''' B6 is the MSS bit.
    ''' </summary>
    <ComponentModel.Description("Request Service (RQS)/Master Summary Status (MSS)")> RequestingService = &H40
    ''' <summary>
    ''' Bit B7 (128), Operation Summary (OSB). Set summary bit indicates that
    ''' an enabled operation event has occurred.
    ''' </summary>
    <ComponentModel.Description("Operation Event (OSB)")> OperationEvent = &H80
    ''' <summary>
    ''' Includes all bits.
    ''' </summary>
    <ComponentModel.Description("All")> All = &HFF ' 255
    ''' <summary>
    ''' Unknown value due to, for example, error trying to get value from the device.
    ''' </summary>
    <ComponentModel.Description("Unknown")> Unknown = &H100
End Enum

''' <summary> Enumerates the status byte flags of the standard event register. </summary>
''' <remarks> Enumerates the Standard Event Status Register Bits.
''' Read this information using ESR? or status.standard.event.
''' Use *ESE or status.standard.enable or event status enable 
''' to enable this register.
''' These values are used when reading or writing to the
''' standard event registers. Reading a status register returns a value.
''' The binary equivalent of the returned value indicates which register bits
''' are set. The least significant bit of the binary number is bit 0, and
''' the most significant bit is bit 15. For example, assume value 9 is
''' returned for the enable register. The binary equivalent is
''' 0000000000001001. This value indicates that bit 0 (OPC) and bit 3 (DDE)
''' are set.
''' </remarks>
<System.Flags()> Public Enum StandardEvents
    <ComponentModel.Description("None")> None = 0
    ''' <summary>
    ''' Bit B0, Operation Complete (OPC). Set bit indicates that all
    ''' pending selected device operations are completed and the unit is ready to
    ''' accept new commands. The bit is set in response to an *OPC command.
    ''' The ICL function OPC() can be used in place of the *OPC command.
    ''' </summary>
    <ComponentModel.Description("Operation Complete (OPC)")> OperationComplete = 1
    ''' <summary>
    ''' Bit B1, Request Control (RQC). Set bit indicates that....
    ''' </summary>
    <ComponentModel.Description("Request Control (RQC)")> RequestControl = &H2
    ''' <summary>
    ''' Bit B2, Query Error (QYE). Set bit indicates that you attempted
    ''' to read data from an empty Output Queue.
    ''' </summary>
    <ComponentModel.Description("Query Error (QYE)")> QueryError = &H4
    ''' <summary>
    ''' Bit B3, Device-Dependent Error (DDE). Set bit indicates that a
    ''' device operation did not execute properly due to some internal
    ''' condition.
    ''' </summary>
    <ComponentModel.Description("Device Dependent Error (DDE)")> DeviceDependentError = &H8
    ''' <summary>
    ''' Bit B4 (16), Execution Error (EXE). Set bit indicates that the unit
    ''' detected an error while trying to execute a command. 
    ''' This is used by QUATECH to report No Contact.
    ''' </summary>
    <ComponentModel.Description("Execution Error (EXE)")> ExecutionError = &H10
    ''' <summary>
    ''' Bit B5 (32), Command Error (CME). Set bit indicates that a
    ''' command error has occurred. Command errors include:<p>
    ''' IEEE-488.2 syntax error — unit received a message that does not follow
    ''' the defined syntax of the IEEE-488.2 standard.  </p><p>
    ''' Semantic error — unit received a command that was misspelled or received
    ''' an optional IEEE-488.2 command that is not implemented.  </p><p>
    ''' The device received a Group Execute Trigger (GET) inside a program
    ''' message.  </p>
    ''' </summary>
    <ComponentModel.Description("Command Error (CME)")> CommandError = &H20
    ''' <summary>
    ''' Bit B6 (64), User Request (URQ). Set bit indicates that the LOCAL
    ''' key on the SourceMeter front panel was pressed.
    ''' </summary>
    <ComponentModel.Description("User Request (URQ)")> UserRequest = &H40
    ''' <summary>
    ''' Bit B7 (128), Power ON (PON). Set bit indicates that the device
    ''' has been turned off and turned back on since the last time this register
    ''' has been read.
    ''' </summary>
    <ComponentModel.Description("Power Toggled (PON)")> PowerToggled = &H80
    ''' <summary>
    ''' Unknown value due to, for example, error trying to get value from the device.
    ''' </summary>
    <ComponentModel.Description("Unknown")> Unknown = &H100
    ''' <summary>Includes all bits.
    ''' </summary>
    <ComponentModel.Description("All")> All = &HFF ' 255
End Enum

#End Region


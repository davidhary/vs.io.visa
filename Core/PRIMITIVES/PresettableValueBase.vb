Imports System.ComponentModel
''' <summary> Defines the contract that must be implemented by Presettable Values. </summary>
''' <license> (c) 2015 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="03/26/2015" by="David" revision="3.0.5563"> Created. </history>
Public MustInherit Class PresettableValueBase
    Implements IPresettable, INotifyPropertyChanged

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="PresettableValueBase">Presettable
    ''' Value</see> class. </summary>
    ''' <param name="propertyName"> The name of the property. </param>
    Protected Sub New(ByVal propertyName As String)
        MyBase.New()
        Me._PropertyName = propertyName
        Me._Command = New Command
    End Sub

#End Region

#Region " COMMAND and VALUE "

    ''' <summary> Gets or sets the command for reading and writing values. </summary>
    ''' <value> The command. </value>
    Public Property Command As Command

    ''' <summary> Gets a value indicating whether this object has value. </summary>
    ''' <value> <c>true</c> if this object has value; otherwise <c>false</c> </value>
    Public MustOverride ReadOnly Property HasValue As Boolean

#End Region

#Region " PUBLISH "

    Private _PropertyName As String
    ''' <summary> Gets the name of the property. </summary>
    ''' <value> The name of the property. </value>
    Protected ReadOnly Property PropertyName As String
        Get
            Return Me._PropertyName
        End Get
    End Property

    ''' <summary> Publishes this object. </summary>
    Public Sub Publish()
        Me.InvokePropertyChanged(New PropertyChangedEventArgs(Me.PropertyName))
    End Sub

    ''' <summary> Event that is raised when a property value changes. </summary>
    Public Event PropertyChanged(ByVal sender As Object, ByVal e As PropertyChangedEventArgs) Implements INotifyPropertyChanged.PropertyChanged

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property Value. Must be called with the
    ''' <see cref="Threading.SynchronizationContext">sync context</see> </summary>
    ''' <param name="e"> The <see cref="PropertyChangedEventArgs" /> instance containing the event
    ''' data. </param>
    Protected Sub InvokePropertyChanged(ByVal e As PropertyChangedEventArgs)
        Dim evt As PropertyChangedEventHandler = Me.PropertyChangedEvent
        evt?.Invoke(Me, e)
        Windows.Forms.Application.DoEvents()
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Sets subsystem values to their known execution clear state. </summary>
    Public MustOverride Sub ClearExecutionState() Implements IPresettable.ClearExecutionState

    ''' <summary> Performs a reset and additional custom setting for the subsystem. </summary>
    ''' <remarks> Use this method to customize the reset. </remarks>
    Public Overridable Sub InitializeKnownState() Implements IPresettable.InitializeKnownState
        Me.ResetKnownState()
    End Sub

    ''' <summary> Gets the subsystem value to their known execution preset state. </summary>
    Public MustOverride Sub PresetKnownState() Implements IPresettable.PresetKnownState

    ''' <summary> Sets the subsystem values to their known execution reset state. </summary>
    Public MustOverride Sub ResetKnownState() Implements IPresettable.ResetKnownState

#End Region

End Class


﻿Imports System.Configuration
Imports System.Drawing
Imports System.Windows.Forms
Imports System.Security.Permissions
Imports System.ComponentModel
Imports isr.Core.Pith
''' <summary> A form that receives trace messages and persists user settings in the Application
''' Settings file. </summary>
''' <remarks> Requires the use of the <see cref="TraceObserverUserFormBaseWrapper">wrapper base form</see> with the designer. 
''' <example> <code>
''' #Const designMode1 = True
''' #Region " BASE FROM WRAPPER "
'''     ' Designing requires changing the condition to True.
''' #If designMode Then
'''     Inherits TraceObserverUserFormBaseWrapper
''' #Else
'''     Inherits TraceObserverUserFormBase
''' #End If
''' #End Region
''' </code> </example> </remarks>
''' <license> (c) 2013 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/31/2013" by="David" revision="6.1.4779.x"> created. </history>
Public MustInherit Class TraceObserverUserFormBase
    Inherits System.Windows.Forms.Form
    Implements ITraceMessageObserver

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Specialized default constructor for use only by derived classes. </summary>

    Protected Sub New()
        MyBase.New()
        Me.InitializeComponent()
        Me.SaveSettingsOnClosing = True
    End Sub

    ''' <summary> Initializes the component. </summary>
    Private Sub InitializeComponent()
        Me.SuspendLayout()
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(331, 341)
        Me.Font = New Font(SystemFonts.MessageBoxFont.FontFamily, 9.75!, FontStyle.Regular, GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "TraceObserverUserFormBase"
        Me.ResumeLayout(False)
    End Sub

#Region " CLASS STYLE "

    ''' <summary> The enable drop shadow version. </summary>
    Public Const EnableDropShadowVersion As Integer = 5

    ''' <summary> Gets or sets the class style. </summary>
    ''' <value> The class style. </value>
    Protected Property ClassStyle As ClassStyleConstants = ClassStyleConstants.None

    ''' <summary> Adds a drop shadow parameter. </summary>
    ''' <remarks> From Code Project: http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx. </remarks>
    ''' <value> Options that control the create. </value>
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        <System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Demand,
                                                        Flags:=System.Security.Permissions.SecurityPermissionFlag.UnmanagedCode)>
        Get
            Dim cp As CreateParams = MyBase.CreateParams
            cp.ClassStyle = cp.ClassStyle Or CInt(Me.ClassStyle)
            Return cp
        End Get
    End Property

#End Region

#End Region

#Region " SETTING EVENTS "

    ''' <summary> Controls if user preferences are saved on closing. </summary>
    ''' <remarks> Set this property to false to disable saving settings. </remarks>
    ''' <value> <c>SaveSettingsOnClosing</c>is a Boolean property. </value>
    Public Property SaveSettingsOnClosing() As Boolean

    ''' <summary> This is called when the form is loaded before it is visible. </summary>
    ''' <remarks> Use this method to set form elements before the form is visible. </remarks>
    <PermissionSetAttribute(SecurityAction.Demand, Unrestricted:=True)>
    Protected Overridable Sub OnLoadSettings()
        If Me.SaveSettingsOnClosing Then
            With isr.Core.Pith.My.MyAppSettings.Get()
                Dim startPos As FormStartPosition = .ReadValue(Me.StartPositionKey, Me.StartPosition)
                If Not Me.StartPosition.Equals(startPos) Then
                    Me.StartPosition = startPos
                End If
            End With
        End If
    End Sub

    ''' <summary> This is called when the form is shown after it is visible. </summary>
    ''' <remarks> Use this method to set form elements after the form is visible. </remarks>
    <PermissionSetAttribute(SecurityAction.Demand, Unrestricted:=True)>
    Protected Overridable Sub OnShowSettings()
        If Me.SaveSettingsOnClosing Then
            With isr.Core.Pith.My.MyAppSettings.Get()
                If Me.StartPosition = FormStartPosition.Manual Then
                    Me.WindowState = .ReadValue(Me.WindowsStateKey, Me.WindowState)
                    If Me.WindowState = FormWindowState.Normal Then
                        Dim size As Drawing.Size = .ReadValue(Me.SizeKey, Me.Size)
                        Dim loc As Drawing.Point = .ReadValue(Me.LocationKey, Me.Location)
                        If Not Me.Location.Equals(loc) AndAlso
                            loc.X < Screen.PrimaryScreen.WorkingArea.Width AndAlso
                            loc.X + size.Width > 0 AndAlso
                            loc.Y < Screen.PrimaryScreen.WorkingArea.Height AndAlso
                            loc.Y + size.Height > 0 Then
                            Me.Location = loc
                        End If
                        If Not Me.Size.Equals(size) Then
                            Me.Size = size
                        End If
                    End If
                End If
            End With
        End If
    End Sub

    ''' <summary> Is called when the form unloads. </summary>
    ''' <remarks> Use Save settings. </remarks>
    <PermissionSetAttribute(SecurityAction.Demand, Unrestricted:=True)>
    Protected Overridable Sub OnSaveSettings()
        If Me.SaveSettingsOnClosing Then
            With isr.Core.Pith.My.MyAppSettings.Get()
                .WriteValue(Me.StartPositionKey, Me.StartPosition)
                If Me.StartPosition = FormStartPosition.Manual Then
                    .WriteValue(Me.WindowsStateKey, Me.WindowState)
                    If Me.WindowState = FormWindowState.Normal Then
                        .WriteValue(Me.LocationKey, Me.Location)
                        .WriteValue(Me.SizeKey, Me.Size)
                    End If
                End If
                .Save()
            End With
        End If
    End Sub

#End Region

#Region " FORM EVENTS "

    ''' <summary> Gets or sets the sentinel indicating that the form loaded without an exception.
    '''           Should be set only if load did not fail. </summary>
    ''' <value> The is loaded. </value>
    Protected Property IsLoaded As Boolean

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Form.Load" /> event after reading the
    ''' start position from the configuration file. </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnLoad(e As System.EventArgs)
        Try
            If Not Me.DesignMode Then
                Me.OnLoadSettings()
            End If
            Me.IsLoaded = True
        Catch
            Throw
        Finally
            MyBase.OnLoad(e)
        End Try
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Form.Shown" /> event after positioning
    ''' the form based on the configuration settings. </summary>
    ''' <param name="e"> A <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnShown(e As System.EventArgs)
        If Not Me.IsLoaded Then Return
        Try
            If Not Me.DesignMode Then
                Me.OnShowSettings()
            End If
        Catch
            Throw
        Finally
            MyBase.OnShown(e)
        End Try
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Form.Closing" /> event after saving the
    ''' form location settings. </summary>
    ''' <param name="e"> A <see cref="T:System.ComponentModel.CancelEventArgs" /> that contains the
    ''' event data. </param>
    Protected Overrides Sub OnClosing(e As System.ComponentModel.CancelEventArgs)
        Try
            If Not Me.DesignMode AndAlso Me.IsLoaded AndAlso e IsNot Nothing AndAlso Not e.Cancel Then
                Me.OnSaveSettings()
            End If
        Catch ex As ConfigurationErrorsException
            ' this error occurs when the system thinks that two managers accessed the configuration file.
            Debug.Assert(Not Debugger.IsAttached, ex.ToString)
        Catch
            Throw
        Finally
            MyBase.OnClosing(e)
        End Try
    End Sub

#End Region

#Region " CONFIGURATION MEMBERS "

    ''' <summary> Gets the location key. </summary>
    ''' <value> The location key. </value>
    Private ReadOnly Property LocationKey As String
        Get
            Return Me.Name & ".Location"
        End Get
    End Property

    ''' <summary> Gets the size key. </summary>
    ''' <value> The size key. </value>
    Private ReadOnly Property SizeKey As String
        Get
            Return Me.Name & ".Size"
        End Get
    End Property

    ''' <summary> Gets the start position key. </summary>
    ''' <value> The windows state key. </value>
    Private ReadOnly Property StartPositionKey As String
        Get
            Return Me.Name & ".StartPosition"
        End Get
    End Property

    ''' <summary> Gets the windows state key. </summary>
    ''' <value> The windows state key. </value>
    Private ReadOnly Property WindowsStateKey As String
        Get
            Return Me.Name & ".FormWindowState"
        End Get
    End Property

#End Region

#Region " I TRACE OBSERVER "

    ''' <summary> Displays a trace message described by value. </summary>
    ''' <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
    Protected Overridable Sub DisplayTraceMessage(ByVal value As TraceMessage)
        If value IsNot Nothing Then
            Dim s As String = value.ExtractSynopsis()
            If Not String.IsNullOrEmpty(s) Then DisplaySynopsis(s)
            Me.DisplayMessage(value)
        End If
    End Sub

    ''' <summary> Displays the <paramref name="value">synopsis</paramref>. </summary>
    ''' <param name="value"> The synopsis to display. </param>
    Protected MustOverride Sub DisplaySynopsis(ByVal value As String)

    ''' <summary> Displays the <paramref name="value">message</paramref>. </summary>
    ''' <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
    Protected MustOverride Sub DisplayMessage(ByVal value As TraceMessage)

    ''' <summary> Observes the Trace event published by <see cref="ITraceMessagePublisher">trace publishers</see>. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="TraceMessageEventArgs" /> instance containing the event data. </param>
    Protected Sub OnTraceMessageAvailable(ByVal sender As Object, ByVal e As TraceMessageEventArgs) Implements ITraceMessageObserver.OnTraceMessageAvailable
        If sender IsNot Nothing AndAlso e IsNot Nothing Then
            Me.DisplayTraceMessage(e.TraceMessage)
        End If
    End Sub

    ''' <summary> Gets or sets the trace show level. The trace message is displayed if the trace level
    ''' is lower than this value. </summary>
    ''' <value> The trace show level. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Protected MustOverride ReadOnly Property TraceShowLevel As Diagnostics.TraceEventType Implements ITraceMessageObserver.TraceShowLevel

    ''' <summary> Determines if the trace event type should be displayed. </summary>
    ''' <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
    ''' <returns> <c>True</c> if the trace event type should be displayed. </returns>
    Protected Function ShouldShow(ByVal eventType As TraceEventType) As Boolean Implements ITraceMessageObserver.ShouldShow
        Return eventType <= Me.TraceShowLevel
    End Function

#End Region

#Region " I TRACE OBSERVER HELPER METHODS "

    ''' <summary> Displays a trace message described by value. </summary>
    ''' <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
    ''' <returns> A Trace Message. </returns>
    Private Function _DisplayTraceMessage(ByVal value As TraceMessage) As TraceMessage
        Me.DisplayTraceMessage(value)
        Return value
    End Function

    ''' <summary> Displays and logs the trace message. </summary>
    ''' <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
    ''' <param name="id">         The identifier to use with the trace event. </param>
    ''' <param name="format">     The format. </param>
    ''' <param name="args">       The arguments. </param>
    ''' <returns> The built <see cref="TraceMessage">trace message</see>. </returns>
    Protected Overridable Function OnTraceMessageAvailable(ByVal eventType As TraceEventType, ByVal id As Integer, ByVal format As String, ByVal ParamArray args() As Object) As TraceMessage
        Return Me._DisplayTraceMessage(New TraceMessage(eventType, id, format, args))
    End Function

    ''' <summary> Displays and logs the trace message. </summary>
    ''' <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
    ''' <param name="id">        The identifier. </param>
    ''' <param name="details">   The details. </param>
    ''' <returns> The built <see cref="TraceMessage">trace message</see>. </returns>
    Protected Function OnTraceMessageAvailable(ByVal eventType As TraceEventType, ByVal id As Integer, ByVal details As String) As TraceMessage
        Return Me._DisplayTraceMessage(New TraceMessage(eventType, id, details))
    End Function

#End Region

End Class

''' <summary> Trace observer and user setting base form and wrapper. </summary>
''' <remarks> This class is required to permit the use of the
''' <see cref="TraceObserverUserFormBase">base form</see> with the designer. </remarks>
''' <license> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="1/28/2014" by="David"> Created. </history>
Public Class TraceObserverUserFormBaseWrapper
    Inherits TraceObserverUserFormBase

    ''' <summary> Displays the <paramref name="value">message</paramref>. </summary>
    ''' <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
    Protected Overrides Sub DisplayMessage(value As TraceMessage)
        Debug.Assert(False, "Illegal call; reset design mode")
    End Sub

    ''' <summary> Displays the <paramref name="value">synopsis</paramref>. </summary>
    ''' <param name="value"> The synopsis to display. </param>
    Protected Overrides Sub DisplaySynopsis(value As String)
    End Sub

    ''' <summary> Gets the trace display level. </summary>
    ''' <value> The trace display level. </value>
    Public Property TraceDisplayLevel As TraceEventType = TraceEventType.Verbose

    ''' <summary> Gets or sets the trace show level. The trace message is displayed if the trace level is lower
    ''' than this value. </summary>
    ''' <value> The trace show level. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Protected Overrides ReadOnly Property TraceShowLevel As Diagnostics.TraceEventType
        Get
            Return Me.TraceDisplayLevel
        End Get
    End Property

End Class

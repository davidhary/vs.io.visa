Imports System.Runtime.CompilerServices
Imports isr.Core.Pith
Imports isr.Core.Pith.EventHandlerExtensions
''' <summary> Defines an event arguments class for <see cref="TraceMessage">trace message</see>. </summary>
''' <license> (c) 2013 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="09/04/2013" by="David" revision="1.2.4955"> created based on the legacy
''' message event arguments. </history>
Public Class TraceMessageEventArgs
    Inherits System.EventArgs

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        Me.New(TraceEventType.Information, "", "")
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="details"> The details. </param>
    Public Sub New(ByVal details As TraceMessage)
        MyBase.new()
        Me._TraceMessage = details
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="format"> Specifies the message formatting string. </param>
    ''' <param name="args">   Specifies the arguments. </param>
    Public Sub New(ByVal format As String, ByVal ParamArray args() As Object)
        Me.New(TraceEventType.Information, 0, format, args)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
    ''' <param name="format"> The message formatting string. </param>
    ''' <param name="args">   The message arguments. </param>
    Public Sub New(ByVal eventType As TraceEventType, ByVal format As String, ByVal ParamArray args() As Object)
        Me.New(eventType, 0, format, args)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
    ''' <param name="id">         The identifier to use with the trace event. </param>
    ''' <param name="format"> The message formatting string. </param>
    ''' <param name="args">   The message arguments. </param>
    Public Sub New(ByVal eventType As TraceEventType, ByVal id As Integer, ByVal format As String, ByVal ParamArray args() As Object)
        Me.New(New TraceMessage(eventType, id, format, args))
    End Sub

#End Region

#Region " PROPERTIES "

    Private _TraceMessage As TraceMessage

    ''' <summary> Gets the Trace Message. </summary>
    ''' <value> A message describing the trace. </value>
    Public ReadOnly Property TraceMessage() As TraceMessage
        Get
            Return Me._TraceMessage
        End Get
    End Property

    ''' <summary> Gets an empty <see cref="TraceMessageEventArgs">event arguments</see>. </summary>
    ''' <value> The empty. </value>
    Public Shared Shadows ReadOnly Property Empty() As TraceMessageEventArgs
        Get
            Return New TraceMessageEventArgs
        End Get
    End Property

#End Region

End Class


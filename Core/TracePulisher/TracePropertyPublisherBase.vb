﻿Imports System.Threading
Imports System.ComponentModel
Imports System.Windows.Forms
Imports isr.Core.Pith
Imports isr.Core.Pith.EventHandlerExtensions
Imports System.Runtime.CompilerServices
Imports isr.IO.Visa.EventHandlerExtensions
''' <summary> Defines the contract that must be implemented by trace and property publishers. </summary>
''' <license> (c) 2012 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/30/14" by="David" revision="1.2.5143"> Based on legacy property publisher.
''' Added trace publisher. </history>
Public MustInherit Class TracePropertyPublisherBase
    Inherits isr.Core.Pith.PropertyPublisherBase
    Implements ITraceMessagePublisher

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="TracePropertyPublisherBase" /> class. </summary>
    Protected Sub New()
        MyBase.New()
    End Sub

#Region " Disposable Support "

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    ''' release only unmanaged resources. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)

        Try
            If Not Me.IsDisposed Then
                If disposing Then
                    Me.RemoveEventHandler(Me.TraceMessageAvailableEvent)
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#End Region

#Region " TRACE MESSAGE AVAILABLE EVENT IMPLEMENTATION "

    ''' <summary> Event queue for all listeners interested in TraceMessageAvailable events. </summary>
    Public Event TraceMessageAvailable As EventHandler(Of TraceMessageEventArgs) Implements ITraceMessagePublisher.TraceMessageAvailable

    ''' <summary> Removes event handler. </summary>
    ''' <param name="value"> The handler. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub RemoveEventHandler(ByVal value As EventHandler(Of TraceMessageEventArgs))
        For Each d As [Delegate] In value.SafeInvocationList
            Try
                RemoveHandler Me.TraceMessageAvailable, CType(d, EventHandler(Of TraceMessageEventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToString)
            End Try
        Next
    End Sub

#Region " I TRACE MESSAGE PUBLISHER "

    ''' <summary> Asynchronously posts a trace message available event. </summary>
    ''' <remarks> Override this method in cases the trace message needs to be used (e.g., displayed or logged) before it is raised. </remarks>
    ''' <param name="value"> The Trace Message to process. </param>
    Protected Overridable Sub OnTraceMessageAvailable(ByVal value As TraceMessage) Implements ITraceMessagePublisher.OnTraceMessageAvailable
        If value IsNot Nothing Then
            TraceMessageAvailableEvent.SafePost(Me, New TraceMessageEventArgs(value))
        End If
    End Sub

    ''' <summary> Asynchronously posts a trace message available event. </summary>
    ''' <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
    ''' <param name="id">        The identifier to use with the trace event. </param>
    ''' <param name="format">    Describes the format to use. </param>
    ''' <param name="args">      A variable-length parameters list containing arguments. </param>
    ''' <returns> The event arguments. </returns>
    Protected Function OnTraceMessageAvailable(ByVal eventType As TraceEventType, ByVal id As Integer,
                                               ByVal format As String, ByVal ParamArray args() As Object) As TraceMessage Implements ITraceMessagePublisher.OnTraceMessageAvailable
        Dim e As New TraceMessage(eventType, id, format, args)
        Me.OnTraceMessageAvailable(e)
        Return e
    End Function

    ''' <summary> Asynchronously posts a trace message available event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Sub OnTraceMessageAvailable(ByVal e As TraceMessageEventArgs) Implements ITraceMessagePublisher.OnTraceMessageAvailable
        If e IsNot Nothing Then Me.OnTraceMessageAvailable(e.TraceMessage)
    End Sub

#End Region

#End Region

End Class

Namespace EventHandlerExtensions


    Partial Public Module Methods

#Region " SYNC CONTEXT "

        ''' <summary> Returns the current synchronization context. </summary>
        ''' <exception cref="InvalidOperationException"> Thrown if the current synchronization thread is null. </exception>
        ''' <returns> A Threading.SynchronizationContext. </returns>
        Private Function CurrentSyncContext() As Threading.SynchronizationContext
            If Threading.SynchronizationContext.Current Is Nothing Then
                Threading.SynchronizationContext.SetSynchronizationContext(New Threading.SynchronizationContext)
            End If
            If Threading.SynchronizationContext.Current Is Nothing Then
                Throw New InvalidOperationException("Current Synchronization Context not set;. Must be set before starting the thread.")
            End If
            Return Threading.SynchronizationContext.Current
        End Function

#End Region

        ''' <summary>
        ''' Executes the given operation on a different thread, asynchronously. Safe for cross threading.
        ''' </summary>
        ''' <param name="handler"> The handler. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       Property Changed event information. </param>
        <Extension>
        Public Sub SafePost(ByVal handler As EventHandler(Of TraceMessageEventArgs), ByVal sender As Object, ByVal e As TraceMessageEventArgs)
            Dim evt As EventHandler(Of TraceMessageEventArgs) = handler
            If evt IsNot Nothing Then
                For Each d As [Delegate] In evt.GetInvocationList
                    Methods.CurrentSyncContext.Post(Sub(ee) d.DynamicInvoke(New Object() {sender, e}), e)
                Next
            End If
#If False Then
            ' much slower than the loop throw the invocation list.
            If evt IsNot Nothing Then Methods.CurrentSyncContext.Post(Sub() evt(sender, e), Nothing)
#End If
        End Sub


    End Module
End Namespace


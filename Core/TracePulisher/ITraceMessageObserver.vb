﻿''' <summary> The contract implemented by the class displaying trace messages. </summary>
''' <license> (c) 2013 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="09/04/2013" by="David" revision="1.2.4955"> created based on the legacy
''' extended message. </history>
Public Interface ITraceMessageObserver

    ''' <summary> Observes the Trace event published by <see cref="ITraceMessagePublisher">trace publishers</see>. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="TraceMessageEventArgs" /> instance containing the event data. </param>
    Sub OnTraceMessageAvailable(sender As Object, e As TraceMessageEventArgs)

    ''' <summary> Gets the trace show level. The trace message is displayed if the trace level
    ''' is lower than this value. </summary>
    ''' <value> The trace show level. </value>
    ReadOnly Property TraceShowLevel As TraceEventType

    ''' <summary> Determines if the trace event type should be displayed. </summary>
    ''' <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
    ''' <returns> <c>True</c> if the trace event type should be displayed. </returns>
    Function ShouldShow(ByVal eventType As TraceEventType) As Boolean

End Interface


﻿Imports isr.Core.Pith.EnumExtensions
Imports isr.Core.Pith.EventHandlerExtensions
Partial Public Class Session

#Region " DEVICE ERRORS "

    ''' <summary> Clears the error cache. </summary>
    Public Sub ClearErrorCache()
        Me.DeviceErrorBuilder = New System.Text.StringBuilder
        Me._DeviceErrorQueue = New Queue(Of DeviceError)
    End Sub

    Private _DeviceErrorQueue As Queue(Of DeviceError)
    ''' <summary> Gets or sets the error queue. </summary>
    ''' <value> A Queue of device errors. </value>
    Public ReadOnly Property DeviceErrorQueue As Queue(Of DeviceError)
        Get
            Return Me._DeviceErrorQueue
        End Get
    End Property

    ''' <summary> The device errors. </summary>
    Public Property DeviceErrorBuilder As System.Text.StringBuilder

    ''' <summary> Gets or sets a report of the error stored in the cached error queue. </summary>
    ''' <value> The cached device errors. </value>
    Public Property DeviceErrors() As String
        Get
            Dim builder As New System.Text.StringBuilder(Me.DeviceErrorBuilder.ToString)
            If Me.StandardEventStatus.GetValueOrDefault(0) <> 0 Then
                Dim report As String = Session.BuildReport(Me.StandardEventStatus.Value, "; ")
                If Not String.IsNullOrWhiteSpace(report) Then
                    If builder.Length > 0 Then
                        builder.AppendLine()
                    End If
                    builder.Append(report)
                End If
            End If
            Return builder.ToString
        End Get
        Set(ByVal value As String)
            Me.DeviceErrorBuilder.AppendLine(value)
            Me.SafePostPropertyChanged("DeviceErrors")
            Me.ErrorAvailable = Not String.IsNullOrWhiteSpace(value)
        End Set
    End Property

    ''' <summary> Gets or sets the error queue query command. </summary>
    ''' <value> The error queue query command. </value>
    ''' <remarks> <see cref="Scpi.Syntax.ErrorQueueQueryCommand">error queue command</see></remarks>
    Public Property ErrorQueueQueryCommand As String

    ''' <summary> Returns the queued error. </summary>
    ''' <remarks> Sends the ':SYST:QUE?' query . </remarks>
    Public Function QueryQueuedError() As DeviceError
        Dim err As New DeviceError()
        Me.ReadServiceRequestStatus()
        If Me.ErrorAvailable() AndAlso Not String.IsNullOrWhiteSpace(Me.ErrorQueueQueryCommand) Then
            err = New DeviceError(Me.QueryTrimEnd(Me.ErrorQueueQueryCommand))
        End If
        Me.ErrorAvailable = err.IsError
        Return err
    End Function

    ''' <summary> Reads the device errors. </summary>
    ''' <returns> The device errors. </returns>
    Public Function QueryDeviceErrors() As String
        Me.ClearErrorCache()
        Dim deviceError As DeviceError
        Do
            deviceError = Me.QueryQueuedError()
            If deviceError.IsError Then
                Me.DeviceErrorQueue.Enqueue(deviceError)
            End If
        Loop While Me.ErrorAvailable()
        Dim message As New System.Text.StringBuilder
        If Me.DeviceErrorQueue IsNot Nothing AndAlso Me.DeviceErrorQueue.Count > 0 Then
            message.AppendFormat("{0} Device Errors:", Me.ResourceName)
            For Each e As DeviceError In Me.DeviceErrorQueue
                message.AppendLine()
                message.Append(e.ErrorMessage)
            Next
            Me.QueryStandardEventStatus()
        End If
        Me.DeviceErrors = message.ToString
        Return Me.DeviceErrors
    End Function

#End Region

#Region " STATUS REGISTER EVENTS: ERROR "

    Private _ErrorAvailableBits As ServiceRequests
    ''' <summary> Gets or sets bits that would be set for detecting if an error is available. </summary>
    ''' <value> The error available bits. </value>
    Public Property ErrorAvailableBits() As ServiceRequests
        Get
            Return Me._ErrorAvailableBits
        End Get
        Set(ByVal value As ServiceRequests)
            If Not value.Equals(Me.ErrorAvailableBits) Then
                Me._ErrorAvailableBits = value
                Me.SafePostPropertyChanged("ErrorAvailableBits")
            End If
        End Set
    End Property

    Private _ErrorAvailable As Boolean
    ''' <summary> Gets or sets a value indicating whether [Error available]. </summary>
    ''' <value> <c>True</c> if [Error available]; otherwise, <c>False</c>. </value>
    Public Property ErrorAvailable As Boolean
        Get
            Return Me._ErrorAvailable
        End Get
        Protected Set(ByVal value As Boolean)
            If value OrElse Not value.Equals(Me.ErrorAvailable) Then
                Me._ErrorAvailable = value
                Me.SafeSendPropertyChanged(NameOf(ErrorAvailable))
            End If
        End Set
    End Property

#End Region

#Region " STATUS REGISTER EVENTS: MESSAGE AVAILABLE "

    ''' <summary> Checks if message is available. </summary>
    ''' <returns> <c>True</c> if the message available bit is on; otherwise, <c>False</c>. </returns>
    Public Function IsMessageAvailable() As Boolean
        Return Me.IsMessageAvailable(ServiceRequests.MessageAvailable)
    End Function

    ''' <summary> Checks if message is available. </summary>
    ''' <param name="pollInterval"> The poll interval. </param>
    ''' <param name="timeout">      Specifies the time to wait for message available. </param>
    ''' <returns> <c>True</c> if the message available bit is on; otherwise, <c>False</c>. </returns>
    Public Function IsMessageAvailable(ByVal pollInterval As TimeSpan, ByVal timeout As TimeSpan) As Boolean
        Return Me.IsMessageAvailable(ServiceRequests.MessageAvailable, pollInterval, timeout)
    End Function

    ''' <summary> Checks if message is available. </summary>
    ''' <param name="messageAvailableBits"> The message available bits. </param>
    ''' <param name="pollInterval">         The poll interval. </param>
    ''' <param name="timeout">              Specifies the time to wait for message available. </param>
    ''' <returns> <c>True</c> if the message available bit is on; otherwise, <c>False</c>. </returns>
    Public Function IsMessageAvailable(ByVal messageAvailableBits As Integer, ByVal pollInterval As TimeSpan, ByVal timeout As TimeSpan) As Boolean
        Dim endTime As DateTime = DateTime.Now.Add(timeout)
        Dim messageAvailable As Boolean = Me.IsMessageAvailable(messageAvailableBits)
        Do Until endTime < DateTime.Now OrElse messageAvailable
            Threading.Thread.Sleep(pollInterval)
            Windows.Forms.Application.DoEvents()
            messageAvailable = Me.IsMessageAvailable(messageAvailableBits)
        Loop
        Return messageAvailable
    End Function

#End Region

#Region " STATUS REGISTER EVENTS: MESSAGE "

    Private _MessageAvailableBits As ServiceRequests
    ''' <summary> Gets or sets bits that would be set for detecting if an Message is available. </summary>
    ''' <value> The Message available bits. </value>
    Public Property MessageAvailableBits() As ServiceRequests
        Get
            Return Me._MessageAvailableBits
        End Get
        Set(ByVal value As ServiceRequests)
            If Not value.Equals(Me.MessageAvailableBits) Then
                Me._MessageAvailableBits = value
                Me.SafePostPropertyChanged("MessageAvailableBits")
            End If
        End Set
    End Property

    Private _MessageAvailable As Boolean
    ''' <summary> Gets or sets a value indicating whether [Message available]. </summary>
    ''' <value> <c>True</c> if [Message available]; otherwise, <c>False</c>. </value>
    Public Property MessageAvailable As Boolean
        Get
            Return Me._MessageAvailable
        End Get
        Protected Set(ByVal value As Boolean)
            If value OrElse Not value.Equals(Me.MessageAvailable) Then
                Me._MessageAvailable = value
                Me.SafeSendPropertyChanged("MessageAvailable")
            End If
        End Set
    End Property

    ''' <summary> Checks if message is available. </summary>
    ''' <param name="messageAvailableBits"> The message available bits. </param>
    ''' <returns> <c>True</c> if the message available bit is on; otherwise, <c>False</c>. </returns>
    Public Function IsMessageAvailable(ByVal messageAvailableBits As Integer) As Boolean
        Me.ReadServiceRequestStatus()
        If Not Me.MessageAvailableBits.Equals(messageAvailableBits) Then
            Me.MessageAvailable = (Me.ServiceRequestStatus And messageAvailableBits) <> 0
        End If
        Return Me.MessageAvailable
    End Function

    ''' <summary> Checks if message is available. </summary>
    ''' <remarks> Delays looking for the message status by the status latency to make sure the
    ''' instrument had enough time to process the previous command. </remarks>
    ''' <param name="latency">     The latency. </param>
    ''' <param name="repeatCount"> Number of repeats. </param>
    ''' <returns> <c>True</c> if message is available. </returns>
    Public Function IsMessageAvailable(ByVal latency As TimeSpan, ByVal repeatCount As Integer) As Boolean
        Do
            If latency > TimeSpan.Zero Then
                Threading.Thread.Sleep(latency)
            End If
            Me.ReadServiceRequestStatus()
            repeatCount -= 1
        Loop Until repeatCount <= 0 OrElse Me.MessageAvailable
        Return Me.MessageAvailable
    End Function

#End Region

#Region " STATUS REGISTER EVENTS: MEASUREMENT "

    Private _MeasurementAvailableBits As ServiceRequests
    ''' <summary> Gets or sets bits that would be set for detecting if an Measurement is available. </summary>
    ''' <value> The Measurement available bits. </value>
    Public Property MeasurementAvailableBits() As ServiceRequests
        Get
            Return Me._MeasurementAvailableBits
        End Get
        Set(ByVal value As ServiceRequests)
            If Not value.Equals(Me.MeasurementAvailableBits) Then
                Me._MeasurementAvailableBits = value
                Me.SafePostPropertyChanged("MeasurementAvailableBits")
            End If
        End Set
    End Property

    Private _MeasurementAvailable As Boolean
    ''' <summary> Gets or sets a value indicating whether [Measurement available]. </summary>
    ''' <value> <c>True</c> if [Measurement available]; otherwise, <c>False</c>. </value>
    Public Property MeasurementAvailable As Boolean
        Get
            Return Me._MeasurementAvailable
        End Get
        Protected Set(ByVal value As Boolean)
            If value OrElse Not value.Equals(Me.MeasurementAvailable) Then
                Me._MeasurementAvailable = value
                Me.SafeSendPropertyChanged("MeasurementAvailable")
            End If
        End Set
    End Property

#End Region

#Region " STATUS REGISTER EVENTS: STANDARD EVENT "

    Private _StandardEventAvailableBits As ServiceRequests
    ''' <summary> Gets or sets bits that would be set for detecting if an Standard Event is available. </summary>
    ''' <value> The Standard Event available bits. </value>
    Public Property StandardEventAvailableBits() As ServiceRequests
        Get
            Return Me._StandardEventAvailableBits
        End Get
        Set(ByVal value As ServiceRequests)
            If Not value.Equals(Me.StandardEventAvailableBits) Then
                Me._StandardEventAvailableBits = value
                Me.SafePostPropertyChanged("StandardEventAvailableBits")
            End If
        End Set
    End Property

    Private _StandardEventAvailable As Boolean
    ''' <summary> Gets or sets a value indicating whether [StandardEvent available]. </summary>
    ''' <value> <c>True</c> if [StandardEvent available]; otherwise, <c>False</c>. </value>
    Public Property StandardEventAvailable As Boolean
        Get
            Return Me._StandardEventAvailable
        End Get
        Protected Set(ByVal value As Boolean)
            If value OrElse Not value.Equals(Me.StandardEventAvailable) Then
                Me._StandardEventAvailable = value
                Me.SafeSendPropertyChanged("StandardEventAvailable")
            End If
        End Set
    End Property

#End Region

#Region " SERVICE REQUEST "

    ''' <summary> Gets or sets the standard service enable command format. </summary>
    ''' <value> The standard service enable command format. </value>
    ''' <remarks> <see cref="Ieee488.Syntax.StandardServiceEnableCommandFormat"></see></remarks>
    Public Property StandardServiceEnableCommandFormat As String

    ''' <summary> Program the device to issue an SRQ upon any of the SCPI events. Uses *ESE to select
    ''' (mask) the events that will issue SRQ and  *SRE to select (mask) the event registers to be
    ''' included in the bits that will issue an SRQ. </summary>
    ''' <param name="standardEventEnableBitmask">  Specifies standard events will issue an SRQ. </param>
    ''' <param name="serviceRequestEnableBitmask"> Specifies which status registers will issue an
    ''' SRQ. </param>
    Public Sub EnableServiceRequest(ByVal standardEventEnableBitmask As StandardEvents,
                                    ByVal serviceRequestEnableBitmask As ServiceRequests)
        Me.ReadServiceRequestStatus()
        If Not String.IsNullOrWhiteSpace(Me.StandardServiceEnableCommandFormat) Then
            Me.WriteLine(Me.StandardServiceEnableCommandFormat, CInt(standardEventEnableBitmask), CInt(serviceRequestEnableBitmask))
        End If
    End Sub

    ''' <summary> Enabled detection of completion. </summary>
    ''' <remarks> 3475. Add Or Visa.Ieee4882.ServiceRequests.OperationEvent. </remarks>
    Public Sub EnableWaitComplete()
        Me.EnableServiceRequest(StandardEvents.All And Not StandardEvents.RequestControl, ServiceRequests.StandardEvent)
    End Sub

    ''' <summary> The service request status. </summary>
    Private _ServiceRequestStatus As ServiceRequests

    ''' <summary> Gets or sets the cached service request Status. </summary>
    ''' <remarks> The service request status is posted to be parsed by the status subsystem that is
    ''' specific to the instrument at hand. This is critical to the proper workings of the status
    ''' subsystem. The service request status is posted asynchronously. This may not be processed
    ''' fast enough to determine the next action. Not sure how to address this at this time. </remarks>
    ''' <value> <c>null</c> if value is not known; otherwise <see cref="ServiceRequests">Service
    ''' Requests</see>. </value>
    Public Property ServiceRequestStatus() As ServiceRequests
        Get
            Return Me._ServiceRequestStatus
        End Get
        Set(ByVal value As ServiceRequests)
            Me.ErrorAvailable = (value And Me.ErrorAvailableBits) <> 0
            Me.MessageAvailable = (value And Me.MessageAvailableBits) <> 0
            Me.MeasurementAvailable = (value And Me.MeasurementAvailableBits) <> 0
            Me.StandardEventAvailable = (value And Me.StandardEventAvailableBits) <> 0
            If value <> ServiceRequests.None OrElse Not value.Equals(Me.ServiceRequestStatus) Then
                Me._ServiceRequestStatus = value
                Me.SafePostPropertyChanged("ServiceRequestStatus")
            End If
        End Set
    End Property

    ''' <summary> Reads the service request Status. This method casts the
    ''' <see cref="ReadStatusByte">Read Status Byte</see> to
    ''' <see cref="ServiceRequests">Service Requests</see>. </summary>
    ''' <returns> <c>null</c> if value is not known; otherwise <see cref="ServiceRequests">Service
    ''' Requests</see>. </returns>
    Public Function ReadServiceRequestStatus() As ServiceRequests
        Me.ServiceRequestStatus = CType(Me.ReadStatusByte, ServiceRequests)
        Return Me.ServiceRequestStatus
    End Function

#End Region

#Region " STANDARD EVENT REGISTER "

    ''' <summary> Returns a detailed report of the event status register (ESR) byte. </summary>
    ''' <param name="value">     Specifies the value that was read from the status register. </param>
    ''' <param name="delimiter"> The delimiter. </param>
    ''' <returns> Returns a detailed report of the event status register (ESR) byte. </returns>
    Public Shared Function BuildReport(ByVal value As StandardEvents, ByVal delimiter As String) As String

        If String.IsNullOrWhiteSpace(delimiter) Then
            delimiter = "; "
        End If

        Dim builder As New System.Text.StringBuilder

        For Each eventValue As StandardEvents In [Enum].GetValues(GetType(StandardEvents))
            If eventValue <> StandardEvents.None AndAlso eventValue <> StandardEvents.All AndAlso (eventValue And value) <> 0 Then
                If builder.Length > 0 Then
                    builder.Append(delimiter)
                End If
                builder.Append(eventValue.Description)
            End If
        Next

        If builder.Length > 0 Then
            builder.Append(".")
            builder.Insert(0, String.Format(Globalization.CultureInfo.CurrentCulture,
                                            "The device standard status register reported: 0x{0:X}{1}", value, Environment.NewLine))
        End If
        Return builder.ToString

    End Function

    ''' <summary> The standard event status. </summary>
    Private _StandardEventStatus As StandardEvents?

    ''' <summary> Gets or sets the cached Standard Event enable bit mask. </summary>
    ''' <value> <c>null</c> if value is not known; otherwise <see cref="StandardEvents">Standard
    ''' Events</see>. </value>
    Public Property StandardEventStatus() As StandardEvents?
        Get
            Return Me._StandardEventStatus
        End Get
        Set(ByVal value As StandardEvents?)
            Me._StandardEventStatus = value
            Me.SafePostPropertyChanged("StandardEventStatus")
        End Set
    End Property

    ''' <summary> Gets or sets the standard event query command. </summary>
    ''' <value> The standard event query command. </value>
    ''' <remarks> <see cref="Ieee488.Syntax.StandardEventQueryCommand"></see></remarks>
    Public Property StandardEventQueryCommand As String

    ''' <summary> Queries the Standard Event enable bit mask. </summary>
    ''' <returns> <c>null</c> if value is not known; otherwise <see cref="StandardEvents">Standard
    ''' Events</see>. </returns>
    Public Function QueryStandardEventStatus() As StandardEvents?
        If Not String.IsNullOrWhiteSpace(Me.StandardEventQueryCommand) Then
            Me.StandardEventStatus = CType(Me.Query(0I, Me.StandardEventQueryCommand), StandardEvents?)
        End If
        Return Me.StandardEventStatus
    End Function

#End Region

End Class

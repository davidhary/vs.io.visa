Namespace My

    ''' <summary> Defines a sealed class to provide project management for this project. </summary>
    ''' <license> (c) 2006 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="06/13/2006" by="David" revision="1.0.2355.x"> Created. </history>
    Public NotInheritable Class MyLibrary

        ''' <summary> Identifier for the trace event. </summary>
        Public Const TraceEventId As Integer = ProjectTraceEventId.CoreLibrary

        Public Const AssemblyTitle As String = "VISA I/O Library"
        Public Const AssemblyDescription As String = "VISA I/O Library"
        Public Const AssemblyProduct As String = "IO.Visa.Library.2018"

    End Class

End Namespace


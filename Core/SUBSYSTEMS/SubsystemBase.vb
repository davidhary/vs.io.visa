Imports NationalInstruments
Imports isr.Core.Pith
Imports isr.Core.Pith.EnumExtensions
''' <summary> Defines the contract that must be implemented by Subsystems. </summary>
''' <license> (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/15/2005" by="David" revision="1.0.1841.x"> Created. </history>
Public MustInherit Class SubsystemBase
    Inherits PresettablePublisherBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="SubsystemBase" /> class. </summary>
    Protected Sub New()
        MyBase.New()
        Me._presettableValues = New PresettableValueCollection
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="SubsystemBase" /> class. </summary>
    ''' <param name="visaSession"> A reference to a <see cref="Visa.Session">message based
    ''' session</see>. </param>
    Protected Sub New(ByVal visaSession As Visa.Session)
        Me.New()
        Me._Session = visaSession
    End Sub

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <param name="disposing"> <c>True</c> if this method releases both managed and unmanaged resources;
    ''' False if this method releases only unmanaged resources. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called
                    Me._Session = Nothing

                End If

                ' Free shared unmanaged resources

                ' free memory
                Me._presettableValues.Clear()
                Me._presettableValues = Nothing

            End If

        Finally

            ' dispose the base class.
            MyBase.Dispose(disposing)

        End Try

    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Sets subsystem values to their known execution clear state. </summary>
    Public Overrides Sub ClearExecutionState()
        Me._presettableValues.ClearExecutionState()
    End Sub

    ''' <summary> Performs a reset and additional custom setting for the subsystem. </summary>
    Public Overrides Sub InitializeKnownState()
        Me.ResetKnownState()
        Me._presettableValues.InitializeKnownState()
    End Sub

    ''' <summary> Gets the preset command. </summary>
    ''' <value> The preset command. </value>
    Protected MustOverride ReadOnly Property PresetCommand As String

    ''' <summary> Gets the subsystem value to their known execution preset state. </summary>
    Public Overrides Sub PresetKnownState()
        If Me.IsSessionOpen AndAlso Not String.IsNullOrWhiteSpace(Me.PresetCommand) Then
            Me.Session.WriteLine(Me.PresetCommand)
        End If
        Me._presettableValues.PresetKnownState()
    End Sub

    ''' <summary> Gets the subsystem value to their known execution Reset state. </summary>
    Public Overrides Sub ResetKnownState()
        Me._presettableValues.ResetKnownState()
    End Sub

    Private _PresettableValues As PresettableValueCollection

    ''' <summary> Adds a value. </summary>
    ''' <param name="value"> The value. </param>
    Protected Sub AddValue(ByVal value As PresettableValueBase)
        Me._presettableValues.Add(value)
    End Sub

    ''' <summary> Removes the value described by value. </summary>
    ''' <param name="value"> The value. </param>
    Protected Sub RemoveValue(ByVal value As PresettableValueBase)
        If Me._presettableValues.Contains(value) Then Me._presettableValues.Remove(value)
    End Sub

    ''' <summary> Publishes all elements. </summary>
    Public Overrides Sub Publish()
        If Me.Publishable Then Me._presettableValues.Publish()
    End Sub

#End Region

#Region " SESSION "

    ''' <summary> Gets a value indicating whether the subsystem has an open session open. </summary>
    ''' <value> <c>True</c> if the device has an open session; otherwise, <c>False</c>. </value>
    Public ReadOnly Property IsSessionOpen As Boolean
        Get
            Return Me.Session IsNot Nothing AndAlso Not Me.Session.Disposed
        End Get
    End Property

    ''' <summary> The session. </summary>
    Private _Session As Session

    ''' <summary> Gets the session. </summary>
    ''' <value> The session. </value>
    Public Property Session As Session
        Get
            Return Me._Session
        End Get
        Set(ByVal value As Session)
            Me._Session = value
            If Me.IsSessionOpen Then
                Me.ResourceName = Me.Session.ResourceName
            Else
                Me.ResourceName = "<closed>"
            End If
        End Set
    End Property

    Private _VisaErrorAvailable As Boolean

    ''' <summary> Gets or sets (protected) the visa error available sentinel. </summary>
    ''' <value> <c>True</c> if the <see cref="LastStatus">visa status</see> indicates a Visa Error;
    ''' Otherwise, <c>False</c>. </value>
    Public Property VisaErrorAvailable As Boolean
        Get
            Return Me._VisaErrorAvailable
        End Get
        Protected Set(ByVal value As Boolean)
            If value <> Me.VisaErrorAvailable Then
                Me._VisaErrorAvailable = value
                Me.SafeSendPropertyChanged("VisaErrorAvailable")
            End If
        End Set
    End Property

    Private _VisaWarningAvailable As Boolean
    ''' <summary> Gets or sets (protected) the visa Warning available sentinel. </summary>
    ''' <value> <c>True</c> if the <see cref="LastStatus">visa status</see> indicates a Visa Warning;
    ''' Otherwise, <c>False</c>. </value>
    Public Property VisaWarningAvailable As Boolean
        Get
            Return Me._VisaWarningAvailable
        End Get
        Protected Set(ByVal value As Boolean)
            If value <> Me.VisaWarningAvailable Then
                Me._VisaWarningAvailable = value
                Me.SafeSendPropertyChanged("VisaWarningAvailable")
            End If
        End Set
    End Property

    Private _LastStatus As VisaNS.VisaStatusCode
    ''' <summary> Gets the last status reported by the VISA session. </summary>
    ''' <value> <see cref="VisaNS.VisaStatusCode">Visa Status Code</see> if session is open; 
    '''         otherwise <see cref="VisaNS.VisaStatusCode.SuccessDeviceNotPresent">device not present</see>. </value>
    Public Property LastStatus As VisaNS.VisaStatusCode
        Get
            If Me.IsSessionOpen Then
                Me.LastStatus = Me.Session.LastStatus
            Else
                Me.LastStatus = VisaNS.VisaStatusCode.SuccessDeviceNotPresent
            End If
            Return Me._LastStatus
        End Get
        Set(ByVal value As VisaNS.VisaStatusCode)
            If value <> Me._LastStatus Then
                Me._LastStatus = value
                Me.SafePostPropertyChanged("LastStatus")
                Me.VisaWarningAvailable = value > 0
                Me.VisaErrorAvailable = value < 0
            End If
        End Set
    End Property

    Private _ResourceName As String
    ''' <summary> Gets the name of the resource. </summary>
    ''' <value> The name of the resource or &lt;closed&gt; if not open. </value>
    Public Property ResourceName As String
        Get
            If Me.IsSessionOpen AndAlso String.IsNullOrWhiteSpace(Me._ResourceName) Then
                Me.ResourceName = Me.Session.ResourceName
            ElseIf Not Me.IsSessionOpen AndAlso Not String.IsNullOrWhiteSpace(Me._ResourceName) Then
                Me.ResourceName = "<closed>"
            End If
            Return Me._ResourceName
        End Get
        Set(ByVal value As String)
            Me._ResourceName = value
            Me.SafePostPropertyChanged("ResourceName")
        End Set
    End Property

    ''' <summary> Returns the status details for the last VISA message status. </summary>
    ''' <returns> The last visa status details. </returns>
    Public Function BuildVisaStatusDetails() As String
        If Me.IsSessionOpen Then
            Return Me.Session.BuildVisaStatusDetails
        ElseIf Me._Session Is Nothing Then
            Return "Visa warning: Device not enabled."
        Else
            Return "Visa warning: VISA Session not open."
        End If
    End Function

#End Region

#Region " QUERY / WRITE / EXECUTE "

#Region " EXECUTE "

    ''' <summary> Executes the command. </summary>
    ''' <param name="command"> The command. </param>
    Public Sub Execute(ByVal command As String)
        If Me.IsSessionOpen AndAlso Not String.IsNullOrWhiteSpace(command) Then
            Me.Session.WriteLine(command)
        End If
    End Sub

#End Region

#Region " ENUMERATION "

    ''' <summary> Issues the query command and parses the returned enum value into an Enum. </summary>
    ''' <param name="value">   The value. </param>
    ''' <param name="command"> The command. </param>
    ''' <returns> The parsed value or none if unknown. </returns>
    Public Function QueryValue(Of T As Structure)(ByVal command As Command, ByVal value As Nullable(Of T)) As Nullable(Of T)
        Dim currentValue As String = value.ToString
        If Me.IsSessionOpen AndAlso command IsNot Nothing AndAlso command.QuerySupported Then
            currentValue = Me.Session.QueryTrimEnd(command.QueryCommand)
        End If
        If String.IsNullOrWhiteSpace(currentValue) Then
            Return New Nullable(Of T)
        Else
            Dim se As New StringEnumerator(Of T)
            Return se.Parse(currentValue)
        End If
    End Function

    ''' <summary> Issues the query command and parses the returned enum value into an Enum. </summary>
    ''' <param name="value">        The value. </param>
    ''' <param name="queryCommand"> The query command. </param>
    ''' <returns> The parsed value or none if unknown. </returns>
    Public Function QueryValue(Of T As Structure)(ByVal queryCommand As String, ByVal value As Nullable(Of T)) As Nullable(Of T)
        If Me.IsSessionOpen AndAlso Not String.IsNullOrWhiteSpace(queryCommand) Then
            Return Me.Session.QueryEnumValue(Of T)(value, queryCommand)
        Else
            Return value
        End If
    End Function

    ''' <summary> Writes the Enum value without reading back the value from the device. </summary>
    ''' <param name="value">   The value. </param>
    ''' <param name="command"> The command. </param>
    ''' <returns> The value or none if unknown. </returns>
    Public Function WriteValue(Of T As Structure)(ByVal command As Command, ByVal value As T) As Nullable(Of T)
        If Me.IsSessionOpen AndAlso command IsNot Nothing AndAlso command.WriteSupported Then
            Return Me.Session.WriteEnumValue(Of T)(value, command.WriteCommand)
        Else
            Return value
        End If
    End Function

    ''' <summary> Writes the Enum value without reading back the value from the device. </summary>
    ''' <param name="value">         The value. </param>
    ''' <param name="commandFormat"> The command format. </param>
    ''' <returns> The value or none if unknown. </returns>
    Public Function WriteValue(Of T As Structure)(ByVal commandFormat As String, ByVal value As T) As Nullable(Of T)
        If Me.IsSessionOpen Then
            Return Me.Session.WriteEnumValue(Of T)(value, commandFormat)
        Else
            Return value
        End If
    End Function

    ''' <summary> Issues the query command and parses the returned enum value name into an Enum. </summary>
    ''' <param name="value">   The value. </param>
    ''' <param name="command"> The command. </param>
    ''' <returns> The parsed value or none if unknown. </returns>
    Public Function Query(Of T As Structure)(ByVal command As Command, ByVal value As Nullable(Of T)) As Nullable(Of T)
        Dim currentValue As String = value.ToString
        If Me.IsSessionOpen AndAlso command IsNot Nothing AndAlso command.QuerySupported Then
            currentValue = Me.Session.QueryTrimEnd(command.QueryCommand)
        End If
        If String.IsNullOrWhiteSpace(currentValue) Then
            Return New Nullable(Of T)
        Else
            Dim se As New StringEnumerator(Of T)
            Return se.ParseContained(currentValue.BuildDelimitedValue)
        End If
    End Function

    ''' <summary> Issues the query command and parses the returned en um value name into an Enum. </summary>
    ''' <param name="value">        The value. </param>
    ''' <param name="queryCommand"> The query command. </param>
    ''' <returns> The parsed value or none if unknown. </returns>
    Public Function Query(Of T As Structure)(ByVal queryCommand As String, ByVal value As Nullable(Of T)) As Nullable(Of T)
        If Me.IsSessionOpen AndAlso Not String.IsNullOrWhiteSpace(queryCommand) Then
            Return Me.Session.QueryEnum(Of T)(value, queryCommand)
        Else
            Return value
        End If
    End Function

    ''' <summary> Writes the Enum value name without reading back the value from the device. </summary>
    ''' <param name="value">   The value. </param>
    ''' <param name="command"> The command. </param>
    ''' <returns> The value or none if unknown. </returns>
    Public Function Write(Of T As Structure)(ByVal command As Command, ByVal value As T) As Nullable(Of T)
        If Me.IsSessionOpen AndAlso command IsNot Nothing AndAlso command.WriteSupported Then
            Return Me.Session.Write(Of T)(value, command.WriteCommand)
        Else
            Return value
        End If
    End Function

    ''' <summary> Writes the Enum value name without reading back the value from the device. </summary>
    ''' <param name="value">         The value. </param>
    ''' <param name="commandFormat"> The command format. </param>
    ''' <returns> The value or none if unknown. </returns>
    Public Function Write(Of T As Structure)(ByVal commandFormat As String, ByVal value As T) As Nullable(Of T)
        If Me.IsSessionOpen Then
            Return Me.Session.Write(Of T)(value, commandFormat)
        Else
            Return value
        End If
    End Function

#End Region

#Region " BOOLEAN "

    ''' <summary> Queries a <see cref="T:Boolean">Boolean</see> value. </summary>
    ''' <param name="value">        The value. </param>
    ''' <param name="queryCommand"> The query command. </param>
    ''' <returns> The value. </returns>
    Public Function Query(ByVal value As Boolean?, ByVal queryCommand As String) As Boolean?
        If Me.IsSessionOpen AndAlso Not String.IsNullOrWhiteSpace(queryCommand) Then
            value = Me.Session.Query(True, queryCommand)
        End If
        Return value
    End Function

    ''' <summary> Queries a <see cref="T:Boolean">Boolean</see> value. </summary>
    ''' <param name="value">   The value. </param>
    ''' <param name="command"> The command. </param>
    ''' <returns> The value. </returns>
    Public Function Query(ByVal value As Boolean?, ByVal command As Command) As Boolean?
        If Me.IsSessionOpen AndAlso command IsNot Nothing AndAlso command.QuerySupported Then
            value = Me.Session.Query(True, command.QueryCommand)
        End If
        Return value
    End Function

    ''' <summary> Write the value without reading back the value from the device. </summary>
    ''' <param name="value">   if set to <c>True</c> [value]. </param>
    ''' <param name="command"> The command. </param>
    ''' <returns> The value. </returns>
    Public Function Write(ByVal value As Boolean, ByVal command As Command) As Boolean?
        If Me.IsSessionOpen AndAlso command IsNot Nothing AndAlso command.WriteSupported Then
            Me.Session.WriteLine(command.WriteCommand, CType(value, Integer))
        End If
        If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
            Return New Boolean?
        Else
            Return value
        End If
    End Function

    ''' <summary> Write the value without reading back the value from the device. </summary>
    ''' <param name="value">         The value. </param>
    ''' <param name="commandFormat"> The command format. </param>
    ''' <returns> The value. </returns>
    Public Function Write(ByVal value As Boolean, ByVal commandFormat As String) As Boolean?
        If Me.IsSessionOpen AndAlso Not String.IsNullOrWhiteSpace(commandFormat) Then
            Me.Session.WriteLine(commandFormat, CType(value, Integer))
        End If
        If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
            Return New Boolean?
        Else
            Return value
        End If
    End Function

#End Region

#Region " INTEGER "

    ''' <summary> Queries an <see cref="T:Integer">integer</see> value. </summary>
    ''' <param name="value">        The value. </param>
    ''' <returns> The value. </returns>
    Public Function Query(ByVal value As Integer?, ByVal command As Command) As Integer?
        If Me.IsSessionOpen AndAlso command IsNot Nothing AndAlso command.QuerySupported Then
            value = Me.Session.Query(0I, command.QueryCommand)
        End If
        Return value
    End Function

    ''' <summary> Queries an <see cref="T:Integer">integer</see> value. </summary>
    ''' <param name="value">        The value. </param>
    ''' <param name="queryCommand"> The query command. </param>
    ''' <returns> The value. </returns>
    Public Function Query(ByVal value As Integer?, ByVal queryCommand As String) As Integer?
        If Me.IsSessionOpen AndAlso Not String.IsNullOrWhiteSpace(queryCommand) Then
            value = Me.Session.Query(0I, queryCommand)
        End If
        Return value
    End Function

    ''' <summary> Write the value without reading back the value from the device. </summary>
    ''' <param name="value">   The value. </param>
    ''' <param name="command"> The command. </param>
    ''' <returns> The value. </returns>
    Public Function Write(ByVal value As Integer, ByVal command As Command) As Integer?
        If Me.IsSessionOpen AndAlso command IsNot Nothing AndAlso command.WriteSupported Then
            Me.Session.WriteLine(command.WriteCommand, value)
        End If
        If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
            Return New Integer?
        Else
            Return value
        End If
    End Function

    ''' <summary> Write the value without reading back the value from the device. </summary>
    ''' <param name="value">         The value. </param>
    ''' <param name="commandFormat"> The command format. </param>
    ''' <returns> The value. </returns>
    Public Function Write(ByVal value As Integer, ByVal commandFormat As String) As Integer?
        If Me.IsSessionOpen AndAlso Not String.IsNullOrWhiteSpace(commandFormat) Then
            Me.Session.WriteLine(commandFormat, value)
        End If
        If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
            Return New Integer?
        Else
            Return value
        End If
    End Function

#End Region

#Region " DOUBLE "

    ''' <summary> Queries an <see cref="T:Double">Double</see> value. </summary>
    ''' <param name="value">   The value. </param>
    ''' <param name="command"> The command. </param>
    ''' <returns> The value. </returns>
    Public Function Query(ByVal value As Double?, ByVal command As Command) As Double?
        If Me.IsSessionOpen AndAlso command IsNot Nothing AndAlso command.QuerySupported Then
            value = Me.Session.Query(0.0F, command.QueryCommand)
        End If
        Return value
    End Function


    ''' <summary> Queries an <see cref="T:Double">Double</see> value. </summary>
    ''' <param name="value">        The value. </param>
    ''' <param name="queryCommand"> The query command. </param>
    ''' <returns> The value. </returns>
    Public Function Query(ByVal value As Double?, ByVal queryCommand As String) As Double?
        If Me.IsSessionOpen AndAlso Not String.IsNullOrWhiteSpace(queryCommand) Then
            value = Me.Session.Query(0.0F, queryCommand)
        End If
        Return value
    End Function

    ''' <summary> Write the value without reading back the value from the device. </summary>
    ''' <param name="value">         The value. </param>
    ''' <returns> The value. </returns>
    Public Function Write(ByVal value As Double, ByVal command As Command) As Double?
        If Me.IsSessionOpen AndAlso command IsNot Nothing AndAlso command.WriteSupported Then
            If value >= (Scpi.Syntax.Infinity - 1) Then
                If command.MaxSupported Then
                    Me.Session.WriteLine(command.WriteCommand, command.MaxValue)
                End If
                value = Scpi.Syntax.Infinity
            ElseIf value <= (Scpi.Syntax.NegativeInfinity + 1) Then
                If command.MinSupported Then
                    Me.Session.WriteLine(command.WriteCommand, command.MinValue)
                End If
                value = Scpi.Syntax.NegativeInfinity
            Else
                Me.Session.WriteLine(command.WriteCommand, value)
            End If
        End If
        If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
            Return New Double?
        Else
            Return value
        End If
    End Function

    ''' <summary> Write the value without reading back the value from the device. </summary>
    ''' <param name="value">         The value. </param>
    ''' <param name="commandFormat"> The command format. </param>
    ''' <returns> The value. </returns>
    Public Function Write(ByVal value As Double, ByVal commandFormat As String) As Double?
        If Me.IsSessionOpen AndAlso Not String.IsNullOrWhiteSpace(commandFormat) Then
            If value >= (Scpi.Syntax.Infinity - 1) Then
                Me.Session.WriteLine(commandFormat, "MAX")
                value = Scpi.Syntax.Infinity
            ElseIf value <= (Scpi.Syntax.NegativeInfinity + 1) Then
                Me.Session.WriteLine(commandFormat, "MIN")
                value = Scpi.Syntax.NegativeInfinity
            Else
                Me.Session.WriteLine(commandFormat, value)
            End If
        End If
        If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
            Return New Double?
        Else
            Return value
        End If
    End Function

#End Region

#Region " STRING "

    ''' <summary> Queries a <see cref="T:String">String</see> value. </summary>
    ''' <param name="value">   The present value. </param>
    ''' <param name="command"> The command. </param>
    ''' <returns> The value. </returns>
    Public Function Query(ByVal value As String, ByVal command As Command) As String
        If Me.IsSessionOpen AndAlso command IsNot Nothing AndAlso command.QuerySupported Then
            Return Me.Session.QueryTrimEnd(command.QueryCommand)
        Else
            Return value
        End If
    End Function

    ''' <summary> Queries a <see cref="T:String">String</see> value. </summary>
    ''' <param name="value">        The present value. </param>
    ''' <param name="queryCommand"> The query command. </param>
    ''' <returns> The value. </returns>
    Public Function Query(ByVal value As String, ByVal queryCommand As String) As String
        If Me.IsSessionOpen AndAlso Not String.IsNullOrWhiteSpace(queryCommand) Then
            Return Me.Session.QueryTrimEnd(queryCommand)
        Else
            Return value
        End If
    End Function

    ''' <summary> Write the value without reading back the value from the device. </summary>
    ''' <param name="value">         The value. </param>
    ''' <returns> The value. </returns>
    Public Function Write(ByVal value As String, ByVal command As Command) As String
        If Me.IsSessionOpen AndAlso command IsNot Nothing AndAlso command.WriteSupported Then
            Me.Session.WriteLine(command.WriteCommand, value)
        End If
        If Me.LastStatus <= VisaNS.VisaStatusCode.Success Then
            Return ""
        Else
            Return value
        End If
    End Function

    ''' <summary> Write the value without reading back the value from the device. </summary>
    ''' <param name="value">         The value. </param>
    ''' <param name="commandFormat"> The command format. </param>
    ''' <returns> The value. </returns>
    Public Function Write(ByVal value As String, ByVal commandFormat As String) As String
        If Me.IsSessionOpen AndAlso Not String.IsNullOrWhiteSpace(commandFormat) Then
            Me.Session.WriteLine(commandFormat, value)
        End If
        If Me.LastStatus <= VisaNS.VisaStatusCode.Success Then
            Return ""
        Else
            Return value
        End If
    End Function

#End Region

#Region " TIME SPAN "

    ''' <summary> Queries an <see cref="T:TimeSpan">TimeSpan</see> value. </summary>
    ''' <param name="command"> The command. </param>
    ''' <returns> The value. </returns>
    Public Function Query(ByVal value As TimeSpan?, ByVal command As Command) As TimeSpan?
        If Me.IsSessionOpen AndAlso command IsNot Nothing AndAlso command.QuerySupported Then
            If String.IsNullOrWhiteSpace(command.ReadFormat) Then
                value = TimeSpan.FromTicks(CLng(TimeSpan.TicksPerSecond * Me.Session.Query(0.0F, command.QueryCommand)))
            Else
                value = Me.Session.Query(command.ReadFormat, command.QueryCommand)
            End If
        End If
        Return value
    End Function

    ''' <summary> Queries an <see cref="T:TimeSpan">TimeSpan</see> value. </summary>
    ''' <param name="queryCommand"> The query command. </param>
    ''' <returns> The value. </returns>
    Public Function Query(ByVal value As TimeSpan?, ByVal format As String, ByVal queryCommand As String) As TimeSpan?
        If Me.IsSessionOpen AndAlso Not String.IsNullOrWhiteSpace(format) AndAlso Not String.IsNullOrWhiteSpace(queryCommand) Then
            value = Me.Session.Query(format, queryCommand)
        End If
        Return value
    End Function

    ''' <summary> Write the value without reading back the value from the device. </summary>
    ''' <param name="value">   The value. </param>
    ''' <param name="command"> The command. </param>
    ''' <returns> The value. </returns>
    Public Function Write(ByVal value As TimeSpan, ByVal command As Command) As TimeSpan?
        If Me.IsSessionOpen AndAlso command IsNot Nothing AndAlso command.WriteSupported Then
            Me.Session.WriteLine(command.WriteCommand, value)
        End If
        If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
            Return New TimeSpan?
        Else
            Return value
        End If
    End Function

    ''' <summary> Write the value without reading back the value from the device. </summary>
    ''' <param name="value">         The value. </param>
    ''' <param name="commandFormat"> The command format. </param>
    ''' <returns> The value. </returns>
    Public Function Write(ByVal value As TimeSpan, ByVal commandFormat As String) As TimeSpan?
        If Me.IsSessionOpen AndAlso Not String.IsNullOrWhiteSpace(commandFormat) Then
            Me.Session.WriteLine(commandFormat, value)
        End If
        If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
            Return New TimeSpan?
        Else
            Return value
        End If
    End Function

#End Region

#End Region

#Region " CHECK AND THROW "

    ''' <summary> Check visa status and throws an exception if a visa error occurred. Can be used with
    ''' queries. </summary>
    ''' <exception cref="VisaException"> Thrown when a Visa error condition occurs. </exception>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <param name="args">   A variable-length parameters list containing arguments. </param>
    Public Sub CheckThrowVisaException(ByVal format As String, ByVal ParamArray args() As Object)
        If Me.IsSessionOpen AndAlso Me.Session.LastStatus < VisaNS.VisaStatusCode.Success Then
            Throw New VisaException(Me.LastStatus, Me.ResourceName, format, args)
        End If
    End Sub

    ''' <summary> Check visa status and throws an exception if a visa error occurred.
    '''           Can be used with queries. </summary>
    ''' <param name="format">   Describes the format to use. </param>
    ''' <param name="args">     A variable-length parameters list containing arguments. </param>
    Public Sub CheckThrowVisaException(ByVal nodeNumber As Integer, ByVal format As String, ByVal ParamArray args() As Object)
        If Me.IsSessionOpen AndAlso Me.Session.LastStatus < VisaNS.VisaStatusCode.Success Then
            Throw New VisaException(Me.LastStatus, Me.ResourceName, nodeNumber, format, args)
        End If
    End Sub

#End Region

End Class


Imports NationalInstruments
''' <summary> Defines the contract that must be implemented by Subsystems that report status. </summary>
''' <license> (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/15/2005" by="David" revision="1.0.1841.x"> Created. </history>
Public MustInherit Class SubsystemPlusStatusBase
    Inherits SubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="SubsystemPlusStatusBase" /> class. </summary>
    ''' <param name="statusSubsystem "> A reference to a <see cref="IO.VISA.StatusSubsystemBase">status subsystem</see>. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId:="0")>
    Protected Sub New(ByVal statusSubsystem As IO.Visa.StatusSubsystemBase)
        MyBase.New(statusSubsystem.Session)
        Me._StatusSubsystem = statusSubsystem
    End Sub

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <param name="disposing"> <c>True</c> if this method releases both managed and unmanaged resources;
    ''' False if this method releases only unmanaged resources. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called
                    Me._StatusSubsystem = Nothing

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' dispose the base class.
            MyBase.Dispose(disposing)

        End Try

    End Sub

#End Region

#Region " STATUS "

    Private _StatusSubsystem As StatusSubsystemBase

    ''' <summary> Gets the status subsystem. </summary>
    ''' <value> The status subsystem. </value>
    Protected ReadOnly Property StatusSubsystem As StatusSubsystemBase
        Get
            Return _StatusSubsystem
        End Get
    End Property

    ''' <summary> Gets a value indicating whether the device is open. This is
    '''           required when the device is used in emulation. </summary>
    ''' <value> <c>True</c> if the device has an open session; otherwise, <c>False</c>. </value>
    Public ReadOnly Property IsDeviceOpen As Boolean
        Get
            Return Me._StatusSubsystem IsNot Nothing AndAlso Me.StatusSubsystem.IsDeviceOpen
        End Get
    End Property

    ''' <summary> Reads the registers. </summary>
    Public Sub ReadRegisters()
        Me.StatusSubsystem.ReadRegisters()
    End Sub

#End Region

#Region " CHECK AND THROW "

    ''' <summary> Checks and throws an exception if device errors occurred.
    ''' Can only be used after receiving a full reply from the device. </summary>
    ''' <exception cref="DeviceException"> Thrown when a device error condition occurs. </exception>
    ''' <param name="flushReadFirst"> Flushes the read buffer before processing the error. </param>
    ''' <param name="format">         Describes the format to use. </param>
    ''' <param name="args">           A variable-length parameters list containing arguments. </param>
    Public Sub CheckThrowDeviceException(ByVal flushReadFirst As Boolean, ByVal format As String, ByVal ParamArray args() As Object)
        Me.StatusSubsystem.CheckThrowDeviceException(flushReadFirst, format, args)
    End Sub

    ''' <summary> Checks and throws an exception if a visa or device errors occurred.
    ''' Can only be used after receiving a full reply from the device. </summary>
    ''' <exception cref="VisaException"> Thrown when a Visa error condition occurs. </exception>
    ''' <exception cref="DeviceException"> Thrown when a device error condition occurs. </exception>
    ''' <param name="flushReadFirst"> Flushes the read buffer before processing the error. </param>
    ''' <param name="format">         Describes the format to use. </param>
    ''' <param name="args">           A variable-length parameters list containing arguments. </param>
    Public Sub CheckThrowVisaDeviceException(ByVal flushReadFirst As Boolean, ByVal format As String, ByVal ParamArray args() As Object)
        Me.StatusSubsystem.CheckThrowVisaDeviceException(flushReadFirst, format, args)
    End Sub

    ''' <summary> Checks and throws an exception if a visa or device errors occurred.
    ''' Can only be used after receiving a full reply from the device. </summary>
    ''' <exception cref="VisaException"> Thrown when a Visa error condition occurs. </exception>
    ''' <exception cref="DeviceException"> Thrown when a device error condition occurs. </exception>
    ''' <param name="flushReadFirst"> Flushes the read buffer before processing the error. </param>
    ''' <param name="format">         Describes the format to use. </param>
    ''' <param name="args">           A variable-length parameters list containing arguments. </param>
    Public Sub CheckThrowVisaDeviceException(ByVal nodeNumber As Integer, ByVal flushReadFirst As Boolean, ByVal format As String, ByVal ParamArray args() As Object)
        Me.StatusSubsystem.CheckThrowVisaDeviceException(nodeNumber, flushReadFirst, format, args)
    End Sub

#End Region

#Region " CHECK AND REPORT "

    ''' <summary> Check and reports visa or device error occurred. Can only be used after receiving a
    ''' full reply from the device. </summary>
    ''' <param name="nodeNumber"> Specifies the remote node number to validate. </param>
    ''' <param name="format">     Specifies the report format. </param>
    ''' <param name="args">       Specifies the report arguments. </param>
    ''' <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
    Public Overridable Function ReportVisaDeviceOperationOkay(ByVal nodeNumber As Integer, ByVal format As String, ByVal ParamArray args() As Object) As Boolean
        Return Me.StatusSubsystem.ReportVisaDeviceOperationOkay(nodeNumber, format, args)
    End Function

    ''' <summary> Checks and reports if a visa or device error occurred. Can only be used after
    ''' receiving a full reply from the device. </summary>
    ''' <param name="nodeNumber">     Specifies the remote node number to validate. </param>
    ''' <param name="flushReadFirst"> Flushes the read buffer before processing the error. </param>
    ''' <param name="format">         Describes the format to use. </param>
    ''' <param name="args">           A variable-length parameters list containing arguments. </param>
    ''' <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
    Public Function ReportVisaDeviceOperationOkay(ByVal nodeNumber As Integer,
                                                  ByVal flushReadFirst As Boolean, ByVal format As String,
                                                  ByVal ParamArray args() As Object) As Boolean
        Return Me.StatusSubsystem.ReportVisaDeviceOperationOkay(nodeNumber, flushReadFirst, format, args)
    End Function

    ''' <summary> Checks and reports if a visa or device error occurred.
    ''' Can only be used after receiving a full reply from the device. </summary>
    ''' <param name="flushReadFirst"> Flushes the read buffer before processing the error. </param>
    ''' <param name="format">         Describes the format to use. </param>
    ''' <param name="args">           A variable-length parameters list containing arguments. </param>
    ''' <returns><c>True</c> if okay; otherwise, <c>False</c>. </returns>
    Public Function ReportVisaDeviceOperationOkay(ByVal flushReadFirst As Boolean, ByVal format As String,
                                                  ByVal ParamArray args() As Object) As Boolean
        Return Me.StatusSubsystem.ReportVisaDeviceOperationOkay(flushReadFirst, format, args)
    End Function

    ''' <summary> Reports if a visa error occurred.
    '''           Can be used with queries. </summary>
    ''' <param name="format">   Describes the format to use. </param>
    ''' <param name="args">     A variable-length parameters list containing arguments. </param>
    ''' <returns><c>True</c> if okay; otherwise, <c>False</c>. </returns>
    Public Function ReportVisaOperationOkay(ByVal format As String, ByVal ParamArray args() As Object) As Boolean
        Return Me.StatusSubsystem.ReportVisaOperationOkay(format, args)
    End Function

    ''' <summary> Reports if a visa error occurred. Can be used with queries. </summary>
    ''' <param name="nodeNumber">   Specifies the remote node number to validate. </param>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <param name="args">   A variable-length parameters list containing arguments. </param>
    ''' <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
    Public Function ReportVisaOperationOkay(ByVal nodeNumber As Integer, ByVal format As String, ByVal ParamArray args() As Object) As Boolean
        Return Me.StatusSubsystem.ReportVisaOperationOkay(nodeNumber, format, args)
    End Function

#End Region

End Class


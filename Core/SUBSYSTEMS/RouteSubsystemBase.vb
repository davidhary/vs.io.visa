Imports NationalInstruments

''' <summary> Defines the contract that must be implemented by a Route Subsystem. </summary>
''' <license> (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/15/2005" by="David" revision="1.0.1841.x"> Created. </history>
Public MustInherit Class RouteSubsystemBase
    Inherits SubsystemPlusStatusBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="RouteSubsystemBase" /> class. </summary>
    ''' <param name="statusSubsystem "> A reference to a <see cref="IO.VISA.StatusSubsystemBase">status subsystem</see>. </param>
    Protected Sub New(ByVal statusSubsystem As IO.Visa.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
        Me._ScanList = ""
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Sets the subsystem values to their known execution reset state. </summary>
    Public Overrides Sub ResetKnownState()
        MyBase.ResetKnownState()
        Me.ScanList = ""
        Me.TerminalMode = RouteTerminalMode.Front
    End Sub

#End Region

#Region " CHANNELS "

    ''' <summary> Gets the close channels command format. </summary>
    ''' <value> The close channels command format. </value>
    Protected MustOverride ReadOnly Property CloseChannelsCommandFormat As String

    ''' <summary> Closes the specified channels in the list. </summary>
    ''' <exception cref="ArgumentNullException" guarantee="strong"> . </exception>
    ''' <param name="channelList"> List of channels. </param>
    Public Sub CloseChannels(ByVal channelList As String)
        MyBase.Execute(String.Format(Me.CloseChannelsCommandFormat, channelList))
    End Sub

    ''' <summary> Gets the recall channel pattern command format. </summary>
    ''' <value> The recall channel pattern command format. </value>
    Protected MustOverride ReadOnly Property RecallChannelPatternCommandFormat As String

    ''' <summary> Recalls channel pattern from a memory location. </summary>
    ''' <exception cref="ArgumentNullException" guarantee="strong"> . </exception>
    ''' <param name="memoryLocation"> Specifies a memory location between 1 and 100. </param>
    Public Sub RecallChannelPattern(ByVal memoryLocation As Integer)
        MyBase.Execute(String.Format(Me.RecallChannelPatternCommandFormat, memoryLocation))
    End Sub

    ''' <summary> Gets or sets the save channel pattern command format. </summary>
    ''' <value> The save channel pattern command format. </value>
    Protected MustOverride ReadOnly Property SaveChannelPatternCommandFormat As String

    ''' <summary> Saves existing channel pattern into a memory location. </summary>
    ''' <exception cref="ArgumentNullException" guarantee="strong"> . </exception>
    ''' <param name="memoryLocation"> Specifies a memory location between 1 and 100. </param>
    Public Sub SaveChannelPattern(ByVal memoryLocation As Integer)
        MyBase.Execute(String.Format(Me.SaveChannelPatternCommandFormat, memoryLocation))
    End Sub

    ''' <summary> Gets the open channels command. </summary>
    ''' <value> The open channels command. </value>
    Protected MustOverride ReadOnly Property OpenChannelsCommand As String

    ''' <summary> Opens all channels. </summary>
    ''' <exception cref="ArgumentNullException" guarantee="strong"> . </exception>
    Public Sub OpenAll()
        MyBase.Execute(Me.OpenChannelsCommand)
    End Sub

    ''' <summary> Gets or sets the open channels command format. </summary>
    ''' <value> The open channels command format. </value>
    Protected MustOverride ReadOnly Property OpenChannelsCommandFormat As String

    ''' <summary> Opens the specified channels in the list. </summary>
    ''' <exception cref="ArgumentNullException" guarantee="strong"> . </exception>
    ''' <param name="channelList"> List of channels. </param>
    Public Sub OpenChannels(ByVal channelList As String)
        MyBase.Execute(String.Format(Me.OpenChannelsCommandFormat, channelList))
    End Sub

#End Region

#Region " SCAN LIST "

    ''' <summary> List of scans. </summary>
    Private _ScanList As String

    ''' <summary> Gets or sets the cached Scan List. </summary>
    ''' <value> A List of scans. </value>
    Public Overloads Property ScanList As String
        Get
            Return Me._ScanList
        End Get
        Protected Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = ""
            If Not value.Equals(Me.ScanList) Then
                Me._ScanList = value
                Me.SafePostPropertyChanged(NameOf(Me.ScanList))
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Scan List. </summary>
    ''' <param name="value"> The scan list. </param>
    ''' <returns> A List of scans. </returns>
    Public Function ApplyScanList(ByVal value As String) As String
        Me.WriteScanList(value)
        If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
            Return Me.ScanList
        Else
            Return Me.QueryScanList()
        End If
    End Function

    ''' <summary> Gets or sets the scan list command. </summary>
    ''' <value> The scan list command. </value>
    Protected MustOverride ReadOnly Property ScanListCommand As Command

    ''' <summary> Queries the Scan List. Also sets the <see cref="ScanList">Route on</see> sentinel. </summary>
    ''' <returns> A List of scans. </returns>
    Public Function QueryScanList() As String
        Me.ScanList = MyBase.Query(Me.ScanList, Me.ScanListCommand)
        Return Me.ScanList
    End Function

    ''' <summary> Writes the Scan List. Does not read back from the instrument. </summary>
    ''' <param name="value"> The scan list. </param>
    ''' <returns> A List of scans. </returns>
    Public Function WriteScanList(ByVal value As String) As String
        Me.ScanList = MyBase.Write(value, Me.ScanListCommand)
        Return Me.ScanList
    End Function

#End Region

#Region " SLOT CARD INFO "

    ''' <summary> Gets or sets the slot card type query command format. </summary>
    ''' <value> The slot card type query command format. </value>
    Protected MustOverride ReadOnly Property SlotCardTypeQueryCommandFormat As String

    ''' <summary> Queries the Slot Card Type. </summary>
    ''' <param name="slotNumber"> The slot number. </param>
    ''' <returns> A Slot Card Type. </returns>
    Public Function QuerySlotCardType(ByVal slotNumber As Integer) As String
        If String.IsNullOrWhiteSpace(Me.SlotCardTypeQueryCommandFormat) Then
            Return ""
        Else
            Return MyBase.Query("", String.Format(Me.SlotCardTypeQueryCommandFormat, slotNumber))
        End If
    End Function

    ''' <summary> Gets or sets the slot card settling time query command format. </summary>
    ''' <value> The slot card settling time query command format. </value>
    Protected MustOverride ReadOnly Property SlotCardSettlingTimeQueryCommandFormat As String

    ''' <summary> Queries the Slot Card settling time. </summary>
    ''' <param name="slotNumber"> The slot number. </param>
    ''' <returns> A Slot Card settling time. </returns>
    Public Function QuerySlotCardSettlingTime(ByVal slotNumber As Integer) As TimeSpan
        If String.IsNullOrWhiteSpace(Me.SlotCardSettlingTimeQueryCommandFormat) Then
            Return TimeSpan.Zero
        Else
            Dim value As Double? = MyBase.Query(New Double?, String.Format(Me.SlotCardSettlingTimeQueryCommandFormat, slotNumber))
            If value.HasValue Then
                Return TimeSpan.FromTicks(CLng(TimeSpan.TicksPerSecond * value.Value))
            Else
                Return TimeSpan.Zero
            End If
        End If
    End Function

#End Region

#Region " TERMINAL MODE "

    ''' <summary> The Route Terminal mode. </summary>
    Private _TerminalMode As RouteTerminalMode?

    ''' <summary> Gets or sets the cached Route Terminal mode. </summary>
    ''' <value> The Route Terminal mode or null if unknown. </value>
    Public Property TerminalMode As RouteTerminalMode?
        Get
            Return Me._TerminalMode
        End Get
        Protected Set(ByVal value As RouteTerminalMode?)
            If Not Nullable.Equals(Me.TerminalMode, value) Then
                Me._TerminalMode = value
                Me.SafePostPropertyChanged(NameOf(Me.TerminalMode))
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Route Terminal mode. </summary>
    ''' <param name="value"> The <see cref="RouteTerminalMode">Route Terminal mode</see>. </param>
    ''' <returns> The Route Terminal mode or null if unknown. </returns>
    Public Function ApplyTerminalMode(ByVal value As RouteTerminalMode) As RouteTerminalMode?
        Me.WriteTerminalMode(value)
        If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
            Return Me.TerminalMode
        Else
            Return Me.QueryTerminalMode()
        End If
    End Function

    ''' <summary> Gets or sets the terminal mode command. </summary>
    ''' <value> The terminal mode command. </value>
    Protected MustOverride ReadOnly Property TerminalModeCommand As Command

    ''' <summary> Queries the Route Terminal Mode. Also sets the <see cref="TerminalMode">output
    ''' on</see> sentinel. </summary>
    ''' <returns> The Route Terminal mode or null if unknown. </returns>
    Public Function QueryTerminalMode() As RouteTerminalMode?
        Me.TerminalMode = MyBase.Query(Of RouteTerminalMode)(Me.TerminalModeCommand, Me.TerminalMode)
        Return Me.TerminalMode
    End Function

    ''' <summary> Writes the Route Terminal mode. Does not read back from the instrument. </summary>
    ''' <param name="value"> The Terminal mode. </param>
    ''' <returns> The Route Terminal mode or null if unknown. </returns>
    Public Function WriteTerminalMode(ByVal value As RouteTerminalMode) As RouteTerminalMode?
        Me.TerminalMode = MyBase.Write(Of RouteTerminalMode)(Me.TerminalModeCommand, value)
        Return Me.TerminalMode
    End Function

#End Region

End Class

''' <summary> Specifies the route terminal mode. </summary>
Public Enum RouteTerminalMode
    <ComponentModel.Description("Not set")> None
    <ComponentModel.Description("Front (FRON)")> Front
    <ComponentModel.Description("Rear (REAR)")> Rear
End Enum

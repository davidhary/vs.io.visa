Imports NationalInstruments
Imports isr.Core.Pith
Imports isr.Core.Pith.EnumExtensions
Namespace Scpi

    ''' <summary> Defines the contract that must be implemented by a SCPI Source Voltage Subsystem. </summary>
    ''' <license> (c) 2012 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="9/26/2012" by="David" revision="1.0.4652"> Created. </history>
    <CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1501:AvoidExcessiveInheritance")>
    Public MustInherit Class SourceVoltageSubsystemBase
        Inherits Visa.SourceVoltageSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> Initializes a new instance of the <see cref="SourceVoltageSubsystemBase" /> class. </summary>
        ''' <param name="statusSubsystem "> A reference to a <see cref="IO.VISA.StatusSubsystemBase">status subsystem</see>. </param>
        Protected Sub New(ByVal statusSubsystem As IO.Visa.StatusSubsystemBase)
            MyBase.New(statusSubsystem)
        End Sub

#End Region

#Region " AUTO RANGE "

        ''' <summary> Queries the Auto Range Enabled sentinel. Also sets the
        ''' <see cref="AutoRangeEnabled">Enabled</see> sentinel. </summary>
        ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        Public Overrides Function QueryAutoRangeEnabled() As Boolean?
            If Me.IsSessionOpen Then
                MyBase.AutoRangeEnabled = Session.Query(True, ":SOUR:VOLT:RANG:AUTO?")
            End If
            Return MyBase.AutoRangeEnabled
        End Function

        ''' <summary> Writes the Auto Range Enabled sentinel. Does not read back from the instrument. </summary>
        ''' <param name="value"> if set to <c>True</c> is enabled. </param>
        ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        Public Overrides Function WriteAutoRangeEnabled(ByVal value As Boolean) As Boolean?
            If Me.IsSessionOpen Then
                Me.Session.WriteLine(":SOUR:VOLT:RANG:AUTO {0:'ON';'ON';'OFF'}", CType(value, Integer))
            End If
            If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
                MyBase.AutoRangeEnabled = New Boolean?
            Else
                MyBase.AutoRangeEnabled = value
            End If
            Return MyBase.AutoRangeEnabled
        End Function

#End Region

#Region " LEVEL "

        ''' <summary> Queries the level. </summary>
        ''' <returns> The level or none if unknown. </returns>
        Public Overrides Function QueryLevel() As Double?
            If Me.IsSessionOpen Then
                MyBase.Level = MyBase.Session.Query(0.0F, ":SOURCE:VOLT?")
            End If
            Return MyBase.Level
        End Function

        ''' <summary> Writes and reads back the source Voltage level. </summary>
        ''' <remarks> These commands set the immediate output Voltage level. The values are programmed in
        ''' Volts. The immediate level is the output Voltage setting. At *RST, the Voltage values = 0.
        ''' The range of values that can be programmed for these commands is coupled with the voltage
        ''' protection and the voltage limit low settings. The maximum value for the immediate and
        ''' triggered voltage level is either the value in the following table, or the voltage protection
        ''' setting divided by 1.05; whichever is lower. The minimum value is either the value in the
        ''' table, or the low voltage setting divided by 0.95; whichever is higher.  </remarks>
        ''' <param name="value"> The Voltage level. </param>
        ''' <returns> <c>True</c> if successful <see cref="VisaNS.VisaStatusCode">VISA Status Code</see>
        ''' was returned; <c>False</c> otherwise. </returns>
        Public Overrides Function WriteLevel(ByVal value As Double) As Double?
            If Me.IsSessionOpen Then
                MyBase.Session.WriteLine(":SOURCE:VOLT {0}", value)
            End If
            If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
                MyBase.Level = New Double?
            Else
                MyBase.Level = value
            End If
            Return MyBase.Level
        End Function

#End Region

#Region " SWEEP START LEVEL "

        ''' <summary> Queries the source voltage Sweep Start Level. </summary>
        ''' <returns> The source voltage Sweep Start Level or none if unknown. </returns>
        Public Overrides Function QuerySweepStartLevel() As Double?
            If Me.IsSessionOpen Then
                MyBase.SweepStartLevel = Me.Session.Query(0.0F, ":SOUR:VOLT:STAR?")
            End If
            Return MyBase.SweepStartLevel
        End Function

        ''' <summary> Writes and reads back the source Voltage Sweep Start Level. </summary>
        ''' <param name="value"> The Voltage SweepStartLevel. </param>
        ''' <returns> <c>True</c> if successful <see cref="VisaNS.VisaStatusCode">VISA Status Code</see>
        ''' was returned; <c>False</c> otherwise. </returns>
        Public Overrides Function WriteSweepStartLevel(ByVal value As Double) As Double?
            If Me.IsSessionOpen Then
                Me.Session.WriteLine(":SOUR:VOLT:STAR {0}", value)
            End If
            If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
                MyBase.SweepStartLevel = New Double?
            Else
                MyBase.SweepStartLevel = value
            End If
            Return MyBase.SweepStartLevel
        End Function

#End Region

#Region " SWEEP STOP LEVEL "

        ''' <summary> Queries the source voltage Sweep Stop Level. </summary>
        ''' <returns> The source voltage Sweep Stop Level or none if unknown. </returns>
        Public Overrides Function QuerySweepStopLevel() As Double?
            If Me.IsSessionOpen Then
                MyBase.SweepStopLevel = Me.Session.Query(0.0F, ":SOUR:VOLT:STOP?")
            End If
            Return MyBase.SweepStopLevel
        End Function


        ''' <summary> Writes and reads back the source Voltage Sweep Stop Level. </summary>
        ''' <param name="value"> The Voltage SweepStopLevel. </param>
        ''' <returns> <c>True</c> if successful <see cref="VisaNS.VisaStatusCode">VISA Status Code</see>
        ''' was returned; <c>False</c> otherwise. </returns>
        Public Overrides Function WriteSweepStopLevel(ByVal value As Double) As Double?
            If Me.IsSessionOpen Then
                Me.Session.WriteLine(":SOUR:VOLT:STOP {0}", value)
            End If
            If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
                MyBase.SweepStopLevel = New Double?
            Else
                MyBase.SweepStopLevel = value
            End If
            Return MyBase.SweepStopLevel
        End Function

#End Region

#Region " SWEEP MODE "

        ''' <summary> Queries the sweep mode. </summary>
        ''' <returns> The sweep mode or none if unknown. </returns>
        Public Overrides Function QuerySweepMode() As SweepMode?
            Dim mode As String = MyBase.SweepMode.ToString
            If Me.IsSessionOpen Then
                mode = Me.Session.QueryTrimEnd(":SOUR:VOLT:MODE?")
            End If
            If String.IsNullOrWhiteSpace(mode) Then
                MyBase.SweepMode = New SweepMode?
            Else
                Dim se As New StringEnumerator(Of SweepMode)
                MyBase.SweepMode = se.ParseContained(mode.BuildDelimitedValue)
            End If
            Return MyBase.SweepMode
        End Function

        ''' <summary> Writes the sweep mode but does not read back from the device. </summary>
        ''' <param name="value"> The  sweep mode. </param>
        ''' <returns> The sweep mode or none if unknown. </returns>
        Public Overrides Function WriteSweepMode(ByVal value As SweepMode) As SweepMode?
            If Me.IsSessionOpen Then
                Me.Session.WriteLine(":SOUR:VOLT:MODE {0}", value.ExtractBetween())
            End If
            If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
                MyBase.SweepMode = New SweepMode?
            Else
                MyBase.SweepMode = value
            End If
            Return MyBase.SweepMode
        End Function

#End Region

    End Class

End Namespace

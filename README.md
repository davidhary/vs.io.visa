# VISA Project

A library for support and extension of the [National Instruments] (https://www.ni.com) and [IVI] (https://www.ivifoundation.org) VISA.

## Getting Started

Clone the project along with its requisite projects to their respective relative path. All projects are installed relative to a common path.

### Prerequisites

The following projects are also required:
* [Core](https://www.bitbucket.org/davidhary/vs.core) - Core Project
* [Share](https://www.bitbucket.org/davidhary/vs.share) - Shared snippets
* [Visa](https://www.bitbucket.org/davidhary/vs.visa) - Visa Project

```
git clone git@bitbucket.org:davidhary/vs.share.git
git clone git@bitbucket.org:davidhary/vs.core.git
git clone git@bitbucket.org:davidhary/vs.visa.git
```

### Installing

Install the projects into the following folders:

#### Projects relative path
```
.\Libraries\VS\Share
.\Libraries\VS\Core\Core
.\Libraries\VS\IO\Visa
```

## Testing

The project includes a few unit test classes. Test applications are under the *Apps* solution folder. 

## Deployment

Deployment projects have not been created for this project.

## Built, Tested and Facilitated By

* [Visual Studio](https://www.visualstudio.com/) - Visual Studio 2015
* [Jarte](https://www.jarte.com/) - RTF Editor
* [Wix Installer](https://www.wixtoolset.org/) - Wix Toolset
* [Atomineer Code Documentation](https://www.atomineerutils.com/) - Code Documentation
* [EW Software](https://github.com/EWSoftware/VSSpellChecker/wiki/) - Spell Checker
* [NI VISA](https://www.NI.com) - National Instruments
* [IVI VISA](https://www.IVIFoundation.org) - IVI Foundation

## Authors

* **David Hary** - *Initial Workarounds* - [ATE Coder](http://www.integratedscientificresources.com)

## License

This project is licensed under the MIT License - see the [LICENSE.md] file at (https://www.bitbucket.org/davidhary/vs.visa/src) for details

## Acknowledgments

* Hat tip to anyone who's code was used
* [Its all a remix] (www.everythingisaremix.info) -- we are but a spec on the shoulders of giants
* [John Simmons](https://www.codeproject.com/script/Membership/View.aspx?mid=7741) - outlaw programmer
* [Stack overflow] (https://www.stackoveflow.com)

## Revision Changes

* Version 3.0.5226	05/23/14	Uses new stack trace parser. Fixes the checking for visa and device errors. 
* Version 3.0.5189	03/17/14	Applies the session status elements when calling the resetting the status subsystem known state even if the status subsystem does not have a defined reset command.
* Version 3.0.5181	03/09/14	Uses abstract properties to define instrument commands. 

﻿Imports isr.Core.Controls.StatusBarExtensions
Imports isr.Core.Controls.TextBoxExtensions
Imports isr.Core.Controls.ToolStripExtensions
Imports isr.Core.Pith
''' <summary> The T1750 Test Panel. </summary>
''' <license> (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="02/07/2005" by="David" revision="2.0.2597.x"> Created. </history>
Public Class TestPanel
#Const designMode1 = True
#Region " BASE FROM WRAPPER "
    ' Designing requires changing the condition to True.
#If designMode Then
    Inherits TraceObserverUserFormBaseWrapper
#Else
    Inherits TraceObserverUserFormBase
#End If
#End Region

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()

        ' Initialize user components that might be affected by resize or paint actions
        'onInitialize()

        ' This method is required by the Windows Form Designer.
        InitializeComponent()

#If designMode Then
        Debug.Assert(False, "Illegal call; reset design mode")
#End If

    End Sub

    ''' <summary> Cleans up managed components. </summary>
    ''' <remarks> Use this method to reclaim managed resources used by this class. </remarks>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")>
    Private Sub OnDisposeManagedResources()
    End Sub

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Form.Closing" /> event. Releases all
    ''' publishers. </summary>
    ''' <param name="e"> A <see cref="T:System.ComponentModel.CancelEventArgs" /> that contains the
    ''' event data. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Overrides Sub OnClosing(ByVal e As System.ComponentModel.CancelEventArgs)
        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            If e IsNot Nothing Then
                e.Cancel = False
            End If
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
            Application.DoEvents()
            MyBase.OnClosing(e)
        End Try
    End Sub

    ''' <summary> Called upon receiving the <see cref="E:System.Windows.Forms.Form.Load" /> event. </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            Trace.CorrelationManager.StartLogicalOperation(Reflection.MethodInfo.GetCurrentMethod.Name)

            ' Add any initialization after the InitializeComponent() call
            ' Me.OnInstantiate()
            If Not Me.DesignMode Then
                Me._Tabs.TabPages.Remove(Me._K2700TabPage)
                Me._Tabs.TabPages.Remove(Me._K2002TabPage)
            End If

            ' set the form caption
            Me.Text = My.Application.Info.ProductName & " release " & My.Application.Info.Version.ToString

            ' build the navigator tree.
            ' Me.BuildNavigatorTreeView()

            ' default to center screen.
            Me.CenterToScreen()

            MyBase.OnLoad(e)

        Catch ex As Exception

            ex.Data.Add("@isr", "Exception loading the form.")
            My.Application.MyLog.TraceSource.TraceEvent(ex, My.MyApplication.TraceEventId)
            If Windows.Forms.DialogResult.Abort = isr.Core.MyMessageBox.ShowDialogAbortIgnore(Me, ex, MessageBoxIcon.Error) Then
                Application.Exit()
            End If

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default
            Trace.CorrelationManager.StopLogicalOperation()

        End Try

    End Sub

    ''' <summary> Called upon receiving the <see cref="E:System.Windows.Forms.Form.Shown" /> event. </summary>
    ''' <param name="e"> A <see cref="T:System.EventArgs" /> that contains the event data. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Overrides Sub OnShown(e As System.EventArgs)

        Try

            Trace.CorrelationManager.StartLogicalOperation(Reflection.MethodInfo.GetCurrentMethod.Name)

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' allow form rendering time to complete: process all messages currently in the queue.
            Application.DoEvents()

            ' display the log file location
            Me.OnTraceMessageAvailable(TraceEventType.Verbose, isr.IO.Visa.Handler.My.MyLibrary.TraceEventId, "Logging;. to: '{0}'",
                                       My.Application.Log.DefaultFileLogWriter.CustomLocation)

            ' allow form rendering time to complete: process all messages currently in the queue.
            Application.DoEvents()
            If Not Me.DesignMode Then
                Me._InterfacePanel.InstrumentChooser.Connectible = False
                Me._InterfacePanel.DisplayInterfaceNames()
                Me.OnTraceMessageAvailable(TraceEventType.Verbose, isr.IO.Visa.Handler.My.MyLibrary.TraceEventId, "Ready to open Visa Session;. ")
            End If

            MyBase.OnShown(e)

        Catch ex As Exception

            ex.Data.Add("@isr", "Exception showing the form.")
            My.Application.MyLog.TraceSource.TraceEvent(ex, My.MyApplication.TraceEventId)
            If Windows.Forms.DialogResult.Abort = isr.Core.MyMessageBox.ShowDialogAbortIgnore(Me, ex, MessageBoxIcon.Error) Then
                Application.Exit()
            End If

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default
            Trace.CorrelationManager.StopLogicalOperation()

        End Try

    End Sub


#End Region

#Region " TRACE MESSAGE EVENT HANDLERS "

    ''' <summary> handles trace message available events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Trace message event information. </param>
    Private Sub TraceMessageAvailableHandler(ByVal sender As Object, ByVal e As TraceMessageEventArgs) Handles _InterfacePanel.TraceMessageAvailable
        MyBase.OnTraceMessageAvailable(sender, e)
    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary> Event handler. Called by exitButton for click events. </summary>
    ''' <param name="eventSender"> The event sender. </param>
    ''' <param name="eventArgs">   Event information. </param>
    Private Sub ExitButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)
        Me.Close()
    End Sub

#End Region

#Region " TRACE MESSAGE OBSERVER "

    ''' <summary> Builds a message for the message log appending a line. </summary>
    ''' <param name="message"> Specifies the message to append. </param>
    ''' <returns> The time stamped message. </returns>
    Friend Shared Function BuildTimeStampLine(ByVal message As String) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, "{0:HH:mm:ss.fff} {1}{2}", DateTime.Now, message, Environment.NewLine)
    End Function

    ''' <summary> Displays the <paramref name="value">message</paramref>. </summary>
    ''' <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
    Protected Overrides Sub DisplayMessage(value As TraceMessage)
        If value IsNot Nothing AndAlso MyBase.ShouldShow(value.EventType) Then
            Me._MessagesTextBox.Prepend(TestPanel.BuildTimeStampLine(value.Details))
        End If
    End Sub

    ''' <summary> Displays the <paramref name="value">synopsis</paramref>. </summary>
    ''' <param name="value"> The synopsis to display. </param>
    Protected Overrides Sub DisplaySynopsis(value As String)
        Me._StatusToolStripLabel.SafeTextSetter(value)
    End Sub

    ''' <summary> Gets or sets the trace show level. The trace message is displayed if the trace level is lower
    ''' than this value. </summary>
    ''' <value> The trace show level. </value>
    Protected Overrides ReadOnly Property TraceShowLevel As Diagnostics.TraceEventType
        Get
            Return My.Application.MyLog.TraceLevel
        End Get
    End Property

#End Region

End Class
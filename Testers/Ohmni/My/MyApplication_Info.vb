﻿Namespace My

    Partial Friend Class MyApplication

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = ProjectTraceEventId.OhmniTester

        Public Const AssemblyTitle As String = "VISA Ohmni Tester"
        Public Const AssemblyDescription As String = "VISA Ohmni Tester"
        Public Const AssemblyProduct As String = "IO.VISA.Ohmni.Tester.2018"

    End Class

End Namespace


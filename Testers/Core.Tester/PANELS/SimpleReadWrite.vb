﻿Imports isr.Core.Pith.EscapeSequencesExtensions
''' <summary> Simple read write. </summary>
''' <license> (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="10/2/2013" by="David" revision=""> Based on NI Code. </history>
Public Class SimpleReadWrite

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructor that prevents a default instance of this class from being created. </summary>
    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me._PolledStatusToolStripLabel.Text = "0x.."
        Me._PolledStatusToolStripLabel.ToolTipText = "Status byte. Double click to update"

        Dim escape As String = isr.Core.Pith.EscapeSequencesExtensions.NewLineEscape
        With Me._WriteComboBox
            .Items.Clear()
            .Items.Add(Ieee488.Syntax.ClearExecutionStateCommand & escape)
            .Items.Add(Ieee488.Syntax.IdentityQueryCommand & escape)
            .Items.Add(Ieee488.Syntax.OperationCompletedCommand & escape)
            .Items.Add(Ieee488.Syntax.OperationCompletedQueryCommand & escape)
            .Items.Add(Ieee488.Syntax.ResetKnownStateCommand & escape)
            .Items.Add(String.Format(Globalization.CultureInfo.CurrentCulture,
                                     Ieee488.Syntax.ServiceRequestEnableCommandFormat, 255) & escape)
            .Items.Add(Ieee488.Syntax.ServiceRequestEnableQueryCommand & escape)
            .Items.Add(String.Format(Globalization.CultureInfo.CurrentCulture,
                                     Ieee488.Syntax.StandardEventEnableCommandFormat, 255) & escape)
            .Items.Add(Ieee488.Syntax.StandardEventEnableQueryCommand & escape)
            .Items.Add(Ieee488.Syntax.WaitCommand & escape)
            .SelectedIndex = 1
        End With
    End Sub

    ''' <summary> Executes the dispose managed resources action. </summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub OnDisposeManagedResources()
        If Me._session IsNot Nothing Then
            Me._session.Dispose()
            Try
                ' Trying to null the session raises an ObjectDisposedException 
                ' if session service request handler was not released. 
                Me._session = Nothing
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToString)
            End Try
        End If
    End Sub

#End Region

#Region " SESSION "

    Private _Session As Visa.Session

    ''' <summary> Selects a resource and open a message based session with this resource. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub OpenSession_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _OpenSessionButton.Click

        Try

            Me.clear()
            Using selector As New Instrument.ResourceSelectorDialog()
                If selector.ShowDialog(Me) = Windows.Forms.DialogResult.OK Then
                    Me._StatusLabel.Text = "Opening Session."
                    Windows.Forms.Cursor.Current = Cursors.WaitCursor
                    Me._session = New Visa.Session(selector.ResourceName)
                    If Me._session IsNot Nothing Then
                        Me.UpdateControlsState(True)
                        Me._StatusLabel.Text = "Session Open."
                    Else
                        Me._StatusLabel.Text = "Failed Opening Session."
                        Me._ReadTextBox.Text = "Failed opening message based VISA session"
                    End If
                End If
            End Using

        Catch ex As Exception

            Me._StatusLabel.Text = "Exception occurred opening session."
            Me._ReadTextBox.Text = ex.ToString

        Finally

            Windows.Forms.Cursor.Current = Cursors.Default

        End Try
        Return

    End Sub

    ''' <summary> Closed an open session. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub CloseSession_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _CloseSessionButton.Click
        Me._StatusLabel.Text = "Closing Session."
        Me.UpdateControlsState(False)
        If Me._session IsNot Nothing Then
            Me._session.Dispose()
            Try
                ' Trying to null the session raises an ObjectDisposedException 
                ' if session service request handler was not released. 
                Me._session = Nothing
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToString)
            End Try
        End If
        Me._StatusLabel.Text = "Session close."
    End Sub

#End Region

#Region " READ AND WRITE "

    ''' <summary> Builds write message. Add Return and New Line in this order as required. </summary>
    Private Function BuildWriteMessage() As String
        Dim message As New System.Text.StringBuilder(_WriteComboBox.Text.Trim)
        If Me._AppendReturnCheckBox.Checked AndAlso
                Not message.ToString.Contains(isr.Core.Pith.EscapeSequencesExtensions.ReturnEscape) Then
            message.Append(isr.Core.Pith.EscapeSequencesExtensions.ReturnEscape)
        End If
        If Me._AppendNewLineCheckBox.Checked AndAlso
                Not message.ToString.Contains(isr.Core.Pith.EscapeSequencesExtensions.NewLineEscape) Then
            message.Append(isr.Core.Pith.EscapeSequencesExtensions.NewLineEscape)
        End If
        Return message.ToString
    End Function

    ''' <summary> Queries (write and then reads) the instrument. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub Query_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _QueryButton.Click
        Windows.Forms.Cursor.Current = Cursors.WaitCursor
        Try
            Me._StatusLabel.Text = "Querying."
            Dim textToWrite As String = Me.buildWriteMessage
            My.Application.MyLog.TraceSource.TraceEvent(TraceEventType.Verbose, My.MyApplication.TraceEventId, "Querying: '{0}'", textToWrite)
            textToWrite = textToWrite.ReplaceCommonEscapeSequences
            Dim timer As New Diagnostics.Stopwatch()
            timer.Start()
            Dim responseString As String = Me._session.Query(textToWrite)
            Dim lastStatus As NationalInstruments.VisaNS.VisaStatusCode = Me._session.LastStatus
            If lastStatus <> NationalInstruments.VisaNS.VisaStatusCode.Success Then
                My.Application.MyLog.TraceSource.TraceEvent(TraceEventType.Warning, My.MyApplication.TraceEventId, "Failure. Last Visa Status: {0}", lastStatus)
            End If
            Me._TimingTextBox.Text = timer.Elapsed.TotalMilliseconds.ToString("0.0", Globalization.CultureInfo.CurrentCulture)
            Dim message As String = responseString.InsertCommonEscapeSequences
            My.Application.MyLog.TraceSource.TraceEvent(TraceEventType.Verbose, My.MyApplication.TraceEventId, "Received: '{0}'", message)
            Me.updateReadMessage(message)
            Me._StatusLabel.Text = "Done Querying."
        Catch ex As Exception
            Me._StatusLabel.Text = "Error Querying."
            Me._ReadTextBox.Text = ex.ToString
        Finally
            Windows.Forms.Cursor.Current = Cursors.Default
        End Try
    End Sub

    ''' <summary> Writes a message to the instrument. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub Write_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _WriteButton.Click
        Windows.Forms.Cursor.Current = Cursors.WaitCursor
        Try
            Me._StatusLabel.Text = "Writing."
            Dim textToWrite As String = Me.buildWriteMessage
            My.MyLibrary.MyLog.TraceSource.TraceEvent(TraceEventType.Verbose, My.MyApplication.TraceEventId, "Writing: '{0}'", textToWrite)
            textToWrite = textToWrite.ReplaceCommonEscapeSequences
            Dim timer As New Diagnostics.Stopwatch()
            timer.Start()
            Me._session.Write(textToWrite)
            Me._TimingTextBox.Text = timer.Elapsed.TotalMilliseconds.ToString("0.0", Globalization.CultureInfo.CurrentCulture)
            Me._StatusLabel.Text = "Done Writing."
        Catch ex As Exception
            Me._StatusLabel.Text = "Error Writing."
            Me._ReadTextBox.Text = ex.ToString
        Finally
            Windows.Forms.Cursor.Current = Cursors.Default
        End Try
    End Sub

    ''' <summary> Updates the read message described by message. </summary>
    ''' <param name="message"> The message. </param>
    Private Sub UpdateReadMessage(ByVal message As String)
        Dim builder As New System.Text.StringBuilder()
        If Me._ReadTextBox.Text.Length > 0 Then
            builder.AppendLine(Me._ReadTextBox.Text)
        End If
        builder.Append(message)
        Me._ReadTextBox.Text = builder.ToString
    End Sub

    ''' <summary> Reads this object. </summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub Read()
        Windows.Forms.Cursor.Current = Cursors.WaitCursor
        Try
            Me._StatusLabel.Text = "Reading."
            Dim timer As New Diagnostics.Stopwatch()
            timer.Start()
            Dim responseString As String = Me._session.ReadString()
            Me._TimingTextBox.Text = timer.Elapsed.TotalMilliseconds.ToString("0.0", Globalization.CultureInfo.CurrentCulture)
            Dim message As String = responseString.InsertCommonEscapeSequences
            My.Application.MyLog.TraceSource.TraceEvent(TraceEventType.Information, My.MyApplication.TraceEventId, "Received: '{0}'", message)
            Me.updateReadMessage(message)
            Me._StatusLabel.Text = "Done Reading."
        Catch ex As Exception
            Me._StatusLabel.Text = "Error Reading."
            Me._ReadTextBox.Text = ex.ToString
        Finally
            Windows.Forms.Cursor.Current = Cursors.Default
        End Try
    End Sub

    ''' <summary> Reads a message from the instrument. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Read_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ReadButton.Click
        Me.read()
    End Sub

    ''' <summary> Event handler. Called by _ClearSessionButton for click events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _ClearSessionButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ClearSessionButton.Click
        Try
            If Me._session IsNot Nothing Then
                Me._session.Clear()
            End If
        Catch ex As Exception
            Me._StatusLabel.Text = "Error clearing the session."
            Me._ReadTextBox.Text = ex.ToString
        Finally
            Windows.Forms.Cursor.Current = Cursors.Default
        End Try
    End Sub


    ''' <summary> Clears the text boxes. </summary>
    Private Sub Clear()
        Me._ReadTextBox.Text = ""
        Me._StatusLabel.Text = ""
        Me._PolledStatusToolStripLabel.Text = "0x.."
        Me._PolledStatusToolStripLabel.ToolTipText = "Status byte. Double click to update"
    End Sub

    ''' <summary> Event handler. Called by clear for click events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Clear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ClearButton.Click
        Me.clear()
    End Sub

    ''' <summary> Updates the controls state. </summary>
    ''' <param name="sessionOpen"> . </param>
    Private Sub UpdateControlsState(ByVal sessionOpen As Boolean)
        Me._OpenSessionButton.Enabled = Not sessionOpen
        Me._CloseSessionButton.Enabled = sessionOpen
        Me._QueryButton.Enabled = sessionOpen AndAlso Not Me._AutoReadCheckBox.Checked
        Me._ReadButton.Enabled = sessionOpen AndAlso Not Me._AutoReadCheckBox.Checked
        Me._ClearSessionButton.Enabled = sessionOpen AndAlso Not Me._AutoReadCheckBox.Checked
        Me._WriteButton.Enabled = sessionOpen
        Me._WriteComboBox.Enabled = sessionOpen
        Me._ClearButton.Enabled = sessionOpen
        If sessionOpen Then
            Me._ReadTextBox.Text = String.Empty
            Me._WriteComboBox.Focus()
        End If
    End Sub

#End Region

#Region " POLL "

    ''' <summary> Event handler. Called by _AutoReadCheckBox for checked changed events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _AutoReadCheckBox_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _AutoReadCheckBox.CheckedChanged
        Me._QueryButton.Enabled = Me._session IsNot Nothing AndAlso Not Me._AutoReadCheckBox.Checked
        Me._ReadButton.Enabled = Me._session IsNot Nothing AndAlso Not Me._AutoReadCheckBox.Checked
    End Sub

    ''' <summary> Event handler. Called by _PollMillisecondsNumeric for value changed events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _PollMillisecondsNumeric_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _PollMillisecondsNumeric.ValueChanged
        Me._PollEnableCheckBox.Checked = False
    End Sub

    ''' <summary> Event handler. Called by _PollEnableCheckBox for checked changed events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _PollEnableCheckBox_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _PollEnableCheckBox.CheckedChanged
        Me._PolledStatusToolStripLabel.Visible = Me._PollEnableCheckBox.Checked
        Me._PollTimer.Enabled = False
        If Me._PollEnableCheckBox.Checked Then
            Me._PollTimer.Interval = CInt(Me._PollMillisecondsNumeric.Value)
        End If
        Me._PollTimer.Enabled = Me._PollEnableCheckBox.Checked
    End Sub

    ''' <summary> Reads and displays the status byte. </summary>
    Private Function DisplayStatusByte() As Integer
        Dim statusByte As Integer = -1
        If Me._session Is Nothing Then
            Me._PolledStatusToolStripLabel.Text = "0x.."
            Me._PolledStatusToolStripLabel.ToolTipText = "Status byte. Double click to update"
        Else
            statusByte = Me._session.ReadStatusByte
            Me._PolledStatusToolStripLabel.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "0x{0:X}", statusByte)
            Me._PolledStatusToolStripLabel.ToolTipText = IO.Visa.StatusSubsystemBase.BuildReport(CType(statusByte, IO.Visa.ServiceRequests), ";")
        End If
        Return statusByte
    End Function

    ''' <summary> Event handler. Called by _PolledStatusToolStripLabel for double click events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _PolledStatusToolStripLabel_DoubleClick(sender As System.Object, ByVal e As System.EventArgs) Handles _PolledStatusToolStripLabel.DoubleClick
        If Not Me._PollEnableCheckBox.Checked Then
            Me.displayStatusByte()
        End If
    End Sub

    ''' <summary> Event handler. Called by _PollTimer for tick events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _PollTimer_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles _PollTimer.Tick
        Try
            _PollTimer.Enabled = False
            If Me._session Is Nothing Then
                Me._PolledStatusToolStripLabel.Text = "0x.."
            Else
                Dim statusbyte As Integer = displayStatusByte()
                If ((statusbyte And CInt(Me._MessageStatusBitValueNumeric.Value)) = Me._MessageStatusBitValueNumeric.Value) AndAlso
                    Me._AutoReadCheckBox.Checked Then
                    Threading.Thread.Sleep(10)
                    Me.read()
                End If
            End If
        Catch
        Finally
            _PollTimer.Enabled = True
        End Try
    End Sub

#End Region

#Region " FORM HANDLERS "

    ''' <summary> Does all the post processing after all the form controls are rendered as the user
    ''' expects them. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub Form_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown

        ' allow form rendering time to complete: process all messages currently in the queue.
        Application.DoEvents()

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' allow some events to occur for refreshing the display.
            Application.DoEvents()

        Catch ex As Exception
            ex.Data.Add("@isr", "Exception showing the form.")
            If DialogResult.Abort = isr.Core.MyMessageBox.ShowDialogAbortIgnore(Me, ex, MessageBoxIcon.Error) Then
                Application.Exit()
            End If
        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

#End Region

End Class
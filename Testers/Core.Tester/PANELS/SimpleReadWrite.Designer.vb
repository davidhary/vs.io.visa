﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> 
Partial Class SimpleReadWrite
    Inherits Core.Pith.UserFormBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> 
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                Me.onDisposeManagedResources()
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId:="statusPanel")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId:="readStatusPanel")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId:="writeStatusPanel")>
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me._ClearButton = New System.Windows.Forms.Button()
        Me._ReadTextBox = New System.Windows.Forms.TextBox()
        Me._ReadTextBoxLabel = New System.Windows.Forms.Label()
        Me._WriteTextBoxLabel = New System.Windows.Forms.Label()
        Me._CloseSessionButton = New System.Windows.Forms.Button()
        Me._OpenSessionButton = New System.Windows.Forms.Button()
        Me._ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._PollMillisecondsNumeric = New System.Windows.Forms.NumericUpDown()
        Me._MessageStatusBitValueNumeric = New System.Windows.Forms.NumericUpDown()
        Me._AutoReadCheckBox = New System.Windows.Forms.CheckBox()
        Me._ReadButton = New System.Windows.Forms.Button()
        Me._WriteButton = New System.Windows.Forms.Button()
        Me._QueryButton = New System.Windows.Forms.Button()
        Me._TimingTextBox = New System.Windows.Forms.TextBox()
        Me._TimingUnitsLabel = New System.Windows.Forms.Label()
        Me._ToolStrip = New System.Windows.Forms.ToolStrip()
        Me._StatusLabel = New System.Windows.Forms.ToolStripLabel()
        Me._PolledStatusToolStripLabel = New System.Windows.Forms.ToolStripLabel()
        Me._PollSettingsGroupBox = New System.Windows.Forms.GroupBox()
        Me._MessageStatusBitsNumericLabel = New System.Windows.Forms.Label()
        Me._PollMillisecondsNumericLabel = New System.Windows.Forms.Label()
        Me._PollEnableCheckBox = New System.Windows.Forms.CheckBox()
        Me._WriteSettingsGroupBox = New System.Windows.Forms.GroupBox()
        Me._AppendNewLineCheckBox = New System.Windows.Forms.CheckBox()
        Me._AppendReturnCheckBox = New System.Windows.Forms.CheckBox()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me._PollTimer = New System.Windows.Forms.Timer(Me.components)
        Me._WriteComboBox = New System.Windows.Forms.ComboBox()
        Me._ClearSessionButton = New System.Windows.Forms.Button()
        CType(Me._PollMillisecondsNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._MessageStatusBitValueNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._ToolStrip.SuspendLayout()
        Me._PollSettingsGroupBox.SuspendLayout()
        Me._WriteSettingsGroupBox.SuspendLayout()
        Me.SuspendLayout()
        '
        '_ClearButton
        '
        Me._ClearButton.Enabled = False
        Me._ClearButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._ClearButton.Location = New System.Drawing.Point(247, 137)
        Me._ClearButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._ClearButton.Name = "_ClearButton"
        Me._ClearButton.Size = New System.Drawing.Size(69, 31)
        Me._ClearButton.TabIndex = 12
        Me._ClearButton.Text = "Erase"
        '
        '_ReadTextBox
        '
        Me._ReadTextBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._ReadTextBox.Location = New System.Drawing.Point(6, 169)
        Me._ReadTextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._ReadTextBox.Multiline = True
        Me._ReadTextBox.Name = "_ReadTextBox"
        Me._ReadTextBox.ReadOnly = True
        Me._ReadTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me._ReadTextBox.Size = New System.Drawing.Size(310, 154)
        Me._ReadTextBox.TabIndex = 9
        Me._ReadTextBox.TabStop = False
        '
        '_ReadTextBoxLabel
        '
        Me._ReadTextBoxLabel.AutoSize = True
        Me._ReadTextBoxLabel.Location = New System.Drawing.Point(6, 148)
        Me._ReadTextBoxLabel.Name = "_ReadTextBoxLabel"
        Me._ReadTextBoxLabel.Size = New System.Drawing.Size(120, 17)
        Me._ReadTextBoxLabel.TabIndex = 8
        Me._ReadTextBoxLabel.Text = "Message Received:"
        '
        '_WriteTextBoxLabel
        '
        Me._WriteTextBoxLabel.Location = New System.Drawing.Point(6, 43)
        Me._WriteTextBoxLabel.Name = "_WriteTextBoxLabel"
        Me._WriteTextBoxLabel.Size = New System.Drawing.Size(125, 18)
        Me._WriteTextBoxLabel.TabIndex = 2
        Me._WriteTextBoxLabel.Text = "Message to Send:"
        '
        '_CloseSessionButton
        '
        Me._CloseSessionButton.Enabled = False
        Me._CloseSessionButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._CloseSessionButton.Location = New System.Drawing.Point(113, 4)
        Me._CloseSessionButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._CloseSessionButton.Name = "_CloseSessionButton"
        Me._CloseSessionButton.Size = New System.Drawing.Size(107, 29)
        Me._CloseSessionButton.TabIndex = 1
        Me._CloseSessionButton.Text = "Close Session"
        '
        '_OpenSessionButton
        '
        Me._OpenSessionButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._OpenSessionButton.Location = New System.Drawing.Point(6, 4)
        Me._OpenSessionButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._OpenSessionButton.Name = "_OpenSessionButton"
        Me._OpenSessionButton.Size = New System.Drawing.Size(107, 29)
        Me._OpenSessionButton.TabIndex = 0
        Me._OpenSessionButton.Text = "Open Session"
        '
        '_ToolTip
        '
        Me._ToolTip.IsBalloon = True
        '
        '_PollMillisecondsNumeric
        '
        Me._PollMillisecondsNumeric.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._PollMillisecondsNumeric.Location = New System.Drawing.Point(216, 60)
        Me._PollMillisecondsNumeric.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me._PollMillisecondsNumeric.Name = "_PollMillisecondsNumeric"
        Me._PollMillisecondsNumeric.Size = New System.Drawing.Size(51, 25)
        Me._PollMillisecondsNumeric.TabIndex = 2
        Me._ToolTip.SetToolTip(Me._PollMillisecondsNumeric, "Time interval of poll timer")
        Me._PollMillisecondsNumeric.Value = New Decimal(New Integer() {1000, 0, 0, 0})
        '
        '_MessageStatusBitValueNumeric
        '
        Me._MessageStatusBitValueNumeric.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._MessageStatusBitValueNumeric.Hexadecimal = True
        Me._MessageStatusBitValueNumeric.Location = New System.Drawing.Point(216, 92)
        Me._MessageStatusBitValueNumeric.Maximum = New Decimal(New Integer() {255, 0, 0, 0})
        Me._MessageStatusBitValueNumeric.Name = "_MessageStatusBitValueNumeric"
        Me._MessageStatusBitValueNumeric.Size = New System.Drawing.Size(51, 25)
        Me._MessageStatusBitValueNumeric.TabIndex = 4
        Me._MessageStatusBitValueNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me._ToolTip.SetToolTip(Me._MessageStatusBitValueNumeric, "Bit value to detect if a message is available for reading.")
        Me._MessageStatusBitValueNumeric.Value = New Decimal(New Integer() {16, 0, 0, 0})
        '
        '_AutoReadCheckBox
        '
        Me._AutoReadCheckBox.AutoSize = True
        Me._AutoReadCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._AutoReadCheckBox.Location = New System.Drawing.Point(48, 124)
        Me._AutoReadCheckBox.Name = "_AutoReadCheckBox"
        Me._AutoReadCheckBox.Size = New System.Drawing.Size(214, 21)
        Me._AutoReadCheckBox.TabIndex = 5
        Me._AutoReadCheckBox.Text = "Read When Message Detected :"
        Me._ToolTip.SetToolTip(Me._AutoReadCheckBox, "Check to have the program automatically read the message from the instrument when" & _
        " message status bit is detected.")
        Me._AutoReadCheckBox.UseVisualStyleBackColor = True
        '
        '_ReadButton
        '
        Me._ReadButton.Enabled = False
        Me._ReadButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._ReadButton.Location = New System.Drawing.Point(166, 100)
        Me._ReadButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._ReadButton.Name = "_ReadButton"
        Me._ReadButton.Size = New System.Drawing.Size(66, 30)
        Me._ReadButton.TabIndex = 6
        Me._ReadButton.Text = "Read"
        '
        '_WriteButton
        '
        Me._WriteButton.Enabled = False
        Me._WriteButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._WriteButton.Location = New System.Drawing.Point(86, 100)
        Me._WriteButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._WriteButton.Name = "_WriteButton"
        Me._WriteButton.Size = New System.Drawing.Size(66, 30)
        Me._WriteButton.TabIndex = 5
        Me._WriteButton.Text = "Write"
        '
        '_QueryButton
        '
        Me._QueryButton.Enabled = False
        Me._QueryButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._QueryButton.Location = New System.Drawing.Point(6, 100)
        Me._QueryButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._QueryButton.Name = "_QueryButton"
        Me._QueryButton.Size = New System.Drawing.Size(66, 30)
        Me._QueryButton.TabIndex = 4
        Me._QueryButton.Text = "Query"
        '
        '_TimingTextBox
        '
        Me._TimingTextBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._TimingTextBox.Location = New System.Drawing.Point(144, 141)
        Me._TimingTextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._TimingTextBox.Name = "_TimingTextBox"
        Me._TimingTextBox.Size = New System.Drawing.Size(49, 25)
        Me._TimingTextBox.TabIndex = 10
        '
        '_TimingUnitsLabel
        '
        Me._TimingUnitsLabel.AutoSize = True
        Me._TimingUnitsLabel.Location = New System.Drawing.Point(195, 145)
        Me._TimingUnitsLabel.Name = "_TimingUnitsLabel"
        Me._TimingUnitsLabel.Size = New System.Drawing.Size(25, 17)
        Me._TimingUnitsLabel.TabIndex = 11
        Me._TimingUnitsLabel.Text = "ms"
        '
        '_ToolStrip
        '
        Me._ToolStrip.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._ToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._StatusLabel, Me._PolledStatusToolStripLabel})
        Me._ToolStrip.Location = New System.Drawing.Point(0, 333)
        Me._ToolStrip.Name = "_ToolStrip"
        Me._ToolStrip.Size = New System.Drawing.Size(635, 25)
        Me._ToolStrip.TabIndex = 15
        Me._ToolStrip.Text = "ToolStrip1"
        '
        '_StatusLabel
        '
        Me._StatusLabel.AutoSize = False
        Me._StatusLabel.Name = "_StatusLabel"
        Me._StatusLabel.Size = New System.Drawing.Size(82, 22)
        Me._StatusLabel.Text = "Open Session."
        '
        '_PolledStatusToolStripLabel
        '
        Me._PolledStatusToolStripLabel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._PolledStatusToolStripLabel.DoubleClickEnabled = True
        Me._PolledStatusToolStripLabel.Name = "_PolledStatusToolStripLabel"
        Me._PolledStatusToolStripLabel.Size = New System.Drawing.Size(30, 22)
        Me._PolledStatusToolStripLabel.Text = "0x00"
        Me._PolledStatusToolStripLabel.ToolTipText = "Polled status byte"
        '
        '_PollSettingsGroupBox
        '
        Me._PollSettingsGroupBox.Controls.Add(Me._MessageStatusBitValueNumeric)
        Me._PollSettingsGroupBox.Controls.Add(Me._MessageStatusBitsNumericLabel)
        Me._PollSettingsGroupBox.Controls.Add(Me._PollMillisecondsNumeric)
        Me._PollSettingsGroupBox.Controls.Add(Me._PollMillisecondsNumericLabel)
        Me._PollSettingsGroupBox.Controls.Add(Me._AutoReadCheckBox)
        Me._PollSettingsGroupBox.Controls.Add(Me._PollEnableCheckBox)
        Me._PollSettingsGroupBox.Location = New System.Drawing.Point(335, 167)
        Me._PollSettingsGroupBox.Name = "_PollSettingsGroupBox"
        Me._PollSettingsGroupBox.Size = New System.Drawing.Size(289, 156)
        Me._PollSettingsGroupBox.TabIndex = 14
        Me._PollSettingsGroupBox.TabStop = False
        Me._PollSettingsGroupBox.Text = "Poll Service Request Byte Settings"
        '
        '_MessageStatusBitsNumericLabel
        '
        Me._MessageStatusBitsNumericLabel.AutoSize = True
        Me._MessageStatusBitsNumericLabel.Location = New System.Drawing.Point(25, 96)
        Me._MessageStatusBitsNumericLabel.Name = "_MessageStatusBitsNumericLabel"
        Me._MessageStatusBitsNumericLabel.Size = New System.Drawing.Size(189, 17)
        Me._MessageStatusBitsNumericLabel.TabIndex = 3
        Me._MessageStatusBitsNumericLabel.Text = "Message Status Bit Value [hex]:"
        '
        '_PollMillisecondsNumericLabel
        '
        Me._PollMillisecondsNumericLabel.AutoSize = True
        Me._PollMillisecondsNumericLabel.Location = New System.Drawing.Point(70, 64)
        Me._PollMillisecondsNumericLabel.Name = "_PollMillisecondsNumericLabel"
        Me._PollMillisecondsNumericLabel.Size = New System.Drawing.Size(144, 17)
        Me._PollMillisecondsNumericLabel.TabIndex = 1
        Me._PollMillisecondsNumericLabel.Text = "Poll Timer Interval [ms]:"
        '
        '_PollEnableCheckBox
        '
        Me._PollEnableCheckBox.AutoSize = True
        Me._PollEnableCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._PollEnableCheckBox.Location = New System.Drawing.Point(52, 32)
        Me._PollEnableCheckBox.Name = "_PollEnableCheckBox"
        Me._PollEnableCheckBox.Size = New System.Drawing.Size(207, 21)
        Me._PollEnableCheckBox.TabIndex = 0
        Me._PollEnableCheckBox.Text = "Poll Service Request on Timer: "
        Me._PollEnableCheckBox.UseVisualStyleBackColor = True
        '
        '_WriteSettingsGroupBox
        '
        Me._WriteSettingsGroupBox.Controls.Add(Me._AppendNewLineCheckBox)
        Me._WriteSettingsGroupBox.Controls.Add(Me._AppendReturnCheckBox)
        Me._WriteSettingsGroupBox.Location = New System.Drawing.Point(335, 61)
        Me._WriteSettingsGroupBox.Name = "_WriteSettingsGroupBox"
        Me._WriteSettingsGroupBox.Size = New System.Drawing.Size(289, 100)
        Me._WriteSettingsGroupBox.TabIndex = 13
        Me._WriteSettingsGroupBox.TabStop = False
        Me._WriteSettingsGroupBox.Text = "Write Settings"
        '
        '_AppendNewLineCheckBox
        '
        Me._AppendNewLineCheckBox.AutoSize = True
        Me._AppendNewLineCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._AppendNewLineCheckBox.Location = New System.Drawing.Point(9, 64)
        Me._AppendNewLineCheckBox.Name = "_AppendNewLineCheckBox"
        Me._AppendNewLineCheckBox.Size = New System.Drawing.Size(248, 21)
        Me._AppendNewLineCheckBox.TabIndex = 1
        Me._AppendNewLineCheckBox.Text = "Auto Append New Line Character [\n]:"
        Me._AppendNewLineCheckBox.UseVisualStyleBackColor = True
        '
        '_AppendReturnCheckBox
        '
        Me._AppendReturnCheckBox.AutoSize = True
        Me._AppendReturnCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._AppendReturnCheckBox.Location = New System.Drawing.Point(26, 32)
        Me._AppendReturnCheckBox.Name = "_AppendReturnCheckBox"
        Me._AppendReturnCheckBox.Size = New System.Drawing.Size(231, 21)
        Me._AppendReturnCheckBox.TabIndex = 0
        Me._AppendReturnCheckBox.Text = "Auto Append Return Character [\r]:"
        Me._AppendReturnCheckBox.UseVisualStyleBackColor = True
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBox1.Location = New System.Drawing.Point(26, 32)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(249, 21)
        Me.CheckBox1.TabIndex = 0
        Me.CheckBox1.Text = "Auto Append Return Character [\r]:"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        '_PollTimer
        '
        Me._PollTimer.Interval = 1000
        '
        '_WriteComboBox
        '
        Me._WriteComboBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._WriteComboBox.FormattingEnabled = True
        Me._WriteComboBox.Location = New System.Drawing.Point(6, 62)
        Me._WriteComboBox.Name = "_WriteComboBox"
        Me._WriteComboBox.Size = New System.Drawing.Size(310, 25)
        Me._WriteComboBox.TabIndex = 3
        '
        '_ClearSessionButton
        '
        Me._ClearSessionButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ClearSessionButton.Location = New System.Drawing.Point(247, 100)
        Me._ClearSessionButton.Name = "_ClearSessionButton"
        Me._ClearSessionButton.Size = New System.Drawing.Size(66, 30)
        Me._ClearSessionButton.TabIndex = 7
        Me._ClearSessionButton.Text = "Clear"
        Me._ToolTip.SetToolTip(Me._ClearSessionButton, "Clears the session")
        Me._ClearSessionButton.UseVisualStyleBackColor = True
        '
        'SimpleReadWrite
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.ClientSize = New System.Drawing.Size(635, 358)
        Me.Controls.Add(Me._ClearSessionButton)
        Me.Controls.Add(Me._WriteComboBox)
        Me.Controls.Add(Me._WriteSettingsGroupBox)
        Me.Controls.Add(Me._PollSettingsGroupBox)
        Me.Controls.Add(Me._ToolStrip)
        Me.Controls.Add(Me._TimingUnitsLabel)
        Me.Controls.Add(Me._TimingTextBox)
        Me.Controls.Add(Me._ClearButton)
        Me.Controls.Add(Me._ReadTextBox)
        Me.Controls.Add(Me._ReadTextBoxLabel)
        Me.Controls.Add(Me._WriteTextBoxLabel)
        Me.Controls.Add(Me._CloseSessionButton)
        Me.Controls.Add(Me._OpenSessionButton)
        Me.Controls.Add(Me._ReadButton)
        Me.Controls.Add(Me._WriteButton)
        Me.Controls.Add(Me._QueryButton)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MinimizeBox = False
        Me.Name = "SimpleReadWrite"
        Me.Text = "Simple Read/Write"
        CType(Me._PollMillisecondsNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._MessageStatusBitValueNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me._ToolStrip.ResumeLayout(False)
        Me._ToolStrip.PerformLayout()
        Me._PollSettingsGroupBox.ResumeLayout(False)
        Me._PollSettingsGroupBox.PerformLayout()
        Me._WriteSettingsGroupBox.ResumeLayout(False)
        Me._WriteSettingsGroupBox.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _ClearButton As System.Windows.Forms.Button
    Private WithEvents _ReadTextBox As System.Windows.Forms.TextBox
    Private WithEvents _ReadTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _WriteTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _CloseSessionButton As System.Windows.Forms.Button
    Private WithEvents _OpenSessionButton As System.Windows.Forms.Button
    Private WithEvents _ToolTip As System.Windows.Forms.ToolTip
    Private WithEvents _ReadButton As System.Windows.Forms.Button
    Private WithEvents _WriteButton As System.Windows.Forms.Button
    Private WithEvents _QueryButton As System.Windows.Forms.Button
    Private WithEvents _TimingUnitsLabel As System.Windows.Forms.Label
    Private WithEvents _ToolStrip As System.Windows.Forms.ToolStrip
    Private WithEvents _StatusLabel As System.Windows.Forms.ToolStripLabel
    Private WithEvents _PolledStatusToolStripLabel As System.Windows.Forms.ToolStripLabel
    Private WithEvents _PollSettingsGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _MessageStatusBitValueNumeric As System.Windows.Forms.NumericUpDown
    Private WithEvents _MessageStatusBitsNumericLabel As System.Windows.Forms.Label
    Private WithEvents _PollMillisecondsNumeric As System.Windows.Forms.NumericUpDown
    Private WithEvents _PollMillisecondsNumericLabel As System.Windows.Forms.Label
    Private WithEvents _AutoReadCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _PollEnableCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _WriteSettingsGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _AppendNewLineCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _AppendReturnCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _TimingTextBox As System.Windows.Forms.TextBox
    Private WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Private WithEvents _PollTimer As System.Windows.Forms.Timer
    Private WithEvents _WriteComboBox As System.Windows.Forms.ComboBox
    Private WithEvents _ClearSessionButton As System.Windows.Forms.Button
End Class
